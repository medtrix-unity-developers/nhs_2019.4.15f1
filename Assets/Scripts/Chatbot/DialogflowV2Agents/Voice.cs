﻿
using Newtonsoft.Json;


[JsonObject]
public class Voice
{
	[JsonProperty]
	public string name { get; set; }

	[JsonProperty]
	public string ssmlGender { get; set; }
}
