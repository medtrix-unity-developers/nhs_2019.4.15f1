﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultBotBubble : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

     
    public void OnCallClicked()
    {
        Application.OpenURL("tel://01174145428");   // telephone number = 0117 4145428/9
    }
    public void OnEmailClicked()
    {
        string email = "renaldietitians@nbt.nhs.uk";
        string subject = MyEscapeURL("");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }
}
