using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class contains the gameobjects and methods for interacting with the UI.
/// </summary>
public class BotUI : MonoBehaviour {
    public GameObject       contentDisplayObject;               // Text gameobject where all the conversation is shown
    public InputField       input;                              // InputField gameobject wher user types their message

    public GameObject       userBubble;                         // reference to user chat bubble prefab
    public GameObject       botBubble;                          // reference to bot chat bubble prefab
    public GameObject       defaultBotBubble;
    public Sprite[]         botSprites;

    private const int       messagePadding = 30;                // space between chat bubbles 
    public int              allMessagesHeight = 30; // int to keep track of where next message should be rendered
    public bool             increaseContentObjectHeight;        // bool to check if content object height should be increased

    public NetworkManager   networkManager;                     // reference to Network Manager script
    [SerializeField] private Material chatBotBubbleMat;
    [SerializeField] private Material chatUserBubbleMat;

    private string senderType = "";

    private int botBubbleCount = 0;
    public DF2ClientAudioTester dF2ClientAudioTester;
    private bool isViewMoreOption = false;

    public List<GameObject> botBubbleArray = new List<GameObject>();


    void Start()
    {
        allMessagesHeight = 30;
        botBubbleArray.Clear();
    }

    public void clearBotBubbleArray()
    {
        botBubbleArray.Clear();
    }
     
    /// <summary>
    /// This method is used to update the display panel with the user's and bot's messages.
    /// </summary>
    /// <param name="sender">The one who wrote this message</param>
    /// <param name="message">The message</param>
    public void UpdateDisplay (string sender, string message, string messageType, string unTrimmedMessage ) {
        // Create chat bubble and add components
        GameObject chatBubbleChild = CreateChatBubble(sender, unTrimmedMessage);
        AddChatComponent(chatBubbleChild, message, messageType);
 
        if (sender != "user")
        {
            Image avatarImageObj = chatBubbleChild.transform.parent.parent.transform.GetChild(1).GetComponent<Image>();
            avatarImageObj.sprite = botSprites[AppManager.Instance.CurrentCharPageID - 1];
            //chatBubbleChild.transform.parent.gameObject.GetComponent<Image>().material = chatBotBubbleMat;
        }

        if (sender == "user")
        {
            if (GameObject.FindObjectOfType<PatientProfileData>().UserProfilePhoto.GetComponent<RawImage>().texture != null)
            {
                Texture2D tex = (Texture2D)GameObject.FindObjectOfType<PatientProfileData>().UserProfilePhoto.GetComponent<RawImage>().texture;
                chatBubbleChild.transform.parent.parent.transform.GetChild(1).GetChild(1).gameObject.SetActive(false);
                chatBubbleChild.transform.parent.parent.transform.GetChild(1).GetChild(0).GetComponent<RawImage>().texture
                    = tex;
                chatBubbleChild.transform.parent.parent.transform.GetChild(1).GetChild(0).transform.localScale
                    = new Vector3(1f, tex.height / (float)tex.width, 1f);
            }
             
        }

        // Set chat bubble position
        StartCoroutine(SetChatBubblePosition(chatBubbleChild.transform.parent.parent.GetComponent<RectTransform>(), sender));
    }

    /// <summary>
    /// Coroutine to set the position of the chat bubble inside the contentDisplayObject.
    /// </summary>
    /// <param name="chatBubblePos">RectTransform of chat bubble</param>
    /// <param name="sender">Sender who sent the message</param>
    private IEnumerator SetChatBubblePosition (RectTransform chatBubblePos, string sender) {
        // Wait for end of frame before calculating UI transform
        yield return new WaitForEndOfFrame();

        // get horizontal position based on sender
        int horizontalPos = 0;
        if (sender == "user") {
            horizontalPos = 30;   
        } else if (sender == "bot") {
            horizontalPos = 10;
        }

        chatBubblePos.anchoredPosition3D = new Vector3(horizontalPos, -(allMessagesHeight), 0);
        // set the vertical position of chat bubble
       
        allMessagesHeight += 50 + (int)chatBubblePos.GetChild(0).GetComponent<RectTransform>().rect.height;

        if (allMessagesHeight > 302)
        {  
            // update contentDisplayObject hieght
            RectTransform contentRect = contentDisplayObject.GetComponent<RectTransform>();
            contentRect.sizeDelta = new Vector2(contentRect.sizeDelta.x, allMessagesHeight + messagePadding);
            contentDisplayObject.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 0f;

            if (isViewMoreOption == true && botBubbleCount >= 2 && sender == "bot")
                 contentDisplayObject.GetComponent<RectTransform>().position  = new Vector2 (0, allMessagesHeight  - (contentDisplayObject.transform.GetChild(contentDisplayObject.transform.childCount - 1).GetChild(0).GetComponent<RectTransform>().rect.height));     
            else
                contentDisplayObject.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 0f;

        }
        else
        {
            contentDisplayObject.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 0f;
        }
    }

    /// <summary>
    /// Coroutine to update chat bubble positions based on their size.
    /// </summary>
    public IEnumerator RefreshChatBubblePosition () {
        // Wait for end of frame before calculating UI transform
        yield return new WaitForEndOfFrame();

  
        // refresh position of all gameobjects based on size
        int localAllMessagesHeight = 30;
        foreach (RectTransform chatBubbleRect in contentDisplayObject.GetComponent<RectTransform>()) {
            chatBubbleRect.anchoredPosition3D = new Vector3(30, -localAllMessagesHeight, 0);

            if (chatBubbleRect.GetChild(0).GetComponent<RectTransform>().sizeDelta.y < 50) {
                localAllMessagesHeight += 50 + (int)chatBubbleRect.GetChild(0).GetComponent<RectTransform>().rect.height;
            } else {
                localAllMessagesHeight += (int)chatBubbleRect.sizeDelta.y + (int)chatBubbleRect.GetChild(0).GetComponent<RectTransform>().rect.height;
            }
        }

        // Update global message Height variable
        allMessagesHeight = localAllMessagesHeight;
        if (allMessagesHeight > 302) {  
            RectTransform contentRect = contentDisplayObject.GetComponent<RectTransform>();
            contentRect.sizeDelta = new Vector2(contentRect.sizeDelta.x, allMessagesHeight + messagePadding);
            contentDisplayObject.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 0;
        }
        else
        {
            contentDisplayObject.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 0f;
        }
    }

    /// <summary>
    /// This method creates chat bubbles from prefabs and sets their positions.
    /// </summary>
    /// <param name="sender">The sender of message for which bubble is rendered</param>
    /// <returns>Reference to empty gameobject on which message components can be added</returns>
    private GameObject CreateChatBubble (string sender, string unTrimmedMessage) {
        GameObject chat = null;
        if (sender == "user") {
            botBubbleCount = 0;
            // Create user chat bubble from prefabs and set it's position
            chat = Instantiate(userBubble);
            chat.transform.SetParent(contentDisplayObject.transform, false);
            senderType = "user";
        } else if (sender == "bot") {
            // Create bot chat bubble from prefabs and set it's position
            chat = Instantiate(botBubble);
            chat.transform.SetParent(contentDisplayObject.transform, false);

            botBubbleArray.Add(chat);

            //if (chat.GetComponentsInChildren<Button>() != null)
            if (unTrimmedMessage.Length > 150)
            {
                isViewMoreOption = true;

                Button[] btns = chat.GetComponentsInChildren<Button>();
                if(btns.Length >= 1)
                {
                    //btns[0].gameObject.SetActive(true);
                    btns[0].name = "viewMore_" + botBubbleCount;
                    btns[0].onClick.AddListener(() => evtListener_ViewMoreClicked(btns[0].transform.parent.parent.gameObject, btns[0].name));
                    botBubbleCount++;
                 }
            }
            else
            {
                Button[] btns = chat.GetComponentsInChildren<Button>();
                btns[0].gameObject.SetActive(false);
                isViewMoreOption = false;
             }

            senderType = "bot";
        }
 

        // Add content size fitter
        ContentSizeFitter chatSize = chat.transform.GetChild(0).gameObject.AddComponent<ContentSizeFitter>();
        //chatSize.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        chatSize.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

        // Add vertical layout group
        VerticalLayoutGroup verticalLayout = chat.transform.GetChild(0).gameObject.AddComponent<VerticalLayoutGroup>();

        if (sender == "user")
        {
            verticalLayout.padding = new RectOffset(20,20,20,20);
        }
        else if (sender == "bot")
        {
             verticalLayout.padding = new RectOffset(30,30,30,30);
        }
        verticalLayout.childAlignment = TextAnchor.MiddleCenter;

        // Return empty gameobject on which chat components will be added
        return chat.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
    }

    public void evtListener_ViewMoreClicked(GameObject botBubbleObj, string eve)
    {
        botBubbleObj.transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
        //int bubbleID = int.Parse( eve.Split('_')[1]);
        int specificBubbleId = botBubbleArray.IndexOf(botBubbleObj.transform.parent.gameObject);
        OverriteChatComponent(botBubbleObj.transform.GetChild(0).gameObject, dF2ClientAudioTester.chatHistoryArray[specificBubbleId], "text");          // dF2ClientAudioTester.arr[bubbleID]
        StartCoroutine(RefreshChatBubblePosition());
        //Debug.Log("COMING TO EVT LISTENER VIEW MORE CLICKED>..." + botBubbleObj.name + eve+ " bubbleID :"+ bubbleID);
    }

    public void UpdateDefaultBotBubble(string sender, string message, string messageType)
    {
        GameObject defaultBubblechat = Instantiate(defaultBotBubble);
        defaultBubblechat.transform.SetParent(contentDisplayObject.transform, false);
        senderType = "bot";
 
        // Set chat bubble position
        StartCoroutine(SetChatBubblePosition(defaultBubblechat.GetComponent<RectTransform>(), sender));
    }
     
    /// <summary>
    /// This method adds message component to chat bubbles based on message type.
    /// </summary>
    /// <param name="chatBubbleObject">The empty gameobject under chat bubble</param>
    /// <param name="message">message to be shown</param>
    /// <param name="messageType">The type of message (text, image etc)</param>
    private void AddChatComponent (GameObject chatBubbleObject, string message, string messageType) {
        switch (messageType) {
            case "text":
                // Create and init Text component
                Text chatMessage = chatBubbleObject.AddComponent<Text>();

                // add font as it is none at times when creating text component from script
                chatMessage.font = Resources.Load("Font/frutiger") as Font;         //  GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

                if (senderType == "user")
                    chatMessage.color = Color.white;
                else
                    chatMessage.color = new Color32(50,50,50,255);// Color.black;

                chatMessage.fontStyle = FontStyle.Normal;
                chatMessage.fontSize = 28;
                chatMessage.lineSpacing = 1;
                chatMessage.horizontalOverflow = HorizontalWrapMode.Wrap;
                chatMessage.verticalOverflow = VerticalWrapMode.Overflow;
                chatMessage.alignment = TextAnchor.UpperLeft;
                chatMessage.supportRichText = true;

                //string tx = "Hello, here is the list: ^My name1 jhd gf dfu fjhfe fhuh jh ^My name2 ^My name3 ^My name4 ^My name5";
                //tx = tx.Replace("^", "\n <size=50><b>.</b></size> ");
                //chatMessage.text = tx.Trim();

                if(senderType == "user")
                {
                    chatMessage.text = message.Trim();
                }
                else
                {
                    //message = "Hello, here is the list: ^Fruit is 1 ^Fruit is 2 ^Fruit is 3 ^Fruit is 4 ^Fruit is 5";
                    message = message.Replace("^^", "^");

                    if (message.Contains("^"))
                    {
                        message = message.Replace("^", "\n<size=50><b>. </b></size>");
                        chatMessage.text = message.Trim();
                    }
                    else
                    {
                        chatMessage.text = message.Trim();
                    }
                }
                //chatMessage.text = message.Trim();
                break;
            case "image":
                // Create and init Image component
                Image chatImage = chatBubbleObject.AddComponent<Image>();
                StartCoroutine(networkManager.SetImageTextureFromUrl(message, chatImage));
                break;
            case "attachment":
                break;
            case "buttons":
                break;
            case "elements":
                break;
            case "quick_replies":
                break;
        }
    }

    private void OverriteChatComponent(GameObject chatBubbleObject, string message, string messageType)
    {
        switch (messageType)
        {
            case "text":
                // Create and init Text component
                Text chatMessage = chatBubbleObject.GetComponent<Text>();

                // add font as it is none at times when creating text component from script
                chatMessage.font = Resources.Load("Font/frutiger") as Font;         //  GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

                if (senderType == "user")
                    chatMessage.color = Color.white;
                else
                    chatMessage.color = new Color32(50, 50, 50, 255);// Color.black;

                chatMessage.fontStyle = FontStyle.Normal;
                chatMessage.fontSize = 28;
                chatMessage.lineSpacing = 1;
                chatMessage.horizontalOverflow = HorizontalWrapMode.Wrap;
                chatMessage.verticalOverflow = VerticalWrapMode.Overflow;
                chatMessage.alignment = TextAnchor.UpperLeft;
                chatMessage.supportRichText = true;

                //string tx = "Hello, here is the list: ^My name1 jhd gf dfu fjhfe fhuh jh ^My name2 ^My name3 ^My name4 ^My name5";
                //tx = tx.Replace("^", "\n <size=50><b>.</b></size> ");
                //chatMessage.text = tx.Trim();

                if (senderType == "user")
                {
                    chatMessage.text = message.Trim();
                }
                else
                {
                    //message = "Hello, here is the list: ^Fruit is 1 ^Fruit is 2 ^Fruit is 3 ^Fruit is 4 ^Fruit is 5";

                    if (message.Contains("^"))
                    {
                        message = message.Replace("^", "\n<size=50><b>.</b></size>   ");
                        chatMessage.text = message.Trim();
                    }
                    else
                    {
                        chatMessage.text = message.Trim();
                    }
                }
                break;
          }
        }

  }
