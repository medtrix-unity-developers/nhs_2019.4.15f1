﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Text;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;

public class LoginHandler : MonoBehaviour
{
    [SerializeField] public PatientEmailCheck _PatientEmailCheck = new PatientEmailCheck();
    [SerializeField] public InputField EmailID;
    [SerializeField] public InputField FirstName;
    [SerializeField] public InputField SecondName;
    [SerializeField] public Text StatusMessage;

    [SerializeField] public Registration1 registration1;
    [SerializeField] public Registration2 registration2;
    [SerializeField] public Registration3 registration3;
    [SerializeField] private ScreenController screenController;


    // Awake is called before the first frame update
    void Awake()
    {
        screenController.OnShowStarted += OnShowLoginPage;
    }

    private void OnShowLoginPage()
    {
        EmailID.text = "";
    }

    public void OnLoginClicked()  // string _setURL, string emailText
    {
        _PatientEmailCheck.User_EmailId = EmailID.text;
        _PatientEmailCheck.User_FirstName = FirstName.text;
        _PatientEmailCheck.User_SecondName = SecondName.text;

        string _setURL = "";
        if (_setURL == "")
        {
            _setURL = "https://www.medtrixhealthcare.com/NHS_ChatBot/NHSChatbot/UserLoginData";
        }

        StartCoroutine(OnEmailValidation(_PatientEmailCheck, _setURL));
    }

    IEnumerator OnEmailValidation(PatientEmailCheck patientEmailCheck, string _setURL)
    {
        StatusMessage.text = "";
        bool userExists = false;
        string patientJSONData = JsonUtility.ToJson(patientEmailCheck);
        //Debug.Log("patientJSONData : " + patientJSONData);
        var request = new UnityWebRequest(_setURL, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(patientJSONData);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();
        if (!request.isNetworkError)
        {
            //Debug.Log("Data successfully sent to server." + request.downloadHandler.text);
            string response = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
            PatientResponseData patientUserResponse = JsonConvert.DeserializeObject<PatientResponseData>(response);

            Debug.Log("Response: " + patientUserResponse.ToString());
            if (patientUserResponse.statusCode == "-1")
            {
                StatusMessage.text = patientUserResponse.status;  //  "Email ID doesn't exist. Please register yourself.";
            }
            else if (patientUserResponse.statusCode == "1")
            {
                //StatusMessage.text = patientUserResponse.status;  // "User Logged In Successfully.";
                AppManager.Instance.PatientUserID = int.Parse(patientUserResponse.userCode);
                SaveData.Instance._PatientData.User_Name = patientUserResponse.User_FirstName + " " + patientUserResponse.User_LastName;
                SaveData.Instance._PatientData.User_FirstName = patientUserResponse.User_FirstName;
                SaveData.Instance._PatientData.User_LastName = patientUserResponse.User_LastName;

                SaveData.Instance._PatientData.User_Id = int.Parse(patientUserResponse.userCode);
                SendReminderData.Instance.storedUserId = int.Parse(patientUserResponse.userCode);
                SaveData.Instance._PatientData.User_EmailId = patientUserResponse.User_EmailId;
                SaveData.Instance._PatientData.User_Height = patientUserResponse.User_Height;
                SaveData.Instance._PatientData.User_Weight = patientUserResponse.User_Weight;
                SaveData.Instance._PatientData.User_Lose_Weight = patientUserResponse.User_Lose_Weight;
                SaveData.Instance._PatientData.User_Weight_6_Month = patientUserResponse.User_Weight_6_Month;
                SaveData.Instance._PatientData.User_Potassium_Info = patientUserResponse.User_Potassium_Info;
                SaveData.Instance._PatientData.User_Phosphate_Info = patientUserResponse.User_Phosphate_Info;
                SaveData.Instance._PatientData.User_Diabetes_Info = patientUserResponse.User_Diabetes_Info;
                SaveData.Instance._PatientData.User_Salt_Info = patientUserResponse.User_Salt_Info;
                SaveData.Instance._PatientData.User_Fluid_Info = patientUserResponse.User_Fluid_Info;
                SaveData.Instance._PatientData.User_Calorie_Info = patientUserResponse.User_Calorie_Info;
                SaveData.Instance._PatientData.User_NHS_Trust = patientUserResponse.User_NHS_Trust;
                SaveData.Instance._PatientData.User_Kidney_Info = patientUserResponse.User_Kidney_Info;
                ScreenManager.Instance.Show("DieticianCustomScreen");
                PlayerPrefs.SetInt("UserLoggedStatus", 1);
                PlayerPrefs.SetInt("UserUniqueID", int.Parse(patientUserResponse.userCode));
                SendReminderData.Instance.GetPosts(patientUserResponse.userCode);
                SaveData.Instance.SavePatientDataToLocal(SaveData.Instance._PatientData);
            }
            else if (patientUserResponse.statusCode == "2")
            {
                StatusMessage.text = patientUserResponse.status; // "Your profile is not Approved by NBT.";
            }
            else if (patientUserResponse.statusCode == "0")
            {
                StatusMessage.text = patientUserResponse.status;  //  "Your profile is pending for verification. You will receive a short email.";
            }
            /*else if (patientUserResponse.statusCode == "3")
            {
                StatusMessage.text = patientUserResponse.status; // "Your profile is Pending by NBT.";
            } */
        }
        else
        {
            Debug.Log("Error sending data to server.");

        }
        //GameObject.FindObjectOfType<Registration1>().OnceEmailExistanceVerified(userExists);
    }


    public void OnEmailEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnEmailEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }



    public void OnRegisterButtonClicked()
    {
        StatusMessage.text = "";
        EmailID.text = "";
        registration1.OnResetFields();
        registration2.OnResetFields();
        registration3.OnResetFields();
        ScreenManager.Instance.Show("RegisterScreen_1");
    }


    [System.Serializable]
    public class PatientResponseData
    {
        public string statusCode;
        public string status;
        public string userCode;
        public string User_EmailId;
        public string User_Height;
        public string User_Weight;
        public string User_Lose_Weight;
        public string User_Weight_6_Month;
        public string User_Potassium_Info;
        public string User_Phosphate_Info;
        public string User_Diabetes_Info;
        public string User_Salt_Info;
        public string User_Fluid_Info;
        public string User_Calorie_Info;
        public string User_NHS_Trust;
        public string User_Kidney_Info;
        public string User_FirstName;
        public string User_LastName;
    }

    [System.Serializable]
    public class PatientEmailCheck
    {
        public string User_EmailId;
        public string User_FirstName;
        public string User_SecondName;
    }
}
