﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PaperPlaneTools;
using System.IO;
using ImageCropperNamespace;

public class PatientProfileData : MonoBehaviour
{
    [SerializeField] private ScreenController screencontroller;

    //public SaveData saveData;
    public RawImage UserProfilePhoto;
    public GameObject _ImageCropper;
    public ImageCropperDemo _ImageCropperDemo;

    [SerializeField] private GameObject ChoosePhotoOption;
    [SerializeField] private GameObject tempPicIcon;
    private string savedProfImagePath;
    private string unSavedProfImagePath;
 
    [SerializeField] private Button EditButton;
    [SerializeField] private Button BackButton;
    [SerializeField] private Button UpdateButton;
    [SerializeField] private Button ProfileButton;


    [SerializeField] private Toggle YesToggleLoseWeight;
    [SerializeField] private Toggle NoToggleLoseWeight;
    [SerializeField] private Toggle YesToggleKidneyTrans;
    [SerializeField] private Toggle NoToggleKidneyTrans;


    [SerializeField] private GameObject[] DietaryInformations;
    [SerializeField] private InputField FullNameText;
    [SerializeField] private InputField FirstNameText;
    [SerializeField] private InputField LastNameText;
    //[SerializeField] private InputField SecondNameText;
    [SerializeField] private InputField HeightText;
    [SerializeField] private InputField WeightText;
    [SerializeField] private InputField WeightPastText;
    [SerializeField] private ScrollRect ProfileContainerRect;

    [SerializeField] private Text ScreenTitle;
    [SerializeField] private GameObject ScreenEditInformation;

    [SerializeField] private Text WeightErrorMessageTxt;
    [SerializeField] private Text PastWeightErrorMessageTxt;
    [SerializeField] private Text ErrorMessageTxt;
    [SerializeField] private Text DietaryErrorMessageTxt;

    [SerializeField] private GameObject _weightConcernPopUp;
    [SerializeField] private GameObject _dietaryInfoPopUp;
    [SerializeField] private GameObject _dietaryInfoPopUp_Phosphate;
    [SerializeField] private GameObject _kidneyPopup_UserProfile;

    [SerializeField] private GameObject _textLineHeight;
    [SerializeField] private GameObject _textLineWeight;
    [SerializeField] private GameObject _textLineWeightPast;
    [SerializeField] private GameObject _arrowHeight;
    [SerializeField] private GameObject _arrowWeight;
    [SerializeField] private GameObject _labelHeight;
    [SerializeField] private GameObject _labelWeight;

    private int dropDownVal_Height = 0;
    private int dropDownVal_Weight = 0;

    [SerializeField] private Dropdown[] DietaryInfoDropDown;


    [SerializeField] private Sprite whiteDietImage;
    [SerializeField] private Sprite blueDietImage;

    private string User_Potassium_Info = "0";
    private string User_Phosphate_Info = "0";
    private string User_Diabetes_Info = "0";
    private string User_Salt_Info = "1";
    private string User_Fluid_Info = "0";
    private string User_Calorie_Info = "0";

    [SerializeField] private Sprite WhiteDiabetes;
    [SerializeField] private Sprite BlueDiabetes;

    [HideInInspector]
    public Texture2D tempTexture2D;

    private void Awake()
    {
        SaveData.Instance._PatientData.User_Id = PlayerPrefs.GetInt("UserUniqueID");
        AppManager.Instance.PatientUserID = PlayerPrefs.GetInt("UserUniqueID");
       
        screencontroller.OnShowStarted += OnProfileScreenShowed;

    }

    private void OnProfileScreenShowed()
    {

        //tempTexture2D = (Texture2D)UserProfilePhoto.texture;
        //if(ProfileContainerRect.verticalNormalizedPosition < 1)
        //    ProfileContainerRect.verticalNormalizedPosition = 2f;
        StartCoroutine(ResetScrollRect2());

        for (int i = 0; i < DietaryInformations.Length; i++)
        {
            DietaryInformations[i].GetComponent<Button>().interactable = false;
            OnDietaryInformationLoad(DietaryInformations[i].name);
        }
        ScreenTitle.text = "Profile";
        ScreenEditInformation.SetActive(false);

        HeightText.GetComponent<InputField>().interactable = false;
        WeightText.GetComponent<InputField>().interactable = false;
        WeightPastText.GetComponent<InputField>().interactable = false;
        _textLineHeight.SetActive(false);
        _textLineWeight.SetActive(false);
        _textLineWeightPast.SetActive(false);
        _arrowHeight.SetActive(false);
        _arrowWeight.SetActive(false);
        _labelHeight.transform.localPosition = new Vector3(0, _arrowHeight.transform.localPosition.y, _arrowHeight.transform.localPosition.z);
        _labelWeight.transform.localPosition = new Vector3(0, _arrowHeight.transform.localPosition.y, _arrowHeight.transform.localPosition.z);

        YesToggleLoseWeight.interactable = false;
        NoToggleLoseWeight.interactable = false;
        //YesToggleKidneyTrans.interactable = false;
        //NoToggleKidneyTrans.interactable = false;
        BackButton.interactable = true;
        UpdateButton.gameObject.SetActive(false);
        ProfileButton.interactable = false;

        for (int i = 0; i < DietaryInfoDropDown.Length - 1; i++)
        {
            DietaryInfoDropDown[i].interactable = false;
        }

        FullNameText.text = SaveData.Instance._PatientData.User_Name;
        FirstNameText.text = SaveData.Instance._PatientData.User_FirstName;    //User_First_Name+" "+ SaveData.Instance._PatientData.User_Last_Name;
        LastNameText.text = SaveData.Instance._PatientData.User_LastName;
        HeightText.text = (SaveData.Instance._PatientData.User_Height == "" ? "0" : SaveData.Instance._PatientData.User_Height);
        WeightText.text = (SaveData.Instance._PatientData.User_Weight == "" ? "0" : SaveData.Instance._PatientData.User_Weight);
        WeightPastText.text = (SaveData.Instance._PatientData.User_Weight_6_Month == "" ? "0" : SaveData.Instance._PatientData.User_Weight_6_Month);

        if (PlayerPrefs.GetInt("HeightDropDownValue") == 0)
            DietaryInfoDropDown[0].value = 0;
        else
            DietaryInfoDropDown[0].value = 1;

        if (PlayerPrefs.GetInt("WeightDropDownValue") == 0)
        {
            DietaryInfoDropDown[1].value = 0;
            DietaryInfoDropDown[2].value = 0;
        }
        else
        {
            DietaryInfoDropDown[1].value = 1;
            DietaryInfoDropDown[2].value = 1;
        }
         

        if (SaveData.Instance._PatientData.User_Lose_Weight == "1")
        {
            YesToggleLoseWeight.isOn = true;
            NoToggleLoseWeight.isOn = false;
        }
        else
        {
            YesToggleLoseWeight.isOn = false;
            NoToggleLoseWeight.isOn = true;
        }

        if (SaveData.Instance._PatientData.User_Kidney_Info == "1")
        {
            YesToggleKidneyTrans.isOn = true;
            NoToggleKidneyTrans.isOn = false;
        }
        else
        {
            YesToggleKidneyTrans.isOn = false;
            NoToggleKidneyTrans.isOn = true;
        }
  
        YesToggleKidneyTrans.interactable = false;
        NoToggleKidneyTrans.interactable = false;

        if (PlayerPrefs.GetString("UserProfileImageExist") != "") //&& UserProfilePhoto.texture == null
        {
            /* string path = PlayerPrefs.GetString("UserProfileImageExist");
             Texture2D _tempTexture2D = NativeGallery.LoadImageAtPath(path, 512, false);
             tempPicIcon.SetActive(false);
             UserProfilePhoto.texture = _tempTexture2D;
             CanvasExtensions.SizeToParent(UserProfilePhoto, 0);
             (UserProfilePhoto.texture as Texture2D).Apply(); */

            tempPicIcon.SetActive(false);

            byte[] pngImageByteArray = null;
            //string tempPath = Path.Combine(Application.persistentDataPath, "images");
            //tempPath = Path.Combine(tempPath, "UserProfileImage.png");
            if(File.Exists(PlayerPrefs.GetString("UserProfileImageExist")))
                 pngImageByteArray = File.ReadAllBytes(PlayerPrefs.GetString("UserProfileImageExist"));
            else
                tempPicIcon.SetActive(true);


            Texture2D tempTexture = new Texture2D(2, 2);
            tempTexture.LoadImage(pngImageByteArray);
            UserProfilePhoto.texture = tempTexture;
        }
    }

    public void OnUploadPhotoClicked()
    {
        PickImage(2048);
    }

    //public Texture2D dummyTexture;     // This is dummy texture to test the profile pic drag and drop...
    private void PickImage(int maxSize)
    {
#if Unity_Editor
    tempTexture2D  = UserProfilePhoto.texture.ToTexture2D();
#endif


        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            //Debug.Log("PickImage path : " + path);
            if (path != null)
            {
                unSavedProfImagePath = path.ToString();
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize, false);
                if (texture == null)
                {
                    //Debug.Log("Couldn't load texture from " + path);
                    return;
                }

                tempPicIcon.SetActive(false);
                _ImageCropper.SetActive(true);
                _ImageCropperDemo.Crop(texture);
                OnUserPickedImage(texture);
    
            }
        }, "MyProfileImage", "image/*");

        //Debug.Log("Permission result: " + permission);
    }

    private void OnUserPickedImage(Texture2D profilePicimage)
    {
        StartCoroutine(TakeScreenshotAndCrop(profilePicimage));
    }

    private IEnumerator TakeScreenshotAndCrop(Texture2D profilePicimage)
    {
        yield return new WaitForEndOfFrame();
        ImageCropper.Instance.Show(profilePicimage, (bool result, Texture originalImage, Texture2D croppedImage) =>
        {
            // Destroy previously cropped texture (if any) to free memory
            Destroy(UserProfilePhoto.texture, 5f);

            // If screenshot was cropped successfully
            if (result)
            {
                // Assign cropped texture to the RawImage
                UserProfilePhoto.enabled = true;
                UserProfilePhoto.texture = croppedImage;
                CanvasExtensions.SizeToParent(UserProfilePhoto, 0);
                  
                tempTexture2D = croppedImage;
            }else
            {
                UserProfilePhoto.enabled = false;
            }
        },
            settings: new ImageCropper.Settings()
            {
                 imageBackground = Color.clear, // transparent background
            },
            croppedImageResizePolicy: (ref int width, ref int height) =>
            {
            });
    }

    public void OnDietaryInformationLoad(string clickedDietaryButton)
    {
        switch (clickedDietaryButton)
        {
            case "Potassium":
                if(SaveData.Instance._PatientData.User_Potassium_Info == "1")
                {
                    //DietaryInformations[0].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[0].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[0].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    User_Potassium_Info = "1";
                }
                else
                {
                    //DietaryInformations[0].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[0].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[0].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    User_Potassium_Info = "0";
                }
                DietaryInformations[0].transform.GetChild(0).gameObject.SetActive(true);
                DietaryInformations[0].transform.GetChild(1).gameObject.SetActive(false);
                break;
            case "Phosphate":
                if (SaveData.Instance._PatientData.User_Phosphate_Info == "1")
                {
                    //DietaryInformations[1].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[1].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[1].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    User_Phosphate_Info = "1";
                }
                else
                {
                    //DietaryInformations[1].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[1].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[1].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    User_Phosphate_Info = "0";

                }
                DietaryInformations[1].transform.GetChild(0).gameObject.SetActive(true);
                DietaryInformations[1].transform.GetChild(1).gameObject.SetActive(false);
                break;
            case "HealthyEating":
                if (SaveData.Instance._PatientData.User_Diabetes_Info == "1")
                {
                    //DietaryInformations[2].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[2].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[2].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    User_Diabetes_Info = "1";
                }
                else
                {
                    //DietaryInformations[2].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[2].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[2].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    User_Diabetes_Info = "0";
                }
                DietaryInformations[2].transform.GetChild(0).gameObject.SetActive(true);
                DietaryInformations[2].transform.GetChild(1).gameObject.SetActive(false);
                break;
            case "Salt":
                if (SaveData.Instance._PatientData.User_Salt_Info == "1")
                {
                    //DietaryInformations[3].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[3].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[3].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    User_Salt_Info = "1";
                }
                else
                {
                    //DietaryInformations[3].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[3].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[3].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    User_Salt_Info = "0";

                }
                DietaryInformations[3].transform.GetChild(0).gameObject.SetActive(true);
                DietaryInformations[3].transform.GetChild(1).gameObject.SetActive(false);
                break;
            case "Fluid":
                if (SaveData.Instance._PatientData.User_Fluid_Info == "1")
                {
                    //DietaryInformations[4].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[4].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[4].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    User_Fluid_Info = "1";
                }
                else
                {
                    //DietaryInformations[4].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[4].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[4].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    User_Fluid_Info = "0";

                }
                DietaryInformations[4].transform.GetChild(0).gameObject.SetActive(true);
                DietaryInformations[4].transform.GetChild(1).gameObject.SetActive(false);
                break;
            case "Calorie":
                if (SaveData.Instance._PatientData.User_Calorie_Info == "1")
                {
                    //DietaryInformations[5].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[5].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[5].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    User_Calorie_Info = "1";
                }
                else
                {
                    //DietaryInformations[5].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[5].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[5].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    User_Calorie_Info = "0";
                }
                DietaryInformations[5].transform.GetChild(0).gameObject.SetActive(true);
                DietaryInformations[5].transform.GetChild(1).gameObject.SetActive(false);
                break;
        }
    }

    public void OnDietaryInformationClicked(string clickedDietaryButton)
    {
        switch (clickedDietaryButton)
        {
            case "Potassium":
                if (User_Potassium_Info == "0")
                {
                    //DietaryInformations[0].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[0].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    DietaryInformations[0].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[0].transform.GetChild(0).gameObject.SetActive(false);
                    DietaryInformations[0].transform.GetChild(1).gameObject.SetActive(true);
                    User_Potassium_Info = "1";
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower potassium diet. Only select this option if you have been advised to follow a low potassium diet by a healthcare professional."; //  For more information about Potassium " + url
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                    _dietaryInfoPopUp.SetActive(true);
                }
                else
                {
                    //DietaryInformations[0].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[0].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    DietaryInformations[0].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[0].transform.GetChild(0).gameObject.SetActive(true);
                    DietaryInformations[0].transform.GetChild(1).gameObject.SetActive(false);
                    User_Potassium_Info = "0";
                }
                break;
            case "Phosphate":
                if (User_Phosphate_Info == "0")
                {
                    //DietaryInformations[1].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[1].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    DietaryInformations[1].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[1].transform.GetChild(0).gameObject.SetActive(false);
                    DietaryInformations[1].transform.GetChild(1).gameObject.SetActive(true);
                    User_Phosphate_Info = "1";
                    _dietaryInfoPopUp_Phosphate.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower phosphate diet. Only select this option if you have been advised to follow a low phosphate diet by a healthcare professional. Phosphate additives are found in many foods eg cola drinks, processed meat, potato products, bakery goods, dairy foods. Check food labels for anything with ‘phos’ in the name. Avoiding these reduces the amount of phosphate you are eating."; //  For more information about Phosphate " + url
                    _dietaryInfoPopUp_Phosphate.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                    _dietaryInfoPopUp_Phosphate.SetActive(true);
                }
                else
                {
                    //DietaryInformations[1].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[1].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    DietaryInformations[1].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[1].transform.GetChild(0).gameObject.SetActive(true);
                    DietaryInformations[1].transform.GetChild(1).gameObject.SetActive(false);
                    User_Phosphate_Info = "0";
                }
                break;
            case "HealthyEating":
                if (User_Diabetes_Info == "0")
                {
                    //DietaryInformations[2].GetComponent<Image>().sprite = BlueDiabetes;   // SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[2].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    DietaryInformations[2].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[2].transform.GetChild(0).gameObject.SetActive(false);
                    DietaryInformations[2].transform.GetChild(1).gameObject.SetActive(true);
                    User_Diabetes_Info = "1";
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a Healthy Eating diet, which is also suitable for people with diabetes."; //  For more information about Healthy Eating " + url
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                    _dietaryInfoPopUp.SetActive(true);
                }
                else
                {
                    //DietaryInformations[2].GetComponent<Image>().sprite = WhiteDiabetes;  //SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[2].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    DietaryInformations[2].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[2].transform.GetChild(0).gameObject.SetActive(true);
                    DietaryInformations[2].transform.GetChild(1).gameObject.SetActive(false);
                    User_Diabetes_Info = "0";
                }
                break;
            case "Salt":
                if (User_Salt_Info == "0")
                {
                    //DietaryInformations[3].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[3].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    DietaryInformations[3].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[3].transform.GetChild(0).gameObject.SetActive(false);
                    DietaryInformations[3].transform.GetChild(1).gameObject.SetActive(true);
                    User_Salt_Info = "1";
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower salt diet. Try not to add salt during or after cooking.";   //  For more information about Salt " + url
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                    _dietaryInfoPopUp.SetActive(true);
                }
                else
                {
                    //DietaryInformations[3].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[3].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    DietaryInformations[3].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[3].transform.GetChild(0).gameObject.SetActive(true);
                    DietaryInformations[3].transform.GetChild(1).gameObject.SetActive(false);
                    User_Salt_Info = "0";

                }
                break;
            case "Fluid":
                if (User_Fluid_Info == "0")
                {
                    //DietaryInformations[4].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[4].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    DietaryInformations[4].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[4].transform.GetChild(0).gameObject.SetActive(false);
                    DietaryInformations[4].transform.GetChild(1).gameObject.SetActive(true);
                    User_Fluid_Info = "1";
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower fluid diet. Only select this option if you have been advised to follow a low fluid diet by a healthcare professional.";  //  For more information about Fluid " + url
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                    _dietaryInfoPopUp.SetActive(true);
                }
                else
                {
                    //DietaryInformations[4].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[4].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    DietaryInformations[4].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[4].transform.GetChild(0).gameObject.SetActive(true);
                    DietaryInformations[4].transform.GetChild(1).gameObject.SetActive(false);
                    User_Fluid_Info = "0";

                }
                break;
            case "Calorie":
                if (User_Calorie_Info == "0")
                {
                    //DietaryInformations[5].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 188@3x");
                    DietaryInformations[5].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    DietaryInformations[5].GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                    DietaryInformations[5].transform.GetChild(0).gameObject.SetActive(false);
                    DietaryInformations[5].transform.GetChild(1).gameObject.SetActive(true);
                    User_Calorie_Info = "1";
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods depending on whether they are high, medium or low in calories. To gain weight choose more high or medium options. To lose weight choose more low calorie options. Adding oil or spread during or after cooking will increase calories.";  //  For more information about Calorie " + url
                    _dietaryInfoPopUp.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                    _dietaryInfoPopUp.SetActive(true);
                }
                else
                {
                    //DietaryInformations[5].GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Rectangle 187@3x");
                    DietaryInformations[5].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                    DietaryInformations[5].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    DietaryInformations[5].transform.GetChild(0).gameObject.SetActive(true);
                    DietaryInformations[5].transform.GetChild(1).gameObject.SetActive(false);
                    User_Calorie_Info = "0";
                }
                break;
        }
    }
     
    public void OnUserProfilePage_ToggleClicked(Toggle toggle)
    {
        //if (!EditButton.gameObject.activeInHierarchy)
        //Debug.Log("1");
        switch (toggle.name)
        {
            case "YesToggle":
                if (toggle.isOn == false) return;
                //toggle.isOn = true;
                toggle.interactable = false;
                //_kidneyPopup_UserProfile.SetActive(true);
                YesToggleKidneyTrans.interactable = false;
                NoToggleKidneyTrans.interactable = true;
                break;
            case "NoToggle":
                //YesToggleKidneyTrans.interactable = true;
                YesToggleKidneyTrans.interactable = true;
                NoToggleKidneyTrans.interactable = false;
                break;
        }
        //}
    } 

    public void KidneyPopup_Clicked()
    {
        if (!EditButton.gameObject.activeInHierarchy)
        {
            if (!YesToggleKidneyTrans.isOn)
            {
                _kidneyPopup_UserProfile.SetActive(true);
            }
        }
    }

    public void OnUserProfilePage_OnCancelClicked()
    {
        YesToggleKidneyTrans.isOn = true;
        _kidneyPopup_UserProfile.SetActive(false);
    }

    public void OnUserProfilePage_OnHeightDropValueChanged(Dropdown dropdown)
    {
        /*dropDownVal_Height = dropdown.value;
        //string choosedHeighttype = dropdown.options[dropdown.value].text;
        if (dropdown.value == 0)
        {
            //heightText.placeholder.GetComponent<Text>().text = "Height: eg 5.10";
            //PlayerPrefs.SetInt("HeightDropDownValue", dropdown.value);
            //Debug.Log(dropdown.value);
        }
        else if (dropdown.value == 1)
        {
            //heightText.placeholder.GetComponent<Text>().text = "Height: eg 157";
            //PlayerPrefs.SetInt("HeightDropDownValue", dropdown.value);
            //Debug.Log(dropdown.value);
        } */
    }
    
    public void OnUserProfilePage_OnWeightDropValueChanged(Dropdown dropdown)
    {
        dropDownVal_Weight = dropdown.value;
        //string choosedWeighttype = dropdown.options[dropdown.value].text;
        if (dropdown.value == 0)
        {
            DietaryInfoDropDown[2].value = 0;
        }
        else if (dropdown.value == 1)
        {
            DietaryInfoDropDown[2].value = 1;
        }
    }

    public void OnProfileEditClicked()
    {
        StartCoroutine(ResetScrollRect2());

        ScreenTitle.text = "Edit Profile";
        ScreenEditInformation.SetActive(true);
        FullNameText.gameObject.SetActive(false);
        FirstNameText.gameObject.SetActive(true);
        LastNameText.gameObject.SetActive(true);
        _textLineHeight.SetActive(true);
        _textLineWeight.SetActive(true);
        _textLineWeightPast.SetActive(true);
        _arrowHeight.SetActive(true);
        _arrowWeight.SetActive(true);
        _labelHeight.transform.localPosition = new Vector3(-30f, _arrowHeight.transform.localPosition.y, _arrowHeight.transform.localPosition.z);
        //_labelWeight.transform.localPosition = new Vector3(-30f, _arrowHeight.transform.localPosition.y, _arrowHeight.transform.localPosition.z);
        ChoosePhotoOption.SetActive(true);
        //FirstNameText.interactable = true;

        EditButton.gameObject.SetActive(false);
        //BackButton.gameObject.SetActive(false);
        UpdateButton.gameObject.SetActive(true);
        YesToggleLoseWeight.interactable = true;
        NoToggleLoseWeight.interactable = true;
        YesToggleKidneyTrans.interactable = true;
        NoToggleKidneyTrans.interactable = true;
        ProfileButton.interactable = true;
         

        for (int i=0; i < DietaryInformations.Length; i++)
        {
            DietaryInformations[i].GetComponent<Button>().interactable = true;
            //OnDietaryInformationClicked(DietaryInformations[i].name);
        }

        if(User_Potassium_Info == "1")
        {
            DietaryInformations[0].transform.GetChild(0).gameObject.SetActive(false);
            DietaryInformations[0].transform.GetChild(1).gameObject.SetActive(true);
        }
        if (User_Phosphate_Info == "1")
        {
            DietaryInformations[1].transform.GetChild(0).gameObject.SetActive(false);
            DietaryInformations[1].transform.GetChild(1).gameObject.SetActive(true);
        }
        if (User_Diabetes_Info == "1")
        {
            DietaryInformations[2].transform.GetChild(0).gameObject.SetActive(false);
            DietaryInformations[2].transform.GetChild(1).gameObject.SetActive(true);
        }
        if (User_Salt_Info == "1")
        {
            DietaryInformations[3].transform.GetChild(0).gameObject.SetActive(false);
            DietaryInformations[3].transform.GetChild(1).gameObject.SetActive(true);
        }
        if (User_Fluid_Info == "1")
        {
            DietaryInformations[4].transform.GetChild(0).gameObject.SetActive(false);
            DietaryInformations[4].transform.GetChild(1).gameObject.SetActive(true);
        }
        if (User_Calorie_Info == "1")
        {
            DietaryInformations[5].transform.GetChild(0).gameObject.SetActive(false);
            DietaryInformations[5].transform.GetChild(1).gameObject.SetActive(true);
        }
        HeightText.GetComponent<InputField>().interactable = true;
        WeightText.GetComponent<InputField>().interactable = true;
        WeightPastText.GetComponent<InputField>().interactable = true;

        for (int i = 0; i < DietaryInfoDropDown.Length - 1; i++)
        {
            DietaryInfoDropDown[i].interactable = true;
        }
    }

    public void OnBackFromEditPageClicked()
    {
        string lastOpenedScreenID = ScreenManager.Instance.backStack[ScreenManager.Instance.backStack.Count - 1];
        //FirstNameText.interactable = false;
        FullNameText.text = SaveData.Instance._PatientData.User_Name;
        FirstNameText.text = SaveData.Instance._PatientData.User_FirstName;
        LastNameText.text = SaveData.Instance._PatientData.User_LastName;
        FullNameText.gameObject.SetActive(true);
        FirstNameText.gameObject.SetActive(false);
        LastNameText.gameObject.SetActive(false);
        
        ErrorMessageTxt.gameObject.SetActive(false);
        DietaryErrorMessageTxt.gameObject.SetActive(false);
        ChoosePhotoOption.SetActive(false);

        if (!EditButton.isActiveAndEnabled)
        {
            OnProfileScreenShowed();
            EditButton.gameObject.SetActive(true);
        }
        else
        {
            if (lastOpenedScreenID == "MenuScreen")
            {
                ScreenManager.Instance.Show("MenuScreen");
            }
            else
            {
                ScreenManager.Instance.Back();
                //ScreenManager.Instance.Show("ChatbotHomeScreen"); 
            }
            //ScreenManager.Instance.Show("ChatbotHomeScreen");
            StartCoroutine(ResetScrollRect());
        }
    }

    private IEnumerator ResetScrollRect()
    {
        yield return new WaitForSeconds(0.6f);
        if (ProfileContainerRect.verticalNormalizedPosition < 1)
            ProfileContainerRect.verticalNormalizedPosition = 2f;
    }

    private IEnumerator ResetScrollRect2()
    {
        if (ProfileContainerRect.verticalNormalizedPosition < 1)
        {
            ProfileContainerRect.movementType = ScrollRect.MovementType.Clamped;
            ProfileContainerRect.verticalNormalizedPosition = 2f;
        }
        yield return new WaitForSeconds(0.25f);
        ProfileContainerRect.movementType = ScrollRect.MovementType.Elastic;
    }

    public void OnProfileUpdateClicked()
    {
        
        if ((User_Potassium_Info == "0" && User_Phosphate_Info == "0" &&
            User_Diabetes_Info == "0" && User_Salt_Info == "0" &&
            User_Fluid_Info == "0" && User_Calorie_Info == "0") ||
            (string.IsNullOrEmpty(FirstNameText.text) ||
            string.IsNullOrEmpty(WeightPastText.text) || string.IsNullOrEmpty(HeightText.text) ||string.IsNullOrEmpty(WeightText.text)))
        {
            if (User_Potassium_Info == "0" && User_Phosphate_Info == "0" &&
            User_Diabetes_Info == "0" && User_Salt_Info == "0" &&
            User_Fluid_Info == "0" && User_Calorie_Info == "0")
            {
                DietaryErrorMessageTxt.gameObject.SetActive(true);
            }
            else
            {
                DietaryErrorMessageTxt.gameObject.SetActive(false);
            }

            if (string.IsNullOrEmpty(FirstNameText.text) || string.IsNullOrEmpty(HeightText.text) ||
            string.IsNullOrEmpty(WeightPastText.text) || string.IsNullOrEmpty(WeightText.text))
            {

                ErrorMessageTxt.gameObject.SetActive(true);
            }
            else
            {
                ErrorMessageTxt.gameObject.SetActive(false);
            }
        }
        else
        {
            StartCoroutine(ResetScrollRect2());
            SaveData.Instance._PatientData.User_Name = FirstNameText.text + " " + LastNameText.text;
            SaveData.Instance._PatientData.User_FirstName = FirstNameText.text;
            SaveData.Instance._PatientData.User_LastName = LastNameText.text;
            FullNameText.text = SaveData.Instance._PatientData.User_Name;
            FullNameText.gameObject.SetActive(true);
            FirstNameText.gameObject.SetActive(false);
            LastNameText.gameObject.SetActive(false);
            _textLineHeight.SetActive(false);
            _textLineWeight.SetActive(false);
            _textLineWeightPast.SetActive(false);
            _arrowHeight.SetActive(false);
            _arrowWeight.SetActive(false);
            _labelHeight.transform.localPosition = new Vector3(0, _arrowHeight.transform.localPosition.y, _arrowHeight.transform.localPosition.z);
            _labelWeight.transform.localPosition = new Vector3(0, _arrowHeight.transform.localPosition.y, _arrowHeight.transform.localPosition.z);
            //FirstNameText.interactable = false;
            ErrorMessageTxt.gameObject.SetActive(false);
            DietaryErrorMessageTxt.gameObject.SetActive(false);
            double weight6Month = double.Parse(WeightPastText.text);
            double userWeight = double.Parse(WeightText.text);
            //double averageWeightLose = Math.Abs(((weight6Month - userWeight) % weight6Month));
            double averageWeightLose = Math.Abs(((weight6Month - userWeight) / weight6Month) * 100);

            //if (HeightText.text == "")
            //    HeightText.text = "0";

            if (userWeight < weight6Month && averageWeightLose > 5 && !YesToggleLoseWeight.isOn)
            {
                _weightConcernPopUp.SetActive(true);
            }

            BackButton.gameObject.SetActive(true);
            EditButton.gameObject.SetActive(true);
            UpdateButton.gameObject.SetActive(false);
            ChoosePhotoOption.SetActive(false);

            ScreenTitle.text = "Profile";
            ScreenEditInformation.SetActive(false);

            HeightText.GetComponent<InputField>().interactable = false;
            WeightText.GetComponent<InputField>().interactable = false;
            WeightPastText.GetComponent<InputField>().interactable = false;

            for (int i = 0; i < DietaryInfoDropDown.Length - 1; i++)
            {
                DietaryInfoDropDown[i].interactable = false;
            }

            YesToggleLoseWeight.interactable = false;
            NoToggleLoseWeight.interactable = false;
            YesToggleKidneyTrans.interactable = false;
            NoToggleKidneyTrans.interactable = false;
            UpdateButton.gameObject.SetActive(false);
            ProfileButton.interactable = false;

            PlayerPrefs.SetInt("HeightDropDownValue", dropDownVal_Height);
            PlayerPrefs.SetInt("WeightDropDownValue", dropDownVal_Weight);

            //SaveData.Instance._PatientData.User_Name = FirstNameText.text;
            SaveData.Instance._PatientData.User_Height = HeightText.text;
            SaveData.Instance._PatientData.User_Weight = WeightText.text;
            SaveData.Instance._PatientData.User_Weight_6_Month = WeightPastText.text;

            SaveData.Instance._PatientData.User_Potassium_Info = User_Potassium_Info;
            SaveData.Instance._PatientData.User_Phosphate_Info = User_Phosphate_Info;
            SaveData.Instance._PatientData.User_Diabetes_Info = User_Diabetes_Info;
            SaveData.Instance._PatientData.User_Salt_Info = User_Salt_Info;
            SaveData.Instance._PatientData.User_Fluid_Info = User_Fluid_Info;
            SaveData.Instance._PatientData.User_Calorie_Info = User_Calorie_Info;

            SaveData.Instance._PatientData.User_Lose_Weight = (YesToggleLoseWeight.isOn ? "1" : "0");
            SaveData.Instance._PatientData.User_Kidney_Info = (YesToggleKidneyTrans.isOn ? "1" : "0");

           SaveData.Instance.SavePatientDataToLocal(SaveData.Instance._PatientData);
            SaveData.Instance.UpdateJSONDataToServer("");

            for (int i = 0; i < DietaryInformations.Length; i++)
            {
                DietaryInformations[i].GetComponent<Button>().interactable = false;
                OnDietaryInformationLoad(DietaryInformations[i].name);

            }
        }

        ////Save User Profile Image locally on device
        if (UserProfilePhoto.texture != null)
        {
            //byte[] texAsByte = tempTexture2D.EncodeToJPG();
            /*            savedProfImagePath = Path.Combine(Application.persistentDataPath, "images" + "UserProfileImage" + ".png");
                        File.WriteAllBytes(savedProfImagePath, tempTexture2D.EncodeToPNG());
                        NativeGallery.SaveImageToGallery(savedProfImagePath, "images", Path.GetFileName(savedProfImagePath)); */

            tempTexture2D =  UserProfilePhoto.texture.ToTexture2D();
            byte[] pngImageByteArray = tempTexture2D.EncodeToPNG();
            string tempPath = Path.Combine(Application.persistentDataPath, "");
            tempPath = Path.Combine(tempPath, "UserProfileImage.png");
            PlayerPrefs.SetString("UserProfileImageExist", tempPath);

            File.WriteAllBytes(tempPath, pngImageByteArray);

        }
    }

    private void OnSavedCallback(string savedString)
    {
        Debug.Log("ON SAVED IMAGE TO THE GALLERY......."+ savedString);
    }

    WWW fileReq;
    IEnumerator RequestImage()
    {
        //Debug.Log("Started Loading image from : " + Application.persistentDataPath + "/UserProfileImage.png");
        fileReq = new WWW("file://" + Application.persistentDataPath + "/UserProfileImage.png");
        while (!fileReq.isDone)
        {
            yield return null;
        }
        yield break;
    }
}

//This is to stop image stretch on Raw Image.
static class CanvasExtensions
{
    public static Vector2 SizeToParent(this RawImage image, float padding = 0)
    {
        float w = 0, h = 0;
        var parent = image.GetComponentInParent<RectTransform>();
        var imageTransform = image.GetComponent<RectTransform>();

        // check if there is something to do
        if (image.texture != null)
        {
            if (!parent) { return imageTransform.sizeDelta; } //if we don't have a parent, just return our current width;
            padding = 1 - padding;
            float ratio = image.texture.width / (float)image.texture.height;
            var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
            if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90)
            {
                //Invert the bounds if the image is rotated
                bounds.size = new Vector2(bounds.height, bounds.width);
            }
            //Size by height first
            h = bounds.height * padding;
            w = h * ratio;
            if (w > bounds.width * padding)
            { //If it doesn't fit, fallback to width;
                w = bounds.width * padding;
                h = w / ratio;
            }
        }
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
        return imageTransform.sizeDelta;
    }
}