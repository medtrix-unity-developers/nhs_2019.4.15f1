﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using PaperPlaneTools;
using System.Text.RegularExpressions;

public class RegistrationScreen : MonoSingleton<RegistrationScreen>
{
    //public SaveData saveData;
    public GameObject page1;
    public GameObject page2;
    public GameObject page3;
    public GameObject backButton;
    
    [SerializeField] private Text firstNameText;
    [SerializeField] private Text secondNameText;
    [SerializeField] private Text emailText;
    [SerializeField] private InputField heightText;
    [SerializeField] private InputField weightText;
    //[SerializeField] private bool userLoseWeight;
    [SerializeField] private InputField userWeight6Month;
    [SerializeField] private string userPotassiumInfo;
    [SerializeField] private string userPhosphateInfo;
    [SerializeField] private string userDiabetesInfo;
    [SerializeField] private string userSaltInfo;
    [SerializeField] private string userFluidInfo;
    [SerializeField] private string userCalorieInfo;
    //[SerializeField] private string userNHSTrust;
    //[SerializeField] private string userKidneyInfo;
    [SerializeField] private Toggle[] page2Toggles;
    [SerializeField] private Toggle page3AgreeToggle;


    [SerializeField] private Toggle YesToggleLoseWeight;
    [SerializeField] private Toggle NoToggleLoseWeight;
    [SerializeField] private Toggle YesToggleKidneyTrans;
    [SerializeField] private Toggle NoToggleKidneyTrans;
      

    //Error message text field.
    [SerializeField] private Text NameErrorMessageTxt;
    [SerializeField] private Text EmailErrorMessageTxt;
    [SerializeField] private Text WeightErrorMessageTxt;
    [SerializeField] private Text PastWeightErrorMessageTxt;
    [SerializeField] private Text AgreeErrorMessageTxt;
    [SerializeField] private Text Page2ErrorMessage;

    [SerializeField] private Text UserExistsErrorMessageTxt;

    [SerializeField] private Dropdown PastWeightDropDown;

    [SerializeField] private ScreenController screencontroller;
    [SerializeField] public GameObject KidneyPopup;
    [SerializeField] public GameObject DietaryInfoPopup;
    [SerializeField] public GameObject WeightConcernPopup;

    //Page 2 check boxes Textboxes 
    [SerializeField] private Text PotassiumTitlePage2Txt;
    [SerializeField] private Text PhosphateTitlePage2Txt;
    [SerializeField] private Text HealthyTitleEatingPage2Txt;
    [SerializeField] private Text SaltTitlePage2Txt;
    [SerializeField] private Text FluidTitlePage2Txt;
    [SerializeField] private Text CalorieTitlePage2Txt;

    [SerializeField] private GameObject PotassiumInfoPage2Txt;
    [SerializeField] private GameObject PhosphateInfoPage2Txt;
    [SerializeField] private GameObject HealthyEatingInfoPage2Txt;
    [SerializeField] private GameObject SaltInfoPage2Txt;
    [SerializeField] private GameObject FluidInfoPage2Txt;
    [SerializeField] private GameObject CalorieInfoPage2Txt;



    // Start is called before the first frame update
    void Awake()
    {
        screencontroller.OnShowStarted += OnShowScreenStarted;
        SaveData.Instance.userAlreadyExistsCallback += OnUserAlreadyExists;
    }

    private void OnShowScreenStarted()
    {
        Page2ErrorMessage.gameObject.SetActive(false);

        string FilePath = Path.Combine(Application.persistentDataPath, "PatientData.json");
        if (File.Exists(FilePath))
        {
            ScreenManager.Instance.Show("ChatbotHomeScreen");
        }
    }

    public void OnNextClicked(GameObject nextPage)
    {
        int toggleCount = 0;
         
        if (nextPage.name == "Page_2")
        {
            bool emailValidation = false;

            if (string.IsNullOrEmpty(firstNameText.text))
            {
                NameErrorMessageTxt.gameObject.SetActive(true);
                //return;
            }
            else
            {
                NameErrorMessageTxt.gameObject.SetActive(false);
                SaveData.Instance._PatientData.User_Name = firstNameText.text + " "+secondNameText.text;
            }

            if (string.IsNullOrEmpty(emailText.text))
            {
                EmailErrorMessageTxt.gameObject.SetActive(true);
                //return;
            }
            else
            {
                emailValidation = ValidateEmail();
                if (emailValidation)
                {
                    EmailErrorMessageTxt.gameObject.SetActive(false);
                    SaveData.Instance._PatientData.User_EmailId = emailText.text;
                }else
                {
                    EmailErrorMessageTxt.gameObject.SetActive(true);
                    //return;
                }
            }
             
            if (string.IsNullOrEmpty(weightText.text))
            {
                WeightErrorMessageTxt.gameObject.SetActive(true);
                //return;
            }
            else
            {
                WeightErrorMessageTxt.gameObject.SetActive(false);
                SaveData.Instance._PatientData.User_Weight = weightText.text;
            }

            if (string.IsNullOrEmpty(userWeight6Month.text))
            {
                PastWeightErrorMessageTxt.gameObject.SetActive(true);
                //return;
            }
            else
            {
                if (string.IsNullOrEmpty(firstNameText.text) || string.IsNullOrEmpty(emailText.text) || string.IsNullOrEmpty(weightText.text) || string.IsNullOrEmpty(userWeight6Month.text) || !emailValidation)
                    return;

                PastWeightErrorMessageTxt.gameObject.SetActive(false);
                double weight6Month = double.Parse(userWeight6Month.text); 
                double userWeight = double.Parse(weightText.text);
                double averageWeightLose = Math.Abs(((weight6Month - userWeight) / weight6Month) * 100 );

                //Debug.Log("Weight Loss : "+" "+ weight6Month+" " + userWeight+"  " +averageWeightLose);
                 
                if (userWeight < weight6Month && averageWeightLose > 5 &&  !YesToggleLoseWeight.isOn)
                {
                    WeightConcernPopup.gameObject.SetActive(true);
                    SaveData.Instance._PatientData.User_Weight_6_Month = userWeight6Month.text;
                    return;
                    //Debug.Log("It looks like you have lost a lot of weight recently.You should discuss this with your dietician or doctor");
                }
                SaveData.Instance._PatientData.User_Weight_6_Month = userWeight6Month.text;
            }

            if (string.IsNullOrEmpty(firstNameText.text) || string.IsNullOrEmpty(emailText.text) || string.IsNullOrEmpty(weightText.text) || string.IsNullOrEmpty(userWeight6Month.text) || !emailValidation)
                return;

            SaveData.Instance._PatientData.User_Lose_Weight = (YesToggleLoseWeight.isOn ? "1" : "0");
            page1.SetActive(false);
            page2.SetActive(false);
            page3.SetActive(false);
            nextPage.SetActive(true);
            backButton.SetActive(true);
        }

        if (nextPage.name == "Page_3")
        {
            Page2ErrorMessage.gameObject.SetActive(false);
            toggleCount = 0;
            foreach (Toggle gameObj in page2Toggles)
            {
                if(gameObj.isOn)
                {
                    toggleCount++;
                }
            }
            if (toggleCount < 1)
            {
                Page2ErrorMessage.gameObject.SetActive(true);
                return;
            }
            else
            {
                Page2ErrorMessage.gameObject.SetActive(false);

                page1.SetActive(false);
                page2.SetActive(false);
                page3.SetActive(false);
                nextPage.SetActive(true);
                backButton.SetActive(true);
            }
        }
    }

    private bool ValidateEmail()
    {
        string email = emailText.text;
        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match match = regex.Match(email);
        if (match.Success)
        {
            EmailErrorMessageTxt.gameObject.SetActive(false);
            return true;
        }
        else
        {
            EmailErrorMessageTxt.gameObject.SetActive(true);
            return false;
        }
    }
 
    public void OnPageNoClicked(GameObject gotoPage)
    {
        page1.SetActive(false);
        page2.SetActive(false);
        page3.SetActive(false);
        gotoPage.SetActive(true);
    }

    public void OnRegisterClicked()
    {
        if (page3AgreeToggle.isOn == false)
        {
            AgreeErrorMessageTxt.gameObject.SetActive(true);
            return;
        }
        SaveData.Instance._PatientData.User_Kidney_Info = (YesToggleKidneyTrans.isOn ? "1" : "0");
        AgreeErrorMessageTxt.gameObject.SetActive(false);

        //saveData.SavePatientDataToLocal();
        SaveData.Instance.SendJSONDataToServer("");
 
        //ScreenManager.Instance.Show("DieticianCustomScreen");
    }

    public void OnInformationClicked(string informationName)
    {
        //ToggleAnswerButton.Instance.clickedInformationName = "Answer_0"; //  OnAnswerButtonClicked("Answer_0");
         ScreenManager.Instance.Show("InformationsScreen");
    }

    public void OnPage_1_ToggleClicked(Toggle toggle)
    {
        /*if (toggle.isOn == true)
        {
            toggle.transform.GetChild(1).GetComponent<Text>().color = new Color(0.46f, 0.46f, 1f,1f);
        }else
        {
            toggle.transform.GetChild(1).GetComponent<Text>().color = new Color(0f, 0f, 0f,1f);
        } */
    }

    public void OnPage_2_ToggleClicked(Toggle toggle)  
    {
        string url = "";
        if (toggle.isOn == false)
        {
            switch (toggle.name)
            {
                case "Potassium":
                    SaveData.Instance._PatientData.User_Potassium_Info = "0";
                    PotassiumTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    PotassiumTitlePage2Txt.fontStyle = FontStyle.Normal;
                    PotassiumInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");
                    break;
                case "Phosphate":
                    SaveData.Instance._PatientData.User_Phosphate_Info = "0";
                    PhosphateTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    PhosphateTitlePage2Txt.fontStyle = FontStyle.Normal;
                    PhosphateInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");

                    break;
                case "HealthyEating":
                    SaveData.Instance._PatientData.User_Diabetes_Info = "0";
                    HealthyTitleEatingPage2Txt.color = new Color32(66, 85, 99, 255);
                    HealthyTitleEatingPage2Txt.fontStyle = FontStyle.Normal;
                    HealthyEatingInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");

                    break;
                case "Salt":
                    SaveData.Instance._PatientData.User_Salt_Info = "0";
                    SaltTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    SaltTitlePage2Txt.fontStyle = FontStyle.Normal;
                    SaltInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");

                    break;
                case "Fluid":
                    SaveData.Instance._PatientData.User_Fluid_Info = "0";
                    FluidTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    FluidTitlePage2Txt.fontStyle = FontStyle.Normal;
                    FluidInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");

                    break;
                case "Calorie":
                    
                    SaveData.Instance._PatientData.User_Calorie_Info = "0";
                    CalorieTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    CalorieTitlePage2Txt.fontStyle = FontStyle.Normal;
                    CalorieInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");
                    break;
            }
            return;
        }
        //toggle.transform.GetChild(2).GetComponent<Text>().color = new Color32(46, 114, 199, 255);
        //toggle.transform.GetChild(3).GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

        // Showing popup after clicked on checkbox button.
        switch (toggle.name)
        {
            case "Potassium":
                SaveData.Instance._PatientData.User_Potassium_Info = "1";
                PotassiumTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                PotassiumTitlePage2Txt.fontStyle = FontStyle.Bold;
                PotassiumInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format ("<link=Potassium><color=black><b><u>click here.</u></b></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower potassium diet. Only select this option if you have been advised to follow a low potassium diet by a healthcare professional."; //  For more information about Potassium " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);
                 break;
            case "Phosphate":
                SaveData.Instance._PatientData.User_Phosphate_Info = "1";
                PhosphateTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                PhosphateTitlePage2Txt.fontStyle = FontStyle.Bold;
                PhosphateInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Phosphate><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower Phosphate diet. Only select this option if you have been advised to follow a low phosphate diet by a healthcare professional. Phosphate additives are found in many foods eg cola drinks, processed meat, potato products, bakery goods, dairy foods. Check food labels for anything with ‘phos’ in the name. Avoiding these reduces the amount of phosphate you are eating."; //  For more information about Phosphate " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 20;
                DietaryInfoPopup.SetActive(true);
 
                break;
            case "HealthyEating":
                SaveData.Instance._PatientData.User_Diabetes_Info = "1";
                HealthyTitleEatingPage2Txt.color = new Color32(46, 114, 199, 255);
                HealthyTitleEatingPage2Txt.fontStyle = FontStyle.Bold;
                HealthyEatingInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");
 
                url = string.Format("<link=HealthyEating><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a Healthy Eating diet. Only select this option if you have been advised to follow a Healthy Eating diet by a healthcare professional."; //  For more information about Healthy Eating " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);
 
                break;
            case "Salt":
                SaveData.Instance._PatientData.User_Salt_Info = "1";
                SaltTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                SaltTitlePage2Txt.fontStyle = FontStyle.Bold;
                SaltInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Salt><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower Salt diet. Only select this option if you have been advised to follow a low Salt diet by a healthcare professional.";   //  For more information about Salt " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);
 
                break;
            case "Fluid":
                SaveData.Instance._PatientData.User_Fluid_Info = "1";
                FluidTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                FluidTitlePage2Txt.fontStyle = FontStyle.Bold;
                FluidInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Fluid><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a higher Fluid diet. Only select this option if you have been advised to follow a high Fluid diet by a healthcare professional.";  //  For more information about Fluid " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);
                 break;
            case "Calorie":
                SaveData.Instance._PatientData.User_Calorie_Info = "1";
                CalorieTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                CalorieTitlePage2Txt.fontStyle = FontStyle.Bold;
                CalorieInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Calorie><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower Calorie diet. Only select this option if you have been advised to follow a low Calorie diet by a healthcare professional.";  //  For more information about Calorie " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);
                break;
        }
    }

    public void OnPage_3_ToggleClicked(Toggle toggle)
    {
        switch (toggle.name)
        {
            case "YesToggle":
                if (toggle.isOn == false) return;
                toggle.isOn = true;
                KidneyPopup.SetActive(true);
                break;

            case "NoToggle":
                break;
        }
    }

    public void OnWeightAlertClicked()
    {
        SaveData.Instance._PatientData.User_Lose_Weight = (YesToggleLoseWeight.isOn ? "1" : "0");
        page1.SetActive(false);
        page2.SetActive(true);
        page3.SetActive(false);
        backButton.SetActive(true);
    }
     

    public void OnCallClicked()
    {
        YesToggleKidneyTrans.isOn = true;
        KidneyPopup.SetActive(false);
        //Debug.Log("On Call Clicked...");
        Application.OpenURL("tel://01174145428");   // telephone number = 0117 4145428/9
    }

    public void OnCancelClicked()
    {
        YesToggleKidneyTrans.isOn = true;
        KidneyPopup.SetActive(false);
        DietaryInfoPopup.SetActive(false);
    }

    public void OnEmailClicked()
    {
        YesToggleKidneyTrans.isOn = true;
        KidneyPopup.SetActive(false);
        //Debug.Log("On Email  Clicked...");
        string email = "renaldietitians@nbt.nhs.uk";
        string subject = MyEscapeURL("");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void OnHeightDropValueChanged(Dropdown dropdown)
    {
        //string choosedHeighttype = dropdown.options[dropdown.value].text;
        if(dropdown.value == 0)
        {
            heightText.placeholder.GetComponent<Text>().text = "Height: eg 5.10";
            PlayerPrefs.SetInt("HeightDropDownValue", dropdown.value);
        }else if(dropdown.value == 1)
        {
            heightText.placeholder.GetComponent<Text>().text = "Height: eg 157";
            PlayerPrefs.SetInt("HeightDropDownValue", dropdown.value);
        }
    }

    public void OnWeightDropValueChanged(Dropdown dropdown)
    {
        //string choosedWeighttype = dropdown.options[dropdown.value].text;
        if (dropdown.value == 0)
        {
            weightText.placeholder.GetComponent<Text>().text = "Weight: eg 9.4";
            PastWeightDropDown.value = 0;
            PlayerPrefs.SetInt("WeightDropDownValue", dropdown.value);
            //userWeight6Month.placeholder.GetComponent<Text>().text = "Weight (Ex: 75 kgs)";
        }
        else if (dropdown.value == 1)
        {
            weightText.placeholder.GetComponent<Text>().text = "Weight: eg 75";
            PastWeightDropDown.value = 1;
            PlayerPrefs.SetInt("WeightDropDownValue", dropdown.value);
            //userWeight6Month.placeholder.GetComponent<Text>().text = "Weight (Ex: 11.8 stone)";
        }
    }
     


    public void OnRegisterBackClicked()
    {
        if (page2.activeSelf)
        {
            backButton.SetActive(false);
            page1.SetActive(true);
            page2.SetActive(false);
        }
        else if(page3.activeSelf)
        {
            page3.SetActive(false);
            page2.SetActive(true);
            backButton.SetActive(true);
        }
    }

    public void OnUserAlreadyExists()
    {
        UserExistsErrorMessageTxt.gameObject.SetActive(true);
    }

    public void OnFirstNameEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnFirstNameEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }


    public void OnLastNameEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnLastNameEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }


    public void OnEmailEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnEmailEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }

    public void OnHeightEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnHeightEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }

    public void OnWeightEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnWeightEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }

    public void OnWeight_Past_Edit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnWeight_Past_EndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }
}
