﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using PaperPlaneTools;
using System.Text.RegularExpressions;

public class Registration1 : MonoBehaviour  //MonoSingleton<RegistrationScreen>
{
    [SerializeField] private ScreenController screencontroller;

    [SerializeField] private Text firstNameText;
    [SerializeField] private Text secondNameText;
    [SerializeField] private Text emailText;
    [SerializeField] private InputField heightText;
    [SerializeField] private InputField weightText;
    [SerializeField] private InputField userWeight6Month;
    //public GameObject backButton;

    [SerializeField] private Toggle YesToggleLoseWeight;
    [SerializeField] private Toggle NoToggleLoseWeight;

    //Error message text field.
    [SerializeField] private Text NameErrorMessageTxt;
    [SerializeField] private Text EmailErrorMessageTxt;
    [SerializeField] private Text WeightErrorMessageTxt;
    [SerializeField] private Text PastWeightErrorMessageTxt;
    [SerializeField] private Text HeightErrorMessageTxt;
    [SerializeField] private Text UserExistsErrorMessageTxt;
    [SerializeField] GameObject NextButton;


    [SerializeField] private Dropdown PastWeightDropDown;
    [SerializeField] public GameObject WeightConcernPopup;
    
    //To clear the value:
    [SerializeField] private InputField firstNameInput;
    [SerializeField] private InputField secondNameInput;
    [SerializeField] private InputField emailTextInput;


    [SerializeField] private GameObject firstNameBorder;
    [SerializeField] private GameObject SecondNameBorder;
    [SerializeField] private GameObject emailBorder;
    [SerializeField] private GameObject heightBorder;
    [SerializeField] private GameObject weightBorder;
    [SerializeField] private GameObject userWeight6MonthBorder;

    //[SerializeField] private ScrollRect RegistrationContainerRect;


    void Awake()
    {
        screencontroller.OnShowStarted += OnShowScreenStarted;
    }

    private void OnShowScreenStarted()
    {
        NameErrorMessageTxt.gameObject.SetActive(false);
        EmailErrorMessageTxt.gameObject.SetActive(false);
        WeightErrorMessageTxt.gameObject.SetActive(false);
        PastWeightErrorMessageTxt.gameObject.SetActive(false);
        HeightErrorMessageTxt.gameObject.SetActive(false);
        UserExistsErrorMessageTxt.gameObject.SetActive(false);

        //RegistrationContainerRect.verticalNormalizedPosition = 2f;

    }
    /*public void OnWeightPastEditStarted()
    {
        if(RegistrationContainerRect.verticalNormalizedPosition >= 1f)
            RegistrationContainerRect.verticalNormalizedPosition = -2f;

    }

    public void OnWeightPastEditEnded()
    {
        RegistrationContainerRect.verticalNormalizedPosition = 2f;

    } */
  

    public void OnResetFields()
    {
        firstNameInput.text = "";
        secondNameInput.text = "";
        emailTextInput.text = "";
        heightText.text = "";
        weightText.text = "";
        userWeight6Month.text = "";

        firstNameInput.placeholder.gameObject.SetActive(true);
        secondNameInput.placeholder.gameObject.SetActive(true);
        emailTextInput.placeholder.gameObject.SetActive(true);
        heightText.placeholder.gameObject.SetActive(true);
        weightText.placeholder.gameObject.SetActive(true);
        userWeight6Month.placeholder.gameObject.SetActive(true);


        firstNameBorder.SetActive(false);
        SecondNameBorder.SetActive(false);
        emailBorder.SetActive(false);
        heightBorder.SetActive(false);
        weightBorder.SetActive(false);
        userWeight6MonthBorder.SetActive(false);
    }


    public void OnNextClicked()
    {
        NextButton.GetComponent<Button>().interactable = false;
        if (!string.IsNullOrEmpty(emailTextInput.text))
        {
            //NextButton.GetComponent<Button>().interactable = false;
            SaveData.Instance.RB_SendJSONDataToServer_UserEmail("", emailTextInput.text);
        }
        else
        {
            OnceEmailExistanceVerified(false);
        }
         
    }

    public void OnceEmailExistanceVerified(bool userExists)
    {
        NextButton.GetComponent<Button>().interactable = true;

        bool emailValidation = false;

        if (string.IsNullOrEmpty(heightText.text))
        {
            HeightErrorMessageTxt.gameObject.SetActive(true);
        }
        else
        {
            HeightErrorMessageTxt.gameObject.SetActive(false);
            SaveData.Instance._PatientData.User_Height = heightText.text;
        }


        if (string.IsNullOrEmpty(firstNameText.text))
        {
            NameErrorMessageTxt.gameObject.SetActive(true);
            //return;
        }
        else
        {
            NameErrorMessageTxt.gameObject.SetActive(false);
            SaveData.Instance._PatientData.User_Name = firstNameText.text + " " + secondNameText.text;
            SaveData.Instance._PatientData.User_FirstName = firstNameText.text;
            SaveData.Instance._PatientData.User_LastName = secondNameText.text;
        }

        if (string.IsNullOrEmpty(emailTextInput.text))
        {
            EmailErrorMessageTxt.gameObject.SetActive(true);
            UserExistsErrorMessageTxt.gameObject.SetActive(false);
            //return;
        }
        else
        {
            emailValidation = ValidateEmail();
            if (emailValidation)
            {
                EmailErrorMessageTxt.gameObject.SetActive(false);

                if (userExists)
                {
                    EmailErrorMessageTxt.gameObject.SetActive(false);
                    UserExistsErrorMessageTxt.gameObject.SetActive(true);
                }
                else
                {
                    UserExistsErrorMessageTxt.gameObject.SetActive(false);
                    SaveData.Instance._PatientData.User_EmailId = emailTextInput.text;
                    //return;
                }
            }
            else
            {
                EmailErrorMessageTxt.gameObject.SetActive(true);
            }

            
        }

        if (string.IsNullOrEmpty(weightText.text))
        {
            WeightErrorMessageTxt.gameObject.SetActive(true);
            //return;
        }
        else
        {
            WeightErrorMessageTxt.gameObject.SetActive(false);
            SaveData.Instance._PatientData.User_Weight = weightText.text;
        }

        if (string.IsNullOrEmpty(userWeight6Month.text))
        {
            PastWeightErrorMessageTxt.gameObject.SetActive(true);
            //return;
        }
        else
        {
            PastWeightErrorMessageTxt.gameObject.SetActive(false);
            if (string.IsNullOrEmpty(firstNameText.text) || string.IsNullOrEmpty(emailTextInput.text) || string.IsNullOrEmpty(heightText.text) || string.IsNullOrEmpty(weightText.text) || string.IsNullOrEmpty(userWeight6Month.text) || !emailValidation)
                return;

            PastWeightErrorMessageTxt.gameObject.SetActive(false);
            double weight6Month = double.Parse(userWeight6Month.text);
            double userWeight = double.Parse(weightText.text);
            double averageWeightLose = Math.Abs(((weight6Month - userWeight) / weight6Month) * 100);

            //Debug.Log("Weight Loss : "+" "+ weight6Month+" " + userWeight+"  " +averageWeightLose);

            if (userWeight < weight6Month && averageWeightLose > 5 && !YesToggleLoseWeight.isOn)
            {
                WeightConcernPopup.gameObject.SetActive(true);
                SaveData.Instance._PatientData.User_Weight_6_Month = userWeight6Month.text;
                return;
                //Debug.Log("It looks like you have lost a lot of weight recently.You should discuss this with your dietician or doctor");
            }
            SaveData.Instance._PatientData.User_Weight_6_Month = userWeight6Month.text;
        }

        //Debug.Log("userExists: " + userExists);
        //NextButton.GetComponent<Button>().interactable = true;
        if (string.IsNullOrEmpty(firstNameText.text) || string.IsNullOrEmpty(emailTextInput.text) || string.IsNullOrEmpty(heightText.text) || string.IsNullOrEmpty(weightText.text) || string.IsNullOrEmpty(userWeight6Month.text) || !emailValidation || userExists)
            return;

        SaveData.Instance._PatientData.User_Lose_Weight = (YesToggleLoseWeight.isOn ? "1" : "0");
        ScreenManager.Instance.Show("RegisterScreen_2");
    }


    public void OnHeightDropValueChanged(Dropdown dropdown)
    {
        //string choosedHeighttype = dropdown.options[dropdown.value].text;
        if (dropdown.value == 0)
        {
            heightText.placeholder.GetComponent<Text>().text = "*Height: eg 5.10";
            PlayerPrefs.SetInt("HeightDropDownValue", dropdown.value);
        }
        else if (dropdown.value == 1)
        {
            heightText.placeholder.GetComponent<Text>().text = "*Height: eg 157";
            PlayerPrefs.SetInt("HeightDropDownValue", dropdown.value);
        }
    }

    public void OnWeightDropValueChanged(Dropdown dropdown)
    {
        //string choosedWeighttype = dropdown.options[dropdown.value].text;
        if (dropdown.value == 0)
        {
            weightText.placeholder.GetComponent<Text>().text = "*Weight: eg 9.4";
            PastWeightDropDown.value = 0;
            PlayerPrefs.SetInt("WeightDropDownValue", dropdown.value);
            //userWeight6Month.placeholder.GetComponent<Text>().text = "Weight (Ex: 75 kgs)";
        }
        else if (dropdown.value == 1)
        {
            weightText.placeholder.GetComponent<Text>().text = "*Weight: eg 75";
            PastWeightDropDown.value = 1;
            PlayerPrefs.SetInt("WeightDropDownValue", dropdown.value);
            //userWeight6Month.placeholder.GetComponent<Text>().text = "Weight (Ex: 11.8 stone)";
        }
    }


    private bool ValidateEmail()
    {
        string email = emailTextInput.text;
        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,5})+)$");
        Match match = regex.Match(email);
        if (match.Success)
        {
            EmailErrorMessageTxt.gameObject.SetActive(false);
            return true;
        }
        else
        {
            EmailErrorMessageTxt.gameObject.SetActive(true);
            return false;
        }
    }

    public void OnWeightAlertClicked()
    {
        SaveData.Instance._PatientData.User_Lose_Weight = (YesToggleLoseWeight.isOn ? "1" : "0");
        ScreenManager.Instance.Show("RegisterScreen_2");
    }


    public void OnFirstNameEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnFirstNameEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }


    public void OnLastNameEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnLastNameEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }


    public void OnEmailEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnEmailEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }

    public void OnHeightEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnHeightEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }

    public void OnWeightEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnWeightEndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }

    public void OnWeight_Past_Edit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(true);
    }

    public void OnWeight_Past_EndEdit(GameObject editHighlightImage)
    {
        editHighlightImage.SetActive(false);
    }



    public void OnAlreadyRegistered()
    {
        ScreenManager.Instance.Show("LoginPage");
    }

}


    
 
