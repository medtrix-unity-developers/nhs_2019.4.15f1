﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatbotHomeScreen : MonoBehaviour
{
    [SerializeField] private ScreenController screencontroller;
    [SerializeField] private GameObject[] characters;
    [SerializeField] private Text welcomeMessage;
    [SerializeField] private RectTransform saraHolder;


    // Start is called before the first frame update
    void Awake()
    {
        if (PlayerPrefs.HasKey("DieticianPage"))
        {
            AppManager.Instance.CurrentCharPageID = PlayerPrefs.GetInt("DieticianPage");
        }
        else
        {
            AppManager.Instance.CurrentCharPageID = 1;
        }

        screencontroller.OnShowStarted += OnShowCustomDietician;
        //OnShowCustomDietician();


    }

    public void OnGetStartedClicked()
    {
        ScreenManager.Instance.Show("ChatbotScreen");
    }

    public void OMICSelected()
    {
        AppManager.Instance.currentChatType = ChatType.MIKE;
        ScreenManager.Instance.Show("ChatbotScreen");
    }
    public void OnKeyboardSelected()
    {
        AppManager.Instance.currentChatType = ChatType.KEYBOARD;
        ScreenManager.Instance.Show("ChatbotScreen");
    }



    private void OnShowCustomDietician()
    {
        string str1 = "NBT".ToUpper();

        if (PlayerPrefs.HasKey("DieticianPage"))
        {
            AppManager.Instance.CurrentCharPageID = PlayerPrefs.GetInt("DieticianPage");
        }
        else
        {
            AppManager.Instance.CurrentCharPageID = 1;
        }

        for (int i = 0; i < characters.Length; i++)
        {
            //characters[i].gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, saraHolder.rect.width);

            if (i == AppManager.Instance.CurrentCharPageID - 1)
            {
                characters[i].SetActive(true);
            }
            else
            {
                characters[i].SetActive(false);
            }
        }
        if (AppManager.Instance.DieticianName == "")
            AppManager.Instance.DieticianName = str1+" "+"Dietitian";

        welcomeMessage.text = "Hello! I’m " + "<color=#2E72C7><size=40><b>" + AppManager.Instance.DieticianName + "</b></size></color>"+"."+ " I’ll be your dietetic assistant.";
        //characterName.text = AppManager.Instance.DieticianName;
    }

    public void OnShowReminderScreen()
    {
        if (PlayerPrefs.HasKey("DieticianPage"))
        {
            AppManager.Instance.CurrentCharPageID = PlayerPrefs.GetInt("DieticianPage");
        }
        else
        {
            AppManager.Instance.CurrentCharPageID = 1;
        }

        ScreenManager.Instance.Show("ReminderScreen");
    }

    public void OnBackClicked()
    {
        ScreenManager.Instance.Show("MenuScreen");
    }


}
