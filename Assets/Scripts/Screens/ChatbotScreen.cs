﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatbotScreen : MonoBehaviour
{
    [SerializeField]
    private GameObject NarrationButton;

    [SerializeField]
    private GameObject STTObjects;
    [SerializeField]
    private GameObject TTSObjects;

    [SerializeField]
    private GameObject TopMicObject;
    [SerializeField]
    private GameObject TopKeyboard;

    [SerializeField]
    private GameObject dialogFlowObject;

    [SerializeField]
    private ScreenController _screenController;

    [SerializeField]
    private GameObject RecordingImage;

    [SerializeField]
    private Transform chatBubblesHolder;

    [SerializeField]
    private Sprite MicWhiteSprite;
    [SerializeField]
    private Sprite MicBlueSprite;

    [SerializeField]
    private InputField AddToFieldInput;
    private TouchScreenKeyboard keyboard  = null;
    private bool chatbotNarration;
 
    private void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;
        _screenController.OnHideStarted += HideStarted;
    
        AddToFieldInput.onEndEdit.AddListener(val =>
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || keyboard.status == TouchScreenKeyboard.Status.Done)
            {
                //Debug.Log("Coming here to enter the text.......");
                dialogFlowObject.GetComponent<DF2ClientAudioTester>().SendText();
            }

        });

    }

    public void OnKeyboardEnter()
    {
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
        keyboard.active = false;

    }


    public void OnRecordButtonClicked()
    {
        if(dialogFlowObject.GetComponent<DF2ClientAudioTester>().isRecording == false)
        {
            //STTObjects.gameObject.transform.GetChild(0).GetComponent<Image>().rectTransform.localScale = new Vector2(0.7f,0.7f);
            STTObjects.gameObject.transform.GetChild(1).GetComponent<Image>().sprite = MicBlueSprite;  // SpriteSheetManager.Instance.GetSpriteFromAtlas("mic_active@3x");
            RecordingImage.SetActive(true);
        }
        else
        {
            //STTObjects.gameObject.transform.GetChild(0).GetComponent<Image>().rectTransform.localScale = new Vector2(0.6f, 0.6f);
            STTObjects.gameObject.transform.GetChild(1).GetComponent<Image>().sprite = MicWhiteSprite;  // SpriteSheetManager.Instance.GetSpriteFromAtlas("mic@3x");
            RecordingImage.SetActive(false);
        }

        dialogFlowObject.GetComponent<DF2ClientAudioTester>().OnButtonRecord();
    }

    private void HideStarted()
    {
        
        dialogFlowObject.GetComponent<DF2ClientAudioTester>().botUI.allMessagesHeight = 30;
       
    }

    private void ShowStarted()
    {
        //if (chatBubblesHolder.childCount <= 0)

        //Debug.Log("Landed On Chat Screen");

        //if (AppManager.Instance.currentChatType == ChatType.MIKE)
        //{
        //    STTObjects.SetActive(true);
        //    TTSObjects.SetActive(false);
        //    TopKeyboard.SetActive(true);
        //    TopMicObject.SetActive(false);
        //}
        //else if (AppManager.Instance.currentChatType == ChatType.KEYBOARD)
        //{
        //    STTObjects.SetActive(false);
        //    TTSObjects.SetActive(true);
        //    TopKeyboard.SetActive(false);
        //    TopMicObject.SetActive(true);
        //    RecordingImage.SetActive(false);

        //    if (AppManager.Instance.DieticianName == "")
        //        AppManager.Instance.DieticianName = "NBT Dietician";
        //    TTSObjects.transform.GetChild(0).GetComponent<InputField>().placeholder.GetComponent<Text>().text = "Press here to talk with " + AppManager.Instance.DieticianName; 
        //}
        //dialogFlowObject.GetComponent<DF2ClientAudioTester>().SendWelcomeRequest();
        StartCoroutine(StartWelcomeRequest());
    }

    IEnumerator StartWelcomeRequest()
    {
        yield return new WaitForSeconds(0f);
        if (chatBubblesHolder.childCount <= 0)
        {
            dialogFlowObject.GetComponent<DF2ClientAudioTester>().botUI.allMessagesHeight = 30;
            dialogFlowObject.GetComponent<DF2ClientAudioTester>().SendWelcomeRequest();

        }
    }

    AudioSource[] m_MyAudioSource;
    public void Btn_ChatbotNarration()
    {
        m_MyAudioSource = GameObject.FindObjectsOfType<AudioSource>();     // GetComponent<AudioSource>();

        if (!chatbotNarration && NarrationButton.GetComponent<Button>().interactable)
        {
            NarrationButton.GetComponent<Button>().interactable = false;
            chatbotNarration = true;
            
            AppManager.Instance.currentChatType = ChatType.MIKE;
            //dialogFlowObject.GetComponent<AudioSource>().volume = 1;
            dialogFlowObject.GetComponent<DF2ClientAudioTester>().AudioVolumeUp(1);

            NarrationButton.transform.GetChild(0).gameObject.SetActive(false);
            NarrationButton.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = "Narration enabled!";
            NarrationButton.transform.GetChild(1).gameObject.SetActive(true);
            Invoke("DisableNarrationPopup", 2.0f);
        }else if (chatbotNarration && NarrationButton.GetComponent<Button>().interactable)
        {
            NarrationButton.GetComponent<Button>().interactable = false;
            chatbotNarration = false;
            AppManager.Instance.currentChatType = ChatType.KEYBOARD;
            //dialogFlowObject.GetComponent<AudioSource>().volume = 0;
            dialogFlowObject.GetComponent<DF2ClientAudioTester>().AudioVolumeDown(0);

            NarrationButton.transform.GetChild(0).gameObject.SetActive(true);
            NarrationButton.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = "Narration disabled!";
            NarrationButton.transform.GetChild(1).gameObject.SetActive(true);
            Invoke("DisableNarrationPopup", 2.0f);
        }
    }

    private void DisableNarrationPopup()
    {
        NarrationButton.GetComponent<Button>().interactable = true;
        NarrationButton.transform.GetChild(1).gameObject.SetActive(false);
    }

    public void OnKeyboardButtonClicked()
    {
        dialogFlowObject.GetComponent<DF2ClientAudioTester>().StopRecord();
        dialogFlowObject.GetComponent<DF2ClientAudioTester>().isRecording = false;
        STTObjects.gameObject.transform.GetChild(1).GetComponent<Image>().sprite = MicWhiteSprite;  // SpriteSheetManager.Instance.GetSpriteFromAtlas("mic@3x");
        STTObjects.SetActive(false);
        TTSObjects.SetActive(true);
        TopKeyboard.SetActive(false);
        TopMicObject.SetActive(true);
        RecordingImage.SetActive(false);
        AppManager.Instance.currentChatType = ChatType.KEYBOARD;
    }
    public void OnMicButtonClicked()
    {
        RecordingImage.SetActive(false);
        dialogFlowObject.GetComponent<DF2ClientAudioTester>().StopRecord();
        dialogFlowObject.GetComponent<DF2ClientAudioTester>().isRecording = false;
        STTObjects.gameObject.transform.GetChild(1).GetComponent<Image>().sprite = MicWhiteSprite;
        STTObjects.SetActive(true);
        TTSObjects.SetActive(false);
        TopKeyboard.SetActive(true);
        TopMicObject.SetActive(false);
        AppManager.Instance.currentChatType = ChatType.MIKE;
    }

    public void OnBackClicked()
    {
        /*string lastOpenedScreenID = ScreenManager.Instance.backStack[ScreenManager.Instance.backStack.Count - 1];
        if (lastOpenedScreenID == "MenuScreen")
        {
            ScreenManager.Instance.BackTo("MenuScreen");
        }
        else
        {
            ScreenManager.Instance.Show("ChatbotOptionsScreen");
        }*/

        string lastOpenedScreenID = ScreenManager.Instance.backStack[ScreenManager.Instance.backStack.Count - 1];
        if (lastOpenedScreenID == "MenuScreen")
        {
            ScreenManager.Instance.BackTo("MenuScreen");
        }
        else if (lastOpenedScreenID == "ChatbotHomeScreen")
        {
            ScreenManager.Instance.Show("ChatbotHomeScreen");
        }
        else
        {
            ScreenManager.Instance.Back();
        }

        dialogFlowObject.GetComponent<DF2ClientAudioTester>().StopAudioPlaying();
        foreach (RectTransform child in chatBubblesHolder)
        {
            Destroy(child.gameObject);
        }
    }


     
}
