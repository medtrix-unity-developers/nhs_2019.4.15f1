﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using PaperPlaneTools;
using System.Text.RegularExpressions;

public class Registration2 : MonoBehaviour
{
    [SerializeField] private ScreenController screencontroller;

    [SerializeField] private string userPotassiumInfo;
    [SerializeField] private string userPhosphateInfo;
    [SerializeField] private string userDiabetesInfo;
    [SerializeField] private string userSaltInfo;
    [SerializeField] private string userFluidInfo;
    [SerializeField] private string userCalorieInfo;
    [SerializeField] private Toggle[] page2Toggles;
    [SerializeField] public GameObject DietaryInfoPopup;
    [SerializeField] public GameObject PhosphateInfoPopup;

    [SerializeField] private Text PotassiumTitlePage2Txt;
    [SerializeField] private Text PhosphateTitlePage2Txt;
    [SerializeField] private Text HealthyTitleEatingPage2Txt;
    [SerializeField] private Text SaltTitlePage2Txt;
    [SerializeField] private Text FluidTitlePage2Txt;
    [SerializeField] private Text CalorieTitlePage2Txt;

    [SerializeField] private GameObject PotassiumInfoPage2Txt;
    [SerializeField] private GameObject PhosphateInfoPage2Txt;
    [SerializeField] private GameObject HealthyEatingInfoPage2Txt;
    [SerializeField] private GameObject SaltInfoPage2Txt;
    [SerializeField] private GameObject FluidInfoPage2Txt;
    [SerializeField] private GameObject CalorieInfoPage2Txt;

    [SerializeField] private Text Page2ErrorMessage;



    // Start is called before the first frame update
    void Start()
    {

    }
    // Start is called before the first frame update
    void Awake()
    {
        screencontroller.OnShowStarted += OnShowScreenStarted;
    }

    private void OnShowScreenStarted()
    {
        Page2ErrorMessage.gameObject.SetActive(false);
    }

    public void OnResetFields()
    {
        foreach (Toggle gameObj in page2Toggles)
        {
            gameObj.isOn = false;
        }
    }


    public void OnNextClicked()
    {
        int toggleCount = 0;

        Page2ErrorMessage.gameObject.SetActive(false);
        foreach (Toggle gameObj in page2Toggles)
        {
            if (gameObj.isOn)
            {
                toggleCount++;
            }
        }
        if (toggleCount < 1)
        {
            Page2ErrorMessage.gameObject.SetActive(true);
            return;
        }
        else
        {
            Page2ErrorMessage.gameObject.SetActive(false);
            ScreenManager.Instance.Show("RegisterScreen_3");
  
         }
    }

    public void OnInformationClicked()
    {
        //ToggleAnswerButton.Instance.clickedInformationName = "Answer_0"; //  OnAnswerButtonClicked("Answer_0");
        ScreenManager.Instance.Show("InformationsScreen");
    }


    public void OnPage_2_ToggleClicked(Toggle toggle)
    {
        string url = "";
        if (toggle.isOn == false)
        {
            switch (toggle.name)
            {
                case "Potassium":
                    SaveData.Instance._PatientData.User_Potassium_Info = "0";
                    PotassiumTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    PotassiumTitlePage2Txt.fontStyle = FontStyle.Normal;
                    PotassiumInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");
                    break;
                case "Phosphate":
                    SaveData.Instance._PatientData.User_Phosphate_Info = "0";
                    PhosphateTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    PhosphateTitlePage2Txt.fontStyle = FontStyle.Normal;
                    PhosphateInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");

                    break;
                case "HealthyEating":
                    SaveData.Instance._PatientData.User_Diabetes_Info = "0";
                    HealthyTitleEatingPage2Txt.color = new Color32(66, 85, 99, 255);
                    HealthyTitleEatingPage2Txt.fontStyle = FontStyle.Normal;
                    HealthyEatingInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");

                    break;
                case "Salt":
                    SaveData.Instance._PatientData.User_Salt_Info = "0";
                    SaltTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    SaltTitlePage2Txt.fontStyle = FontStyle.Normal;
                    SaltInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");

                    break;
                case "Fluid":
                    SaveData.Instance._PatientData.User_Fluid_Info = "0";
                    FluidTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    FluidTitlePage2Txt.fontStyle = FontStyle.Normal;
                    FluidInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");

                    break;
                case "Calorie":

                    SaveData.Instance._PatientData.User_Calorie_Info = "0";
                    CalorieTitlePage2Txt.color = new Color32(66, 85, 99, 255);
                    CalorieTitlePage2Txt.fontStyle = FontStyle.Normal;
                    CalorieInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-3@3x");
                    break;
            }
            return;
        }
        //toggle.transform.GetChild(2).GetComponent<Text>().color = new Color32(46, 114, 199, 255);
        //toggle.transform.GetChild(3).GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

        // Showing popup after clicked on checkbox button.
        switch (toggle.name)
        {
            case "Potassium":
                SaveData.Instance._PatientData.User_Potassium_Info = "1";
                PotassiumTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                PotassiumTitlePage2Txt.fontStyle = FontStyle.Bold;
                PotassiumInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Potassium><color=black><b><u>click here.</u></b></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower potassium diet. Only select this option if you have been advised to follow a low potassium diet by a healthcare professional."; //  For more information about Potassium " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);
                break;
            case "Phosphate":
                SaveData.Instance._PatientData.User_Phosphate_Info = "1";
                PhosphateTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                PhosphateTitlePage2Txt.fontStyle = FontStyle.Bold;
                PhosphateInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Phosphate><color=black><u>click here.</u></color></link>");
                PhosphateInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower phosphate diet. Only select this option if you have been advised to follow a low phosphate diet by a healthcare professional. Phosphate additives are found in many foods eg cola drinks, processed meat, potato products, bakery goods, dairy foods. Check food labels for anything with ‘phos’ in the name. Avoiding these reduces the amount of phosphate you are eating."; //  For more information about Phosphate " + url
                PhosphateInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                PhosphateInfoPopup.SetActive(true);
                break;

            case "HealthyEating":
                SaveData.Instance._PatientData.User_Diabetes_Info = "1";
                HealthyTitleEatingPage2Txt.color = new Color32(46, 114, 199, 255);
                HealthyTitleEatingPage2Txt.fontStyle = FontStyle.Bold;
                HealthyEatingInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=HealthyEating><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a Healthy Eating diet, which is also suitable for people with diabetes."; //  For more information about Healthy Eating " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);

                break;
            case "Salt":
                SaveData.Instance._PatientData.User_Salt_Info = "1";
                SaltTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                SaltTitlePage2Txt.fontStyle = FontStyle.Bold;
                SaltInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Salt><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower salt diet. Try not to add salt during or after cooking.";   //  For more information about Salt " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);

                break;
            case "Fluid":
                SaveData.Instance._PatientData.User_Fluid_Info = "1";
                FluidTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                FluidTitlePage2Txt.fontStyle = FontStyle.Bold;
                FluidInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Fluid><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods and drinks for a lower fluid diet. Only select this option if you have been advised to follow a low fluid diet by a healthcare professional.";  //  For more information about Fluid " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);
                break;
            case "Calorie":
                SaveData.Instance._PatientData.User_Calorie_Info = "1";
                CalorieTitlePage2Txt.color = new Color32(46, 114, 199, 255);
                CalorieTitlePage2Txt.fontStyle = FontStyle.Bold;
                CalorieInfoPage2Txt.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas("Info-circle-5@3x");

                url = string.Format("<link=Calorie><color=black><u>click here.</u></color></link>");
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.text = "For help choosing foods depending on whether they are high, medium or low in calories. To gain weight choose more high or medium options. To lose weight choose more low calorie options. Adding oil or spread during or after cooking will increase calories.";  //  For more information about Calorie " + url
                DietaryInfoPopup.GetComponent<SingleButtonPopup>().BodyText.fontSize = 25;
                DietaryInfoPopup.SetActive(true);
                break;
        }
    }
    public void OnCancelClicked()
    {
        DietaryInfoPopup.SetActive(false);
        PhosphateInfoPopup.SetActive(false);

    }

    public void OnPage2BackClicked()
    {
        ScreenManager.Instance.Show("RegisterScreen_1");
    }
}
