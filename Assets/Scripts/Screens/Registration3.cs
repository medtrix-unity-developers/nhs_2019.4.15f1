﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using PaperPlaneTools;
using System.Text.RegularExpressions;

public class Registration3 : MonoBehaviour
{
    [SerializeField] private Toggle YesToggleKidneyTrans;
    [SerializeField] private Toggle NoToggleKidneyTrans;
    [SerializeField] private Toggle page3AgreeToggle;

    [SerializeField] private Text AgreeErrorMessageTxt;
    [SerializeField] private Text UserExistsErrorMessageTxt;

    [SerializeField] public GameObject KidneyPopup;
    [SerializeField] public GameObject RegistrationConfirmPopup;


    [SerializeField] private ScreenController screencontroller;

    public GameObject NextButton;

    void Awake()
    {
        screencontroller.OnShowStarted += OnShowScreenStarted;
        SaveData.Instance.userAlreadyExistsCallback += OnUserAlreadyExists;
        SaveData.Instance.userRegistrationConfirm += OnUserConfirmPopup;
    }

    private void OnShowScreenStarted()
    {
        AgreeErrorMessageTxt.gameObject.SetActive(false);
        UserExistsErrorMessageTxt.gameObject.SetActive(false);
    }

    public void OnResetFields()
    {
        page3AgreeToggle.isOn = false;
    }

    public void OnUserAlreadyExists()
    {
        UserExistsErrorMessageTxt.gameObject.SetActive(true);
    }

    public void OnRegisterClicked()
    {
        if (page3AgreeToggle.isOn == false)
        {
            AgreeErrorMessageTxt.gameObject.SetActive(true);
            return;
        }
        SaveData.Instance._PatientData.User_Kidney_Info = (YesToggleKidneyTrans.isOn ? "1" : "0");
        AgreeErrorMessageTxt.gameObject.SetActive(false);
        //saveData.SavePatientDataToLocal();
        SaveData.Instance.SendJSONDataToServer("");
    }

    public void OnPage_3_ToggleClicked(Toggle toggle)
    {
        switch (toggle.name)
        {
            case "YesToggle":
                if (toggle.isOn == false) return;
                toggle.isOn = true;
                toggle.interactable = false;
                KidneyPopup.SetActive(true);
                break;

            case "NoToggle":
                YesToggleKidneyTrans.interactable = true;
                break;
        }
    }

    public void OnCallClicked()
    {
        YesToggleKidneyTrans.isOn = true;
        KidneyPopup.SetActive(false);
        //Debug.Log("On Call Clicked...");
        Application.OpenURL("tel://01174145428");   // telephone number = 0117 4145428/9
    }

    public void OnCancelClicked()
    {
        YesToggleKidneyTrans.isOn = true;
        KidneyPopup.SetActive(false);
    }

    public void OnEmailClicked()
    {
        YesToggleKidneyTrans.isOn = true;
        KidneyPopup.SetActive(false);
        //Debug.Log("On Email  Clicked...");
        string email = "renaldietitians@nbt.nhs.uk";
        //string subject = MyEscapeURL("Test Subject");
        //string body = MyEscapeURL("My Body\r\nFull of non-escaped chars");
        string subject = MyEscapeURL("");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void OnPage3BackClicked()
    {
        UserExistsErrorMessageTxt.gameObject.SetActive(false);
        ScreenManager.Instance.Show("RegisterScreen_2");
    }

    public void OnUserConfirmPopup()
    {
        RegistrationConfirmPopup.SetActive(true);
    }

}
