﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PaperPlaneTools;

public class MenuScreen : MonoBehaviour
{
    [SerializeField] private Text[] Textfields;
    [SerializeField] private GameObject[] SideBorders;

    [SerializeField] private Text Home;
    [SerializeField] private Text Chat;
    [SerializeField] private Text Reminder;
    [SerializeField] private Text Profile;
    [SerializeField] private Text Information;
    [SerializeField] private Text Terms;
    [SerializeField] private Text Privacy;
    [SerializeField] private Text Dietician;
    [SerializeField] private Text Logout;
    [SerializeField] private ScreenController screenController;

    [SerializeField] private Font Fruitger_Bold;
    [SerializeField] private Font Fruitger_Light;
     

    private void Awake()
    {
        screenController.OnShowStarted += OnMenuScreenShow;
    }

    private void OnMenuScreenShow()
    {
        if (ScreenManager.Instance.backStack.Count <= 0) return;

        string lastOpenedScreenID = ScreenManager.Instance.backStack[ScreenManager.Instance.backStack.Count - 1];
        if (lastOpenedScreenID == "ChatbotHomeScreen")
        {
            foreach (Text text in Textfields)
            {
                if (text.name == "Home")
                {
                    text.font = Fruitger_Bold;
                    text.fontStyle = FontStyle.Bold;
                }
                else
                {
                    text.font = Fruitger_Light;
                    text.fontStyle = FontStyle.Normal;
                }
            }
            OnMenuOptionsClicked(SideBorders[0].transform.parent.GetComponent<Button>());


        }
            

     } 

    public void OnHomeButton()
    {
        //AppManager.Instance.currentChatType = ChatType.KEYBOARD;
        //ScreenManager.Instance.Show("ChatbotOptionsScreen");
          
    }

    public void OnMenuOptionsClicked(Button button)
    {
        foreach(Text text in Textfields)
        {
            if(text.name == button.name)
            {
                text.font = Fruitger_Bold;
                text.fontStyle = FontStyle.Bold;
            }
            else
            {
                text.font = Fruitger_Light;
                text.fontStyle = FontStyle.Normal;

            }
         }

        foreach(GameObject sliderObj in SideBorders)
        {
            sliderObj.GetComponent<Image>().enabled = false;
        }
        button.transform.GetChild(0).gameObject.GetComponent<Image>().enabled = true;

        switch (button.name)
        {

            case "Home":
                ScreenManager.Instance.Show("ChatbotHomeScreen");
                break;
            case "Chat":
                //OnHomeButton();
                ScreenManager.Instance.Show("ChatbotScreen");
                break;
            case "Reminder":
                ScreenManager.Instance.Show("ReminderScreen");
                break;
            case "Profile":
                ScreenManager.Instance.Show("ProfileScreen");
                break;
            case "Information":
                ScreenManager.Instance.Show("InformationsScreen");
                break;
            case "Terms":
                ScreenManager.Instance.Show("TermsConditionsScreen");
                break;
            case "PrivacyPolicy":
                ScreenManager.Instance.Show("PrivacyPolicy");
                break;
            case "Dietician":
                //Debug.Log("COUNT");
                ScreenManager.Instance.Show("DieticianCustomScreen");
                GameObject.FindObjectOfType<DieticianCustomScreen>().btn_Back.SetActive(true);
                break;
            case "LogOut":
                new Alert("", "Do you want to logout ?")
                .SetPositiveButton("Yes", () => {
                    Debug.Log("Ok handler");
                    PlayerPrefs.SetInt("UserLoggedStatus", 0);
                    ScreenManager.Instance.Show("LoginPage");
                })
                .SetNegativeButton("No", () => {
                    //Debug.Log("No handler");
                })
                .Show();

                break;
        } 
    }

}
