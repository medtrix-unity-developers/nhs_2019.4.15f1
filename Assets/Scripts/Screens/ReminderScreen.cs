﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReminderScreen : MonoBehaviour
{
    [SerializeField] private ScreenController screencontroller;
    [SerializeField] private Sprite[] botSprites;
    [SerializeField] private Image avatarImage;
    [SerializeField] private CalendarInit calendarInit;

    // Start is called before the first frame update
    void Awake()
    {
        screencontroller.OnShowStarted += OnShowReminderScreen;
        screencontroller.OnHideStarted += OnHiderReminderScreen;
     }

    public void OnShowReminderScreen()
    {
        avatarImage.sprite = botSprites[AppManager.Instance.CurrentCharPageID - 1];
        calendarInit.OnCalendarInitEnabled();
    }

    public void OnHiderReminderScreen()
    {
        calendarInit.OnCalendarInitDisabled();
    }

    public void OnBackClicked()
    {
        string lastOpenedScreenID = ScreenManager.Instance.backStack[ScreenManager.Instance.backStack.Count - 1];
        if (lastOpenedScreenID == "MenuScreen")
        {
            ScreenManager.Instance.BackTo("MenuScreen");
        }
        else
        {
            //ScreenManager.Instance.Back();
            ScreenManager.Instance.Show("ChatbotHomeScreen");
        }
    }


}
