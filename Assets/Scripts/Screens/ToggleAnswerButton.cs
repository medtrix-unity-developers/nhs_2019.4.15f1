﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ToggleAnswerButton : MonoSingleton<ToggleAnswerButton>
{
   
    [SerializeField] private Toggle[] toggles;
    [SerializeField] private GameObject[] answerBoxes;
    [SerializeField] private Text[] questions; 

    [SerializeField] private ScreenController screencontroller;
    public string clickedInformationName;
  
    
    private void Start()
    {
       // screencontroller.OnShowStarted += OnStartAnswer;
    }

    /*public void OnStartAnswer()
    {
        // clickedInformationName = _clickedInformationName;
        OnAnswerButtonClicked(clickedInformationName);
    }*/

    public void OnAnswerButtonClicked(string clickedAnswerName)
    {
        //string lastOpenedScreenID = ScreenManager.Instance.backStack[ScreenManager.Instance.backStack.Count - 1];
        //Debug.Log("CURRENT SCREEN on TOGGLE ANSWER :"+ScreenManager.Instance.CurrentScreenId);
        int clickedAnswerID = Convert.ToInt32(clickedAnswerName.Split('_')[1]);
        string currentScreenID = ScreenManager.Instance.CurrentScreenId;
 
        if (answerBoxes[clickedAnswerID] && answerBoxes[clickedAnswerID].activeSelf && currentScreenID == "InformationsScreen")
        {
            answerBoxes[clickedAnswerID].SetActive(false);
            toggles[clickedAnswerID].targetGraphic.gameObject.SetActive(true);
            toggles[clickedAnswerID].graphic.gameObject.SetActive(false);
            questions[clickedAnswerID].color = new Color32(66, 85, 99, 255);
            questions[clickedAnswerID].fontStyle = FontStyle.Normal;

            //topPaddings[clickedAnswerID].SetActive(false);
            return;
        }

        //Debug.Log("Clicked Button Name :"+ clickedAnswerName);
        for(int i =0; i < answerBoxes.Length; i++)
        {
            if( i == clickedAnswerID)
            {
                toggles[i].targetGraphic.gameObject.SetActive(false);
                toggles[i].graphic.gameObject.SetActive(true);
                toggles[i].isOn = true;
                toggles[i].graphic.enabled = true;
                answerBoxes[i].SetActive(true);
                questions[i].color = new Color32(46, 114, 199, 255);
                questions[i].fontStyle = FontStyle.Bold;
                //topPaddings[i].SetActive(true);
            }
            else
            {
                toggles[i].targetGraphic.gameObject.SetActive(true);
                toggles[i].graphic.gameObject.SetActive(false);
                answerBoxes[i].SetActive(false);
                questions[i].color = new Color32(66, 85, 99, 255);
                questions[i].fontStyle = FontStyle.Normal;

                //topPaddings[i].SetActive(false);
            }
        }
    }

    public void FromMenuClicked(string clickedAnswerName)
    {
        int clickedAnswerID = Convert.ToInt32(clickedAnswerName.Split('_')[1]);

        for (int i = 0; i < answerBoxes.Length; i++)
        {
            if (i == clickedAnswerID)
            {
                toggles[i].targetGraphic.gameObject.SetActive(false);
                toggles[i].graphic.gameObject.SetActive(true);
                toggles[i].isOn = true;
                toggles[i].graphic.enabled = true;
                answerBoxes[i].SetActive(true);
                questions[i].color = new Color32(46, 114, 199, 255);
                questions[i].fontStyle = FontStyle.Bold;
                //topPaddings[i].SetActive(true);
            }
            else
            {
                toggles[i].targetGraphic.gameObject.SetActive(true);
                toggles[i].graphic.gameObject.SetActive(false);
                answerBoxes[i].SetActive(false);
                questions[i].color = new Color32(66, 85, 99, 255);
                questions[i].fontStyle = FontStyle.Normal;

                //topPaddings[i].SetActive(false);
            }
        }
    }


    public void OnBackClicked()
    { 
        ScreenManager.Instance.Back();
    }

}
