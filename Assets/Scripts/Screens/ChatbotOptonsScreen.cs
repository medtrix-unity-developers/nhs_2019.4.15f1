﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatbotOptonsScreen : MonoBehaviour
{
    [SerializeField]
    //private Transform chatBubblesHolder;
    // Start is called before the first frame update
    void Awake()
    {
        if (PlayerPrefs.HasKey("DieticianPage"))
        {
            AppManager.Instance.CurrentCharPageID = PlayerPrefs.GetInt("DieticianPage");
        }
        else
        {
            AppManager.Instance.CurrentCharPageID = 1;
        }
    }
    public void OnKeyboardOptionsChoosed()
    {
        AppManager.Instance.currentChatType = ChatType.KEYBOARD;
        ScreenManager.Instance.Show("ChatbotScreen");
    }

    public void OnMicOptionsChoosed()
    {
        AppManager.Instance.currentChatType = ChatType.MIKE;
        ScreenManager.Instance.Show("ChatbotScreen");
    }


    public void OnBackClicked()
    {
        //ScreenManager.Instance.Show("ChatbotHomeScreen");

        string lastOpenedScreenID = ScreenManager.Instance.backStack[ScreenManager.Instance.backStack.Count - 1];
        if (lastOpenedScreenID == "MenuScreen")
        {
            ScreenManager.Instance.Show("MenuScreen");
        }
        else
        {
            ScreenManager.Instance.Show("ChatbotHomeScreen");
        }

        /*foreach (RectTransform child in chatBubblesHolder)
        {
            Destroy(child.gameObject);
        }*/
    }
}
