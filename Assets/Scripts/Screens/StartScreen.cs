﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using PaperPlaneTools;
using System.Text.RegularExpressions;

public class StartScreen : MonoBehaviour
{
    private void Awake()
    {
        string FilePath = Path.Combine(Application.persistentDataPath, "PatientData.json");
        if (File.Exists(FilePath))
        {
            ScreenManager.Instance.Show("ChatbotHomeScreen");
        }
    }

     
}
