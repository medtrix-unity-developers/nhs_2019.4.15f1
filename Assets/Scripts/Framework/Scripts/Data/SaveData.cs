﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Text;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;

//TODO Login and registration from stalin.
public class SaveData : MonoSingleton<SaveData>
{
    private SendReminderData _sendReminderData;
    [SerializeField] public PatientData _PatientData = new PatientData();
    [SerializeField] public PatientEmailCheck _PatientEmailCheck = new PatientEmailCheck();


    public Action userAlreadyExistsCallback;
    public Action userRegistrationConfirm;

    private string FilePath;
    //string registrationURL = "https://www.medtrixhealthcare.com/NHS_ChatBot/NHSChatbot/Registration";

    void Start()
    {
        LoadPatientData();
    }

    public void LoadPatientData()
    {
        FilePath = Path.Combine(Application.persistentDataPath, "PatientData.json");
        _sendReminderData = GetComponent<SendReminderData>();

        byte[] bytes = File.ReadAllBytes(FilePath);
        string someString = Encoding.ASCII.GetString(bytes);
 
        if (someString != null)
        {
            string json = File.ReadAllText(FilePath);
            try
            {
                _PatientData = JsonConvert.DeserializeObject<PatientData>(json);
                AppManager.Instance.PatientUserID = _PatientData.User_Id;
            }
            catch (Exception)
            {
                Debug.Log("File invalid format");
                return;
            }
        }
        else
        {
            Debug.Log("File Not found or invalid path");
        }
    }

     

    public void SavePatientDataToLocal(PatientData _patientData_45)
    {
        //Debug.LogError("44444444:"+FilePath);
        FilePath = Path.Combine(Application.persistentDataPath, "PatientData.json");

        var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(_patientData_45);
        //string json = JsonConvert.SerializeObject(_PatientData);
        byte[] bytes = Encoding.ASCII.GetBytes(jsonString);
        try
        {
            File.WriteAllBytes(FilePath, bytes);
            Debug.Log("File Saved  HERE >>>>>"+ FilePath);
        }
        catch (Exception)
        {
            Debug.Log("File Couldn't save.");
        }

        LoadPatientData();
    }
 
    public void SendJSONDataToServer(string _setURL)
    {
        if(_setURL == "")
        {
            _setURL = "https://www.medtrixhealthcare.com/NHS_ChatBot/NHSChatbot/Registration"; ;
        }

        StartCoroutine(PostMethod(_PatientData, _setURL));
    }

    IEnumerator PostMethod(PatientData patientData, string _setURL)
    {
        GameObject.FindObjectOfType<Registration3>().NextButton.GetComponent<Button>().interactable = false;
        string patientJSONData = JsonUtility.ToJson(_PatientData);
        Debug.Log("patientJSONData : "+ patientJSONData);
        var request = new UnityWebRequest(_setURL, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(patientJSONData);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();
        if (!request.isNetworkError)
        {
            //Debug.Log("Data successfully sent to server." + request.downloadHandler.text);
            string response = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
            PatientResponseData patientUserResponse = JsonConvert.DeserializeObject<PatientResponseData>(response);

            if (patientUserResponse.userCode != "0")
            {
                _PatientData.User_Id = int.Parse(patientUserResponse.userCode);
                _sendReminderData.storedUserId = _PatientData.User_Id;
                //SavePatientDataToLocal();
                PlayerPrefs.SetString("PatientUserID", patientUserResponse.userCode);
                AppManager.Instance.PatientUserID = _PatientData.User_Id;
                //ScreenManager.Instance.Show("DieticianCustomScreen");

                userRegistrationConfirm?.Invoke();
            }
            else
            {
                userAlreadyExistsCallback?.Invoke();
            }
        }
        else
        {
            Debug.Log("Error sending data to server.");

        }
        GameObject.FindObjectOfType<Registration3>().NextButton.GetComponent<Button>().interactable = true;
    }

    public void RB_SendJSONDataToServer_UserEmail(string _setURL, string emailText)
    {
        _PatientEmailCheck.User_EmailId = emailText;

        if (_setURL == "")
        {
            _setURL = "https://www.medtrixhealthcare.com/NHS_ChatBot/NHSChatbot/CheckUserIDExits";
        }

        StartCoroutine(RB_CheckUserEmailExistance(_PatientEmailCheck, _setURL));
    }

    IEnumerator RB_CheckUserEmailExistance(PatientEmailCheck patientEmailCheck, string _setURL)
    {
        bool userExists = false;
        string patientJSONData = JsonUtility.ToJson(patientEmailCheck);
        //Debug.Log("patientJSONData : " + patientJSONData);
        var request = new UnityWebRequest(_setURL, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(patientJSONData);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();
        if (!request.isNetworkError)
        {
            //Debug.Log("Data successfully sent to server." + request.downloadHandler.text);
            string response = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
            PatientResponseData patientUserResponse = JsonConvert.DeserializeObject<PatientResponseData>(response);

            //Debug.Log("Response: " + patientUserResponse.ToString());
            if (patientUserResponse.status != "User Logged In Successfully")
            {
                //User Doesn't Exist
            }
            else
            {
                //User Exists
                userExists = true;
                userAlreadyExistsCallback?.Invoke();
            }
        }
        else
        {
            Debug.Log("Error sending data to server.");

        }
        GameObject.FindObjectOfType<Registration1>().OnceEmailExistanceVerified(userExists);
    }


    public void UpdateJSONDataToServer(string _setURL)
    {
        if (_setURL == "")
        {
            _setURL = "https://www.medtrixhealthcare.com/NHS_ChatBot/NHSChatbot/UpdateUserData";
        }

        StartCoroutine(UpdatePostMethod(_PatientData, _setURL));
    }

    IEnumerator UpdatePostMethod(PatientData patientData, string _setURL)
    {
        string patientJSONData = JsonUtility.ToJson(_PatientData);
        //Debug.Log("patientJSONData : " + patientJSONData);

        var request = new UnityWebRequest(_setURL, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(patientJSONData);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();
        if (!request.isNetworkError)
        {
             //Debug.Log("Data successfully sent to server." + request.downloadHandler.text);
             string response = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
             SavePatientDataToLocal(patientData);
        }
        else
        {
            Debug.Log("Error sending data to server.");
        }
    }



    [System.Serializable]
    public class PatientData
    {
        public string User_Name = "";
        public string User_FirstName = "";
        public string User_LastName = "";

        public string User_EmailId = ""; 
        public string User_Height = "";  
        public string User_Weight = ""; 
        public string User_Lose_Weight = ""; 
        public string User_Weight_6_Month = "";  

        public string User_Potassium_Info = "0"; 
        public string User_Phosphate_Info = "0";  
        public string User_Diabetes_Info = "0";  
        public string User_Salt_Info = "0";  
        public string User_Fluid_Info = "0";  
        public string User_Calorie_Info = "0";  
        public string User_NHS_Trust = "0"; 
        public string User_Kidney_Info = "0";
        public int User_Id;
    }

    [System.Serializable]
    public class PatientResponseData
    {
        public string userCode;
        public string status;
    
    }

    [System.Serializable]
    public class PatientEmailCheck
    {
        public string User_EmailId;
    }
}
