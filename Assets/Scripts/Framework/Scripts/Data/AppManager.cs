﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
  

[Serializable]
public enum ChatType
{
    MIKE = 1,
    KEYBOARD = 2
}

public class AppManager : MonoSingleton<AppManager>
{
    //public AppMode currentAppMode;
    public ChatType currentChatType;
    public GameObject BottomPanel;
    public GameObject Backbutton;
    public int CurrentCharPageID = 1;
    public string DieticianName = "";

    public int PatientUserID;
  
    //public const string CUSTOM_CHARACTER_IDENTIFIER = "CustomCharacterData.dat";
    internal static readonly ChatType chattype;
    public Texture2D UserProfilePhoto;
      

    private void Awake()
    {
        PatientUserID = PlayerPrefs.GetInt("UserUniqueID");

        if (PlayerPrefs.HasKey("DieticianName"))
        {
            DieticianName = PlayerPrefs.GetString("DieticianName");
        }
        if (PlayerPrefs.HasKey("DieticianPage"))
        {
            CurrentCharPageID = PlayerPrefs.GetInt("DieticianPage");
        }
        else
        {
            CurrentCharPageID = 1;
        }
         
    }


}
