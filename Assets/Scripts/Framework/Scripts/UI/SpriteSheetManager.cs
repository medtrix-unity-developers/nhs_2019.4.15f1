﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
 
 
public class SpriteSheetManager : MonoSingleton<SpriteSheetManager>
{
	public Dictionary<string, Sprite> _Spritesheet_2;
	public Dictionary<string, Sprite> _Spritesheet_1;
	//public Dictionary<string, Sprite> _commonSprites;

	void Awake()
    {
        LoadSpritesheet1();
		LoadSpritesheet2();

	}

	private void LoadSpritesheet1()
    {
		Sprite[] SpritesData = Resources.LoadAll<Sprite>("Spritesheets/Spritesheet_1");
		_Spritesheet_1 = new Dictionary<string, Sprite>();

		for (int i = 0; i < SpritesData.Length; i++)
		{
			_Spritesheet_1.Add(SpritesData[i].name, SpritesData[i]);
		}

	}

	private void LoadSpritesheet2()
	{
		Sprite[] SpritesData = Resources.LoadAll<Sprite>("Spritesheets/Spritesheets_2");
		_Spritesheet_2 = new Dictionary<string, Sprite>();

		for (int i = 0; i < SpritesData.Length; i++)
		{
			_Spritesheet_2.Add(SpritesData[i].name, SpritesData[i]);
		}
        //Debug.Log("Asthma sprites count :>>>>"+ _asthmaSprites.Count);
	}
	
	/*private void LoadCOPDDictionary()
	{
		Sprite[] SpritesData = Resources.LoadAll<Sprite>("Spritesheets/COPDSpritesheet_1");
		_copdSprites = new Dictionary<string, Sprite>();

		for (int i = 0; i < SpritesData.Length; i++)
		{
			_copdSprites.Add(SpritesData[i].name, SpritesData[i]);
		}
        //Debug.Log("LoadCOPDDictionary sprites count :>>>>" + _copdSprites.Count);
    }

	private void LoadCommonDictionary()
	{
		Sprite[] SpritesData = Resources.LoadAll<Sprite>("Spritesheets/CommonSpritesheet_1");
		_commonSprites = new Dictionary<string, Sprite>();

		for (int i = 0; i < SpritesData.Length; i++)
		{
			_commonSprites.Add(SpritesData[i].name, SpritesData[i]);
		}
        //Debug.Log("LoadCommonDictionary sprites count :>>>>" + _commonSprites.Count);

    } */

    /*public Sprite GetAsthmaSpriteByName(string name)
	{
		
	}


	public Sprite GetCOPDSpriteByName(string name)
	{
		
	}*/

    public Sprite GetSpriteFromAtlas(string name)
    {
        if (_Spritesheet_2.ContainsKey(name))
            return _Spritesheet_2[name];
		else if(_Spritesheet_1.ContainsKey(name))
			return _Spritesheet_1[name];
		else
            return null;
	}

	/*public Sprite GetCommonSpriteByName(string name)
	{
		if (_commonSprites.ContainsKey(name))
			return _commonSprites[name];
		else
			return null;
	}*/

}
