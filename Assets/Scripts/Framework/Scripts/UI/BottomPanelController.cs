﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomPanelController : MonoBehaviour
{
    public Button HomeButton;
    public Button InhalerButton;
    public Button SwitchButton;
    public Button TrackerButton;
    public Button CalendarButton;

    [SerializeField] private GameObject selectedMark;
    [SerializeField] private GameObject[] characterObjectIcons;
    public int clickedID;


    // Start is called before the first frame update
    void Start()
    {
        //AppManager.OnModeChange += OnSwitchMode;
        SetButtonSprites();
    }

    public void OnBottomMenuClicked(string clickedTypeName)
    {
        string charIconIdname = clickedTypeName.Split('_')[1].ToString();
        clickedID = int.Parse(charIconIdname);
        //Debug.Log("clickedTypeName on Custom CharBottom Panel..  ::...." + clickedTypeName);

         
     

        switch (clickedID)
        {
            case 0:
                selectedMark.transform.SetParent(characterObjectIcons[0].transform, false);
 
                break;

            case 1:
                selectedMark.transform.SetParent(characterObjectIcons[1].transform, false);
 
                break;

            case 2:
                selectedMark.transform.SetParent(characterObjectIcons[2].transform, false);
            
                break;

            case 3:
                selectedMark.transform.SetParent(characterObjectIcons[3].transform, false);
        
                break;
        }

        //}
    }


    

    public void OnSwitchMode()
    {
        //Debug.Log("OnSwithch Mode in BottomPanelController is "+ appMode);
        /*if (AppManager.Instance.currentAppMode == AppMode.SAA)
        {
            AppManager.Instance.currentAppMode = AppMode.COPD;
            AppModeTxt.text = "COPD";

        }
        else if (AppManager.Instance.currentAppMode == AppMode.COPD)
        {
            AppManager.Instance.currentAppMode = AppMode.SAA;
            AppModeTxt.text = "ASTHMA";

        } */
        //AppManager.Instance.OnSwitchMode();
        SetButtonSprites();
    }


    public void SetButtonSprites()
    {
        /*HomeButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppManager.Instance.currentAppMode, "home_active@3x");
        //HomeButton.GetComponent<RectTransform>().sizeDelta = new Vector2(59f, 57f);

        InhalerButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppManager.Instance.currentAppMode, "inhaler_active@3x");
        //InhalerButton.GetComponent<RectTransform>().sizeDelta = new Vector2(49f, 60f);

        SwitchButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppManager.Instance.currentAppMode, "switch_active@3x");
        //SwitchButton.GetComponent<RectTransform>().sizeDelta = new Vector2(120f, 120f);

        TrackerButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppManager.Instance.currentAppMode, "tracker_active@3x");
        //TrackerButton.GetComponent<RectTransform>().sizeDelta = new Vector2(53f, 60f);
 
        CalendarButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppMode.SAA, "sidemenu_calendar_active@3x");
        //CalendarButton.GetComponent<RectTransform>().sizeDelta = new Vector2(46f, 52f);

        if (AppManager.Instance.currentAppMode == AppMode.SAA)
        {
            AppModeTxt.text = "ASTHMA";
            CalendarButton.GetComponent<Image>().color = new Color32(8,149,161,255);
        }
        else if (AppManager.Instance.currentAppMode == AppMode.COPD)
        {
            AppModeTxt.text = "COPD";
            CalendarButton.GetComponent<Image>().color = new Color32(0,80,85,255);

        } */
    }

}
