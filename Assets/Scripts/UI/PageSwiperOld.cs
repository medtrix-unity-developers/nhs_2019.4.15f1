﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PageSwiperOld : MonoBehaviour, IDragHandler, IEndDragHandler
{
    private Vector3 panelLocation;
    public float percentThreshold = 0.2f;
    public float easing = 0.5f;
    public int totalPages = 4;
    private int currentPage = 1;
    [SerializeField] private ScreenController _screenController;
    [SerializeField] private GameObject[] pageMarkers;

    [SerializeField] private GameObject[] avatarImages;
    [SerializeField] private RectTransform saraHolder;



    // Start is called before the first frame update
    void Start()
    {
       // panelLocation = transform.position;

        for(int i =0; i < avatarImages.Length; i++)
        {
            avatarImages[i].gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, saraHolder.rect.width);
            avatarImages[i].transform.position = new Vector3();
        }
        
    }

    public void OnDrag(PointerEventData data)
    {
        //float difference = data.pressPosition.x - data.position.x;
        //transform.position = panelLocation - new Vector3(difference, 0, 0);
    }
    public void OnEndDrag(PointerEventData data)
    {
        float percentage = (data.pressPosition.x - data.position.x) / Screen.width;
        if (Mathf.Abs(percentage) >= percentThreshold)
        {
            Vector3 newLocation = panelLocation;
            if (percentage > 0 && currentPage < totalPages)
            {
                currentPage++;
                newLocation += new Vector3(-Screen.width, 0, 0);
            }
            else if (percentage < 0 && currentPage > 1)
            {
                currentPage--;
                newLocation += new Vector3(Screen.width, 0, 0);
            }
            StartCoroutine(SmoothMove(transform.position, newLocation, easing));
            panelLocation = newLocation;
        }
        else
        {
            StartCoroutine(SmoothMove(transform.position, panelLocation, easing));
        }
    }
    IEnumerator SmoothMove(Vector3 startpos, Vector3 endpos, float seconds)
    {
        float t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startpos, endpos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    private void ChangeMarkerPoint()
    {
        for(int i=0; i< pageMarkers.Length;i++)
        {
            if(i == currentPage - 1)
            {
                pageMarkers[i].transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                pageMarkers[i].transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }

}
