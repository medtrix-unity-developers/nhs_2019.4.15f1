﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ClickablePrivacyText : MonoBehaviour, IPointerClickHandler
{
    //[SerializeField] private GameObject privacyPopup;
    //[SerializeField] private GameObject termsPopup;


    public void OnPointerClick(PointerEventData eventData)
    {
        //Debug.Log("CLICKED ClickableTermsText: ");

        var text = GetComponent<TextMeshProUGUI>();
        if(eventData.button == PointerEventData.InputButton.Left)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, Input.mousePosition, null);
            if(linkIndex > -1)
            {
                var linkInfo = text.textInfo.linkInfo[linkIndex];
                var linkId = linkInfo.GetLinkID();
                //var itemData = FindObjectOfType<ItemDataController>().Get(linkId);

                if (linkInfo.GetLinkID() == "PrivacyGoogle")
                {
                    //Debug.Log("CLICKED on ClickableTerms  TermsEmailID : " + linkInfo.GetLinkID());
                    Application.OpenURL("https://policies.google.com/privacy");
                }
                if (linkInfo.GetLinkID() == "PrivacyUnity")
                {
                    //Debug.Log("CLICKED on ClickableTerms  TermsEmailID : " + linkInfo.GetLinkID());
                    Application.OpenURL("https://unity3d.com/legal/privacy-policy");
                }
                if (linkInfo.GetLinkID() == "PrivacyEmail")
                {
                    //Debug.Log("CLICKED on ClickableTerms  TermsEmailID : " + linkInfo.GetLinkID());
                    OnEmailClicked();
                }
                


            }
        }
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void OnEmailClicked()
    {
        //Debug.Log("On Email  Clicked...");
        string email = "renaldietitians@nbt.nhs.uk";
        string subject = MyEscapeURL("");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }



}
