﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class CharacterCustomName : MonoBehaviour
{
    [SerializeField]private GameObject[] characters;
    [SerializeField] private InputField characterName;
    [SerializeField] private ScreenController screencontroller;
    [SerializeField] private RectTransform saraHolder;
    [SerializeField] private GameObject BackButton;


   // Start is called before the first frame update
   void Start()
    {
        screencontroller.OnShowStarted += OnShowCustomDietician;
        //OnShowCustomDietician();
     
    }

    private void OnShowCustomDietician()
    {
        /*if (characterName.text == "")
        {
            characterName.text = "NBT Dietician";
        }

        if(PlayerPrefs.GetString("DieticianName") != "")
        {
            BackButton.SetActive(false);
        }else
        {
            BackButton.SetActive(true);
        } */

        characterName.text = "";
        if (AppManager.Instance.DieticianName.ToString() == "Nbt Dietetic Assistant")
        {
            string str1 = "NBT".ToUpper();
            string defaultDietician = str1 + " " + "Dietetic Assistant";
            characterName.text = defaultDietician;
        }


        for (int i = 0; i < characters.Length; i++)
        {

            characters[i].gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, saraHolder.rect.width);

            if (i == AppManager.Instance.CurrentCharPageID - 1)
            {
                characters[i].SetActive(true);
            }
            else
            {
                characters[i].SetActive(false);
            }
        }
        //characterName.text = AppManager.Instance.DieticianName;
    }

    public void OnSaveClicked()
    {
        string str1 = "NBT".ToUpper();

        string defaultDietician = "";
        if (characterName.text.Trim() == "")
        {
            defaultDietician = str1 + " " + "Dietetic Assistant";
            characterName.text = defaultDietician;
            AppManager.Instance.DieticianName = defaultDietician;
            PlayerPrefs.SetString("DieticianName", defaultDietician);
        }
        else
        {
            AppManager.Instance.DieticianName = characterName.text;
            PlayerPrefs.SetString("DieticianName", characterName.text);
        }
        
        PlayerPrefs.SetInt("DieticianPage", AppManager.Instance.CurrentCharPageID);
        ScreenManager.Instance.Show("ChatbotHomeScreen");
    }

    public void OnBackClicked()
    {
        ScreenManager.Instance.Show("DieticianCustomScreen");
    }
}
