﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrivacyPolicyScreen : MonoBehaviour
{
    [SerializeField] private GameObject scrollRectContainer;
    [SerializeField] private ScreenController screenController;

    // Start is called before the first frame update
    void Start()
    {
        screenController.OnShowStarted += OnShowTerms;

    }
    private void OnShowTerms()
    {
        //if (scrollRectContainer.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition < 1)
        //scrollRectContainer.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 2f;

    }


    public void OnCloseButtonClicked()
    {
        //if (scrollRectContainer.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition < 1)
        //    scrollRectContainer.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 2f;
        StartCoroutine(ResetScrollRect2());

    }

    private IEnumerator ResetScrollRect2()
    {
        yield return new WaitForSeconds(0.5f);

        if (scrollRectContainer.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition < 1)
        {
            scrollRectContainer.transform.GetComponentInParent<ScrollRect>().movementType = ScrollRect.MovementType.Clamped;
            scrollRectContainer.transform.GetComponentInParent<ScrollRect>().verticalNormalizedPosition = 2f;
        }
        yield return new WaitForSeconds(0.25f);

        scrollRectContainer.transform.GetComponentInParent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;
    }

}
