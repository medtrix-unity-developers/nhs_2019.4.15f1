﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ClickableText : MonoBehaviour, IPointerClickHandler
{
    //[SerializeField] private GameObject privacyPopup;
    //[SerializeField] private GameObject termsPopup;


    public void OnPointerClick(PointerEventData eventData)
    {
        //Debug.Log("CLICKED OnPointerClick: ");

        var text = GetComponent<TextMeshProUGUI>();
        if(eventData.button == PointerEventData.InputButton.Left)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, Input.mousePosition, null);
            if(linkIndex > -1)
            {
                var linkInfo = text.textInfo.linkInfo[linkIndex];
                var linkId = linkInfo.GetLinkID();
                //var itemData = FindObjectOfType<ItemDataController>().Get(linkId);
                //Debug.Log("CLICKED LINK NAME : " + linkInfo.GetLinkID());

                if (linkInfo.GetLinkID() == "Terms")
                {
                    ScreenManager.Instance.Show("TermsConditionsScreen");
                }

                if (linkInfo.GetLinkID() == "Potassium")
                {
                    ScreenManager.Instance.Show("InformationsScreen");
                    ToggleAnswerButton.Instance.OnAnswerButtonClicked("Answer_0");
                    RegistrationScreen.Instance.DietaryInfoPopup.SetActive(false);
                }
                else if (linkInfo.GetLinkID() == "Phosphate")
                {
                    ScreenManager.Instance.Show("InformationsScreen");
                    ToggleAnswerButton.Instance.OnAnswerButtonClicked("Answer_1");
                    RegistrationScreen.Instance.DietaryInfoPopup.SetActive(false);
                }
                else if (linkInfo.GetLinkID() == "HealthyEating")
                {
                    ScreenManager.Instance.Show("InformationsScreen");
                    ToggleAnswerButton.Instance.OnAnswerButtonClicked("Answer_2");
                    RegistrationScreen.Instance.DietaryInfoPopup.SetActive(false);
                }
                else if (linkInfo.GetLinkID() == "Salt")
                {
                    ScreenManager.Instance.Show("InformationsScreen");
                    ToggleAnswerButton.Instance.OnAnswerButtonClicked("Answer_3");
                    RegistrationScreen.Instance.DietaryInfoPopup.SetActive(false);
                }
                else if (linkInfo.GetLinkID() == "Fluid")
                {
                    ScreenManager.Instance.Show("InformationsScreen");
                    ToggleAnswerButton.Instance.OnAnswerButtonClicked("Answer_4");
                    RegistrationScreen.Instance.DietaryInfoPopup.SetActive(false);
                }
                else if (linkInfo.GetLinkID() == "Calorie")
                {
                    ScreenManager.Instance.Show("InformationsScreen");
                    ToggleAnswerButton.Instance.OnAnswerButtonClicked("Answer_5");
                    RegistrationScreen.Instance.DietaryInfoPopup.SetActive(false);
                }
                 

            }
        }
    }
 
}
