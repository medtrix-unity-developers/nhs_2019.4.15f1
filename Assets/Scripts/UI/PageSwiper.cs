﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PageSwiper : MonoBehaviour, IDragHandler, IEndDragHandler
{
    private Vector3 panelLocation;
    public float percentThreshold = 0.2f;
    public float easing = 0.5f;
    public int totalPages = 4;
    private int currentPage = 1;
    [SerializeField] private GameObject[] pageMarkers;
    [SerializeField] private GameObject[] avatarImages;
    [SerializeField] private RectTransform saraHolder;
    [SerializeField] private GameObject btn_Next;
    [SerializeField] private GameObject btn_Previous;

    public RectTransform _scrollRectRect;
    public RectTransform _container;

    private PointerEventData _lastPointerData;
    private bool isDragging = false;

    // Start is called before the first frame update
    void Start()
    {
        AppManager.Instance.CurrentCharPageID = currentPage;
        ManageButtonActiveState();
        _scrollRectRect = GetComponent<RectTransform>();
        SetPagePositions();
    }

    public void OnBeginDrag(PointerEventData data)
    {
        //_lastPointerData = data;
    }

    public void OnDrag(PointerEventData data)
    {
        //if (isDragging == true) return;
        //_lastPointerData = data;
        //panelLocation = transform.position;
        //isDragging = true;
    }
    public void OnEndDrag(PointerEventData data)
    {
        
        //float percentage = (data.pressPosition.x - data.position.x) / Screen.width;
        //if (Mathf.Abs(percentage) >= percentThreshold)
        //{
        //    Vector3 newLocation = panelLocation;
        //    if (percentage > 0 && currentPage < totalPages)
        //    {
        //        btn_Previous.GetComponent<Button>().interactable = false;
        //        btn_Next.GetComponent<Button>().interactable = false;
        //        currentPage++;
        //        AppManager.Instance.CurrentCharPageID = currentPage;
                
        //        newLocation += new Vector3(-(Screen.width), 0, 0);
        //    }
        //    else if (percentage < 0 && currentPage > 1)
        //    {
        //        btn_Previous.GetComponent<Button>().interactable = false;
        //        btn_Next.GetComponent<Button>().interactable = false;
        //        currentPage--;
        //        AppManager.Instance.CurrentCharPageID = currentPage;
                
        //        newLocation += new Vector3((Screen.width), 0, 0);
        //    }

        //    if (currentPage < totalPages || currentPage > 0)
        //    {
        //        ChangeMarkerPoint();
        //        StartCoroutine(SmoothMove(transform.position, newLocation, easing));
        //        panelLocation = newLocation;
        //    }
        //}
        //else
        //{
        //    ChangeMarkerPoint();
        //    StartCoroutine(SmoothMove(transform.position, panelLocation, easing));
        //}
    }

    public void CancelDrag()
    {
        //if (_lastPointerData != null)
        //{
        //    _lastPointerData.pointerDrag = null;
        //}
    }

    IEnumerator SmoothMove(Vector3 startpos, Vector3 endpos, float seconds)
    {
        float t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startpos, endpos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
        //isDragging = false;
        //Debug.Log("On End Smooth Move...");
        ManageButtonActiveState();
    }

    public void SaveBotAvatarIDToPlayerPref()
    {
        PlayerPrefs.SetInt("DieticianPage", AppManager.Instance.CurrentCharPageID);
    }

    public void NextImage()
    {
        btn_Previous.GetComponent<Button>().interactable = false;
        btn_Next.GetComponent<Button>().interactable = false;
        //btn_Previous.gameObject.SetActive(false);
        //btn_Next.gameObject.SetActive(false);

        panelLocation = transform.position;
        Vector3 newLocation = panelLocation;
        currentPage++;
        AppManager.Instance.CurrentCharPageID = currentPage;
        //ManageButtonActiveState();
        //PlayerPrefs.SetInt("DieticianPage", AppManager.Instance.CurrentCharPageID);
        newLocation += new Vector3(-(Screen.width), 0, 0);

        ChangeMarkerPoint();
        StartCoroutine(SmoothMove(transform.position, newLocation, easing));
        panelLocation = newLocation;
        //Debug.Log("currentPage:: " + currentPage);
    }

    public void PreviousImage()
    {
        btn_Previous.GetComponent<Button>().interactable = false;
        btn_Next.GetComponent<Button>().interactable = false;
        //btn_Previous.gameObject.SetActive(false);
        //btn_Next.gameObject.SetActive(false);

        panelLocation = transform.position;
        Vector3 newLocation = panelLocation;
        currentPage--;
        AppManager.Instance.CurrentCharPageID = currentPage;
        //ManageButtonActiveState();
        //PlayerPrefs.SetInt("DieticianPage", AppManager.Instance.CurrentCharPageID);
        newLocation += new Vector3(Screen.width, 0, 0);

        ChangeMarkerPoint();
        StartCoroutine(SmoothMove(transform.position, newLocation, easing));
        panelLocation = newLocation;
        //Debug.Log("currentPage:: " + currentPage);
    }

    private void ManageButtonActiveState()
    {
        if(currentPage == 1)
        {
            btn_Previous.GetComponent<Button>().interactable = false;
            btn_Previous.gameObject.SetActive(false);
            btn_Previous.GetComponent<Image>().color = new Color32(200, 200, 200, 255);
        }
        else
        {
            btn_Previous.GetComponent<Button>().interactable = true;
            btn_Previous.gameObject.SetActive(true);
            btn_Previous.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }

        if (currentPage == 4)
        {
            btn_Next.GetComponent<Button>().interactable = false;
            btn_Next.gameObject.SetActive(false);
            btn_Next.GetComponent<Image>().color = new Color32(200, 200, 200, 255);
        }
        else
        {
            btn_Next.GetComponent<Button>().interactable = true;
            btn_Next.gameObject.SetActive(true);
            btn_Next.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
    }

    private void ChangeMarkerPoint()
    {
        for (int i = 0; i < pageMarkers.Length; i++)
        {
            if (i == currentPage - 1)
            {
                pageMarkers[i].transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                pageMarkers[i].transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }

    private void SetPagePositions()
    {
        int width = 0;
        //int height = 0;
        //int offsetX = 0;
        //int containerWidth = 0;
        //int containerHeight = 0;
  
        // screen width in pixels of scrollrect window
        width = (int)_scrollRectRect.rect.width;
        // center position of all pages
        //offsetX = width / 2;
        // total width
        //containerWidth = width; //  * totalPages;
         
 
        // iterate through all container childern and set their positions
        for (int i = 0; i < totalPages; i++)
        {
            
            RectTransform child = _container.GetChild(i).GetComponent<RectTransform>();
            Vector2 childPosition = new Vector2((i * width), 0f);  //  - containerWidth / 2 + offsetX
            child.anchoredPosition = childPosition;
  
        }
    } 
}
