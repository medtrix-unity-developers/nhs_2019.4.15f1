﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NotificationSamples;
using UnityEngine.SceneManagement;
  
#if UNITY_ANDROID
using Unity.Notifications.Android;
#endif

#if UNITY_IOS
using Unity.Notifications.iOS;
#endif

//TODO Mobile Notification.
public class MyNotificationManager : MonoSingleton<MyNotificationManager>
{
    [SerializeField] private GameNotificationsManager notificationManager;
    //[SerializeField] private PendingNotification notification;

 

    private void StartNotification()
    {

#if UNITY_ANDROID
        GameNotificationChannel channel = new GameNotificationChannel("NHS Chatbot", "Medication Reminder", "This is an medication reminder.");
        notificationManager.Initialize(channel);
#endif
  
    }


    // Start is called before the first frame update
    void Start()
    {
        StartNotification();

#if UNITY_ANDROID
        var notificationIntentData = AndroidNotificationCenter.GetLastNotificationIntent();
        if (notificationIntentData != null)
        {
            //var id = notificationIntentData.Id;
            //var channel = notificationIntentData.Channel;
            //var notification = notificationIntentData.Notification;
            GameObject.FindObjectOfType<ChatbotHomeScreen>().OnShowReminderScreen();
            //SceneManager.LoadScene("NewScene");
        }
#endif

#if UNITY_IOS
        {
            var notification = iOSNotificationCenter.GetLastRespondedNotification();
            if (notification != null)
            {
                GameObject.FindObjectOfType<ChatbotHomeScreen>().OnShowReminderScreen();

                //var msg = "Last Received Notification: " + notification.Identifier;
                //msg += "\n - Notification received: ";
                //msg += "\n - .Title: " + notification.Title;
                //msg += "\n - .Badge: " + notification.Badge;
                //msg += "\n - .Body: " + notification.Body;
                //msg += "\n - .CategoryIdentifier: " + notification.CategoryIdentifier;
                //msg += "\n - .Subtitle: " + notification.Subtitle;
                //msg += "\n - .Data: " + notification.Data;
                //Debug.Log(msg);
            }
        }
#endif
    }

    public void ShowNotificationAfterDelay(string title,string body, DateTime time)
    {
 
 #if UNITY_IOS
        var calendarTrigger = new iOSNotificationCalendarTrigger()
        {
            Year = time.Date.Year,
            Month = time.Date.Month,
            Day = time.Date.Day,
            Hour = time.Hour,
            Minute = time.Minute,
            Second = time.Second,
            Repeats = false
        };
 
        var notification = new iOSNotification()
        {
            // You can optionally specify a custom identifier which can later be 
            // used to cancel the notification, if you don't set one, a unique 
            // string will be generated automatically.
 
            Identifier = (time.Date.Day.ToString() + time.Hour.ToString() + time.Minute.ToString() + time.Second.ToString()),
            Title = title,
            Body = body,
            Subtitle = "",
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = calendarTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notification);
#endif

    }



    public void ShowAndroidNotification(string title, string body, DateTime time)
    {
#if UNITY_ANDROID

        IGameNotification createNofitication = notificationManager.CreateNotification();
        if (createNofitication != null)
        {
            createNofitication.Title = title;
            createNofitication.Body = body;
            createNofitication.DeliveryTime = time;
            createNofitication.SmallIcon = "icon_1";
            createNofitication.LargeIcon = "icon_2";
            createNofitication.Id = Int32.Parse(time.Date.Day.ToString() + time.Hour.ToString() + time.Minute.ToString() + time.Second.ToString());
            var notificationToDisplay = notificationManager.ScheduleNotification(createNofitication);
            notificationToDisplay.Reschedule = true;
        }
#endif

        /*
#if UNITY_EDITOR

                 var c = new AndroidNotificationChannel()
                {
                    Id = "NHS Chatbot",
                    Name = "Medication Reminder",
                    Importance = Importance.High,
                    Description = "Medication Reminder",

                };
                AndroidNotificationCenter.RegisterNotificationChannel(c);

                var notification = new AndroidNotification();
                notification.Title = title;
                notification.Text = body;
                notification.SmallIcon = "icon_1";
                notification.LargeIcon = "icon_2";
                notification.FireTime = time;
                notification.Number = Int32.Parse(time.Date.Day.ToString() + time.Hour.ToString() + time.Minute.ToString());
                notification.IntentData = (time.Date.Day.ToString() + time.Hour.ToString() + time.Minute.ToString());
                var identifier = AndroidNotificationCenter.SendNotification(notification, "NHS Chatbot");
                //AndroidNotificationCenter.SendNotification(notification, "NHS Chatbot");
#endif
        */
    }


    public void CancelScheduledNotification(string title, string body, DateTime time)
    {

#if UNITY_IOS || UNITY_IPHONE
            string idValue = (time.Date.Day.ToString() + time.Hour.ToString() + time.Minute.ToString() + time.Second.ToString());
            iOSNotificationCenter.RemoveScheduledNotification(idValue);
#endif

#if UNITY_ANDROID
            int idValue = Int32.Parse(time.Date.Day.ToString() + time.Hour.ToString() + time.Minute.ToString() + time.Second.ToString());
            notificationManager.CancelNotification(idValue);
#endif

    }

}
