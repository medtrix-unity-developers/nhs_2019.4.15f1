﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RB_GameManager : MonoBehaviour
{
    [SerializeField]
    private ScrollRect scrollRect_Month;
    [SerializeField]
    private RectTransform contentPanel_Month;
    [SerializeField]
    private RectTransform[] target_Month;

    [SerializeField]
    private ScrollRect scrollRect_Date;
    [SerializeField]
    private RectTransform contentPanel_Date;
    [SerializeField]
    private RectTransform[] target_Date;

    [SerializeField]
    private ScrollRect scrollRect_Month_MiniCalendar;
    [SerializeField]
    private RectTransform contentPanel_Month_MiniCalendar;
    [SerializeField]
    private RectTransform[] target_Month_MiniCalendar;

    [SerializeField]
    private ScrollRect scrollRect_Date_MiniCalendar;
    [SerializeField]
    private RectTransform contentPanel_Date_MiniCalendar;
    [SerializeField]
    private RectTransform[] target_Date_MiniCalendar;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SnapToTargetMonth(int value)
    {
        Canvas.ForceUpdateCanvases();

        contentPanel_Month.anchoredPosition =
            (Vector2)scrollRect_Month.transform.InverseTransformPoint(contentPanel_Month.position)
            - (Vector2)scrollRect_Month.transform.InverseTransformPoint(target_Month[value - 1].position);

        //for (int i=0; i<contentPanel_Month.childCount; i++)
        //{
        //    if (i == value - 1)
        //    {
        //        target_Month[i].gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        //        target_Month[i].gameObject.transform.GetChild(1).GetComponent<Text>().color = new Color32(0, 94, 184, 255);
        //        //selected
                
        //    }
        //    else
        //    {
        //        target_Month[i].gameObject.GetComponent<Image>().color = new Color32(232, 237, 238, 100);
        //        target_Month[i].gameObject.transform.GetChild(1).GetComponent<Text>().color = new Color32(191, 191, 191, 255);
        //    }
        //}
    }

    public void SnapToTargetDate(int value)
    {
        
        Canvas.ForceUpdateCanvases();

        contentPanel_Date.anchoredPosition =
            (Vector2)scrollRect_Date.transform.InverseTransformPoint(contentPanel_Date.position)
            - (Vector2)scrollRect_Date.transform.InverseTransformPoint(target_Date[value - 1].position);

        //for (int i = 0; i < contentPanel_Date.childCount; i++)
        //{
        //    if (i == value - 1)
        //    {
        //        target_Date[i].gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        //        target_Date[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(0, 94, 184, 255);
        //        target_Date[i].gameObject.transform.GetChild(3).GetComponent<Text>().color = new Color32(0, 94, 184, 255);
        //        //selected

        //    }
        //    else
        //    {
        //        target_Date[i].gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color32(232, 237, 238, 100);
        //        target_Date[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(191, 191, 191, 255);
        //        target_Date[i].gameObject.transform.GetChild(3).GetComponent<Text>().color = new Color32(191, 191, 191, 255);
        //    }
        //}
    }

    public void SnapToTargetMonth_MiniCalendar(int value)
    {
        Canvas.ForceUpdateCanvases();

        contentPanel_Month_MiniCalendar.anchoredPosition =
            (Vector2)scrollRect_Month_MiniCalendar.transform.InverseTransformPoint(contentPanel_Month_MiniCalendar.position)
            - (Vector2)scrollRect_Month_MiniCalendar.transform.InverseTransformPoint(target_Month_MiniCalendar[value - 1].position);
    }

    public void SnapToTargetDate_MiniCalendar(int value)
    {

        Canvas.ForceUpdateCanvases();

        contentPanel_Date_MiniCalendar.anchoredPosition =
            (Vector2)scrollRect_Date_MiniCalendar.transform.InverseTransformPoint(contentPanel_Date_MiniCalendar.position)
            - (Vector2)scrollRect_Date_MiniCalendar.transform.InverseTransformPoint(target_Date_MiniCalendar[value - 1].position);
    }
}
