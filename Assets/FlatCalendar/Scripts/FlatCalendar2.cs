﻿ 
#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using System.Collections;
using System.Globalization;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using static FlatCalendar2.EventExportList;
using UnityEngine.UI.Extensions;
using System.Threading.Tasks;
using System.Threading;

public class FlatCalendar2 : MonoBehaviour
{
    public CustomDerivedCalendar customDerivedCalendar;

 
	/**
	 * Max day slots (DO NOT CHANGE THIS VALUE)
	 */
	public static readonly int max_day_slots = 38;

	/**
	 * Current UI Style
	 */
	public int current_UiStyle;

    /**
	 * Start Options
	 */
    public bool eventNotification;
    public bool startWeekFromSunday;
    public int current_UiLanguage;
    public bool initCalendarFromFile;
    public string filePath;
    private int currentDaySlot;
    /**
    * Gameobjects Fields 
     */
    //public Toggle toggleDeleteEvent_this, toggleDeleteEvent_all;
    public Toggle toggleEditEvent_this, toggleEditEvent_all;
    public Image januarySlot;
    public GameObject[] daysOfWeek_events_label;
    public GameObject[] sliderDaysButtons;
    public GameObject[] sliderMonthsButtons;
    public GameObject[] sliderYearsButtons;

    public GameObject btn_SwitchModality;
    public GameObject slider;

    //0 = SliderDaysButtons ScrollRect
    //1 = SliderMonthButtons ScrollRect
    //2 = SliderYearButtons ScrollRect
    public ScrollRect[] scrollRects;
    public GameObject grid;

    public GameObject btn_nextYear;
    public GameObject btn_prevYear;
    public GameObject btn_nextMonth;
    public GameObject btn_prevMonth;
    public GameObject btn_calendar;
    public GameObject label_year;
    public GameObject label_month;
    public GameObject label_dayOfWeek;
    public GameObject label_dayNumber;

    public GameObject event_Visualizer;
    public GameObject event_Visualizer_backButton;

    //0 = slider Button
    //1 = grid Button
    //2 = back Button
    //3 = set alarm
    public GameObject[] btn_AddEvent;
    public bool currentAlarmValue = true;
    public GameObject addEvent_Visualizer;
    public GameObject eventObj;
    public EventObj modifingEvent;
    public GameObject eventsScrollView;
    public GameObject eventObjcContent;
    public GameObject confirmDelete;
    public GameObject deleteEventBox;

    //0 = hours slider
    //1 = minutes slider
    public GameObject[] field_TimeSlider;
    public int selected_hours = -1;
    public int selected_minutes = -1;

    //0 = NewEvent
    //1 = Event time
    //2 = Event title
    //3 = Event description
    //4 = Todays Events
    //5 = Save Button
    //6 = Today's Events
    public GameObject[] add_event_labels;
    public GameObject field_Name;
    public GameObject label_Date;
    public GameObject field_timeError;
    public GameObject field_Description;
    public GameObject btn_Confirm;

    [SerializeField] private GameObject ReminderDateErrorMessage;

    public GameObject reminder_FooterPanel;
    [SerializeField] private GameObject DeleteReminderPanel;

    /**
	 * Export Event class Object
	 */
    public class EventExportList
    {
        public List<EventExport> calendar { get; set; }

        public class EventExport
        {
            public bool alarm { get; set; }
            public DateTime day { get; set; }
            public string name { get; set; }
            public string description { get; set; }

            public int userId { get; set; }
            public DateTime startDate { get; set; }
            public DateTime endDate { get; set; }

            public string storedCustomDays { get; set; }
            public string storedMedicineTimings { get; set; }
            public string storedReminderTimes { get; set; }

        }
    }

    /**
	 * Event class Object
	 */
    public class EventObj
	{
        public bool alarm = true;
		public string name;
		public string description;
        public TimeSpan ? time;
        public int userId;
        public DateTime startDate;
        public DateTime endDate;

        public string storedCustomDays;
        public string storedMedicineTimings;
        public string storedReminderTimes;

        public EventObj(string _name, string _description, TimeSpan _time)
        {
            name = _name;
            description = _description;
            time = _time;
        }

        public EventObj(string _name, string _description, TimeSpan _time, int _userId, DateTime _startDate, DateTime _endDate, string _storedCustomDays, string _storedMedicineTimings, string _storedReminderTimes)
		{
			name 		= _name;
			description = _description;
            time = _time;
            userId = _userId;
            startDate = _startDate;
            endDate = _endDate;

            storedCustomDays = _storedCustomDays;
            storedMedicineTimings = _storedMedicineTimings;
            storedReminderTimes = _storedReminderTimes;
        }

        public EventObj(string _name, string _description)
        {
            name = _name;
            description = _description;
            time = null;
        }

        public void print()
		{
			//Debug.Log("Name Event: " + name + " Description Event: " + description);
            //GleyNotifications.SendNotification("NHS App"+name, description, new System.TimeSpan(0, 1, 0), null, null, "Opened from NHS Minimized Notification");
        }
    }

	/**
	 * Time Structure Object 
	 */
	public struct TimeObj
	{
		public int    year;
		public int    month;
		public int    day;
		public int    totalDays;
		public string dayOfWeek;
		public int    dayOffset;

		public TimeObj(int _year,int _month,int _day, int _totalDays, string _dayOfWeek, int _dayOffset)
		{
			year      = _year;
			month     = _month;
			day       = _day;
			totalDays = _totalDays;
			dayOffset = _dayOffset;
			dayOfWeek = _dayOfWeek;
		}

		public void print()
		{
			//Debug.Log("Year:"+year+" Month:"+month+" Day:"+day+" Day of Week:"+dayOfWeek);
		}
	}

	/**
	 * Current Time 
	 */
	public TimeObj currentTime;

    /**
	 * Notifyed Events 
	 */
    public static List<EventObj> notifyedEvents;

	/**
	 * Event List 
	 */
	public static Dictionary<int,Dictionary<int,Dictionary<int,List<EventObj>>>> events_list; // <Year,<Month,<Day,Number of Events>>>

	/**
	 * Delegate Callbacks 
	 */
	public delegate void Delegate_OnDaySelected(TimeObj time);
	public delegate void Delegate_OnEventSelected(TimeObj time, List<EventObj> evs);
	public delegate void Delegate_OnMonthChanged(TimeObj time);
	public delegate void Delegate_OnNowDay(TimeObj time);
    public delegate void Delegate_OnTriggerEvent(EventObj evnt);
    public Delegate_OnDaySelected   delegate_ondayselected;
	public Delegate_OnEventSelected delegate_oneventselected;
	public Delegate_OnMonthChanged  delegate_onmonthchanged;
	public Delegate_OnNowDay		delegate_onnowday;
    public Delegate_OnTriggerEvent  delegate_ontriggerevent;


    // Use this for initialization
    virtual public void initFlatCalendar()
	{	
        // Enable components to be find by the StyleClass 
        grid.SetActive(true);
        event_Visualizer.SetActive(true);
        slider.SetActive(true);
        addEvent_Visualizer.SetActive(true);

        // Add Event Listeners
        addEventsListener();

        // Apply UI Color style
        FlatCalendarStyle2.changeUIStyle(current_UiStyle);

        // Disable components
        event_Visualizer.SetActive(false);
        slider.SetActive(false);
        addEvent_Visualizer.SetActive(false);
        // Set current time
        setCurrentTime();

        // Initialize event list
        events_list = new Dictionary<int, Dictionary<int, Dictionary<int,List<EventObj>>>>();

        // Initialize notifyed events list
        notifyedEvents = new List<EventObj>();

		// Update Calendar with Current Data
		updateCalendar(currentTime.month,currentTime.year);

        // start thread for notification process
        if (eventNotification && delegate_ontriggerevent != null)
            StartCoroutine(CheckEventNotification());

        // Set path and try import to events data
        if (initCalendarFromFile)
        {
            if (filePath == "")
                filePath = Application.persistentDataPath + "/events.json";
            importFlatCalendarEvents(filePath);
        }
        else filePath = "";

        SnapToTargetMonthAndDate();
        // Show events
        populateEventVisualizer();
    }    
    
    // Use this for storing events as json file
    // The path parameter is the saving location with the file name.json
    public void exportFlatCalendarEvents(string path)
    {
        List<EventExport> calendar  = new List<EventExport>();
        foreach (var year_items in events_list)
        {
            int year = year_items.Key;
            foreach (var month_items in events_list[year])
            {
                int month = month_items.Key;
                foreach (var day_items in events_list[year][month])
                {
                    int day = day_items.Key;
                    List<EventObj> eventsObj = events_list[year][month][day];
                    foreach (EventObj eventObj in eventsObj)
                    {
                        EventExport current = new EventExport();
                        TimeSpan time;

                        if (eventObj.time != null)
                        {
                            time = (TimeSpan)eventObj.time;
                            current.day = new DateTime(year, month, day, time.Hours, time.Minutes, 00);
                        }
                        else
                        {
                            current.day = new DateTime(year, month, day, 00, 00, 01);
                        }
                        current.name = eventObj.name;
                        current.alarm = eventObj.alarm;
                        current.description = eventObj.description;
                        current.userId = eventObj.userId;
                        current.startDate = eventObj.startDate;
                        current.endDate = eventObj.endDate;

                        current.storedCustomDays = eventObj.storedCustomDays;
                        current.storedMedicineTimings = eventObj.storedMedicineTimings;
                        current.storedReminderTimes = eventObj.storedReminderTimes;

                        calendar.Add(current);
                    }
                }
            }
        }
        calendar = calendar.OrderBy(item => item.day).ToList();
        string json = JsonConvert.SerializeObject(calendar);
        try
        {
            File.WriteAllText(path, json);
            Debug.Log("File Saved");
        }
        catch (Exception)
        {
            Debug.Log("File invalid path");
        }
    }

    // Use this for import your events as json file
    // The path parameter is the current json file location with the file name
    public void importFlatCalendarEvents(string path)
    {
        string json;
        List<EventExport> calendar;
        if (File.Exists(path))
            json = File.ReadAllText(path);
        else
        {
            if(filePath != Application.persistentDataPath + "/events.json")
                Debug.Log("File invalid path");
            return;
        }
        try
        {
            calendar = JsonConvert.DeserializeObject<List<EventExport>>(json);
        }
        catch (Exception)
        {
            Debug.Log("File invalid format");
            return;
        }

        foreach (EventExport evnt in calendar)
        {
            TimeSpan invalid = new TimeSpan(00, 00, 01);
            TimeSpan time = new TimeSpan(evnt.day.Hour, evnt.day.Minute, evnt.day.Second);
            EventObj eventObj;
            if(invalid.Equals(time))
                eventObj = new EventObj(evnt.name, evnt.description);
            else 
                eventObj = new EventObj(evnt.name, evnt.description, time);

            eventObj.alarm = evnt.alarm;
            eventObj.userId = evnt.userId;
            eventObj.startDate = evnt.startDate;
            eventObj.endDate = evnt.endDate;
            eventObj.storedCustomDays = evnt.storedCustomDays;
            eventObj.storedMedicineTimings = evnt.storedMedicineTimings;
            eventObj.storedReminderTimes = evnt.storedReminderTimes;
            addEvent(evnt.day.Year, evnt.day.Month, evnt.day.Day, eventObj);
        }
    }

	void Start () { }

	void Update()    {
        //Debug.Log("Date/Month:: " + currentTime.day + " :: " + currentTime.month);
        if (Input.GetKeyDown(KeyCode.Q)){
            onSliderUpdateCaledar();
        }
    }

    IEnumerator CheckEventNotification()
    {
        DateTime now = DateTime.Now;
        if (checkEventExist(now.Year, now.Month, now.Day))
        {
            List<EventObj> todays_events = events_list[now.Year][now.Month][now.Day];
            foreach (EventObj currentEvent in todays_events)
            {
                TimeSpan now_time = new TimeSpan(now.Hour, now.Minute, now.Second);
                if (now_time > currentEvent.time && currentEvent.alarm)
                {
                    if (!notifyedEvents.Contains(currentEvent))
                    {
                        delegate_ontriggerevent(currentEvent);
                        notifyedEvents.Add(currentEvent);
                    }
                }
            }
        }
        yield return new WaitForSeconds(3);
        if(eventNotification)
            StartCoroutine(CheckEventNotification());
    }

    // Update Calendar
    public void updateCalendar(int month_number, int year)
	{
		// Populate day slots
		populateAllSlot(month_number,year);

		// Update Year and Month Label
		label_year.GetComponent<Text>().text      = "" + currentTime.year;
		label_month.GetComponent<Text>().text     = getMonthStringFromNumber(currentTime.month);
        label_year.GetComponent<Text>().color = FlatCalendarStyle2.color_bubbleSelectionMarker;
        label_month.GetComponent<Text>().color = FlatCalendarStyle2.color_bubbleSelectionMarker;
        markSelectionDay(currentTime.day);
        onSliderUpdateCaledar();
    }
    virtual public void onSliderUpdateCaledar()
    {
        //Debug.Log("comingFromEvnt");

        //set Days
        foreach (GameObject button in sliderDaysButtons)
        {
            button.SetActive(false);
            //button.GetComponentInChildren<Image>().enabled = false;
        }

        for (int i = 0; i < currentTime.totalDays; i++)
        {
            sliderDaysButtons[i].SetActive(true);
            Text[] day = sliderDaysButtons[i].GetComponentsInChildren<Text>();
            day[1].text = getDayOfWeek(currentTime.year, currentTime.month, i + 1);
            sliderDaysButtons[i].gameObject.transform.GetChild(0).GetComponent<Image>().enabled = true;

            if (checkEventExist(currentTime.year, currentTime.month, i + 1))
            {
                sliderDaysButtons[i].transform.GetChild(4).GetComponent<Image>().enabled = true;    //GetComponentInChildren<Image>().enabled = true;
                sliderDaysButtons[i].transform.GetChild(4).GetComponent<Image>().color = FlatCalendarStyle2.color_bubbleEvent;     //GetComponentInChildren<Image>().color = FlatCalendarStyle2.color_bubbleEvent;
            }
            else
            {
                sliderDaysButtons[i].transform.GetChild(4).GetComponent<Image>().enabled = false;    
            }

            if (currentTime.day == i + 1)
            {
                sliderDaysButtons[i].gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                sliderDaysButtons[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(0, 94, 184, 255);
                sliderDaysButtons[i].gameObject.transform.GetChild(3).GetComponent<Text>().color = new Color32(0, 94, 184, 255);
            }
            else
            {
                sliderDaysButtons[i].gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color32(232, 237, 238, 100);
                sliderDaysButtons[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(191, 191, 191, 255);
                sliderDaysButtons[i].gameObject.transform.GetChild(3).GetComponent<Text>().color = new Color32(191, 191, 191, 255);
            }
 
        }

        //set Month
        for(int i=0; i< 12; i++)
        {
            sliderMonthsButtons[i].GetComponentInChildren<Text>().text = getMonthStringFromNumber(i+1);
            if (i + 1 == currentTime.month)
            {
                sliderMonthsButtons[i].GetComponentInChildren<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                sliderMonthsButtons[i].GetComponentInChildren<Text>().color = new Color32(0, 94, 184, 255);
            }
            else
            {
                //Debug.Log(sliderMonthsButtons[i].gameObject.GetComponent<Image>().color);
                sliderMonthsButtons[i].GetComponentInChildren<Image>().color = new Color(0.91f, 0.93f, 0.933f, 0.39f);
                sliderMonthsButtons[i].GetComponentInChildren<Text>().color = new Color32(191, 191, 191, 255);
            }
        }

        //set Year
        for (int i = 0; i < 16; i++)
        {
            sliderYearsButtons[i].GetComponentInChildren<Text>().text = (System.DateTime.Now.Year + i).ToString();
            if(sliderYearsButtons[i].GetComponentInChildren<Text>().text == (currentTime.year).ToString())
            {
                sliderYearsButtons[i].GetComponentInChildren<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                sliderYearsButtons[i].GetComponentInChildren<Text>().color = new Color32(0, 94, 184, 255);
            }
            else
            {
                //sliderYearsButtons[i].GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_dayTextNormalGrid;
                sliderYearsButtons[i].GetComponentInChildren<Image>().color = new Color(0.91f, 0.93f, 0.933f, 0.39f);
                sliderYearsButtons[i].GetComponentInChildren<Text>().color = new Color32(191, 191, 191, 255);
            }
        }
        //set Events       
        populateEventVisualizer();
    }

    public void SnapToTargetMonthAndDate()
    {
        GameObject.FindObjectOfType<RB_GameManager>().SnapToTargetDate(currentTime.day);
        GameObject.FindObjectOfType<RB_GameManager>().SnapToTargetMonth(currentTime.month);
    }

    public void refreshCalendar()
	{
		populateAllSlot(currentTime.month,currentTime.year);
        markSelectionDay(currentTime.day);
    }

    /* Get Month String from Montth Number 
	 * 
	 * Example: Genuary <====> 1
	 */
    public string getMonthStringFromNumber(int month_number)
	{
		string month = "";
        if(current_UiLanguage == 0)
        {
            if (month_number == 1) month = "January";
            if (month_number == 2) month = "February";
            if (month_number == 3) month = "March";
            if (month_number == 4) month = "April";
            if (month_number == 5) month = "May";
            if (month_number == 6) month = "June";
            if (month_number == 7) month = "July";
            if (month_number == 8) month = "August";
            if (month_number == 9) month = "September";
            if (month_number == 10) month = "October";
            if (month_number == 11) month = "November";
            if (month_number == 12) month = "December";
        }

        if(current_UiLanguage == 1)
        {
            if (month_number == 1) month = "Gennaio";
            if (month_number == 2) month = "Febbraio";
            if (month_number == 3) month = "Marzo";
            if (month_number == 4) month = "Aprile";
            if (month_number == 5) month = "Maggio";
            if (month_number == 6) month = "Giugno";
            if (month_number == 7) month = "Luglio";
            if (month_number == 8) month = "Agosto";
            if (month_number == 9) month = "Settembre";
            if (month_number == 10) month = "Ottobre";
            if (month_number == 11) month = "Novembre";
            if (month_number == 12) month = "Dicembre";
        }

        if (current_UiLanguage == 2)
        {
            if (month_number == 1) month = "Enero";
            if (month_number == 2) month = "Febrero";
            if (month_number == 3) month = "Marzo";
            if (month_number == 4) month = "Abril";
            if (month_number == 5) month = "Mayo";
            if (month_number == 6) month = "Junio";
            if (month_number == 7) month = "Julio";
            if (month_number == 8) month = "Agosto";
            if (month_number == 9) month = "Septiembre";
            if (month_number == 10) month = "Octubre";
            if (month_number == 11) month = "Noviembre";
            if (month_number == 12) month = "Diciembre";
        }

        return month;
	}

    public string getShortMonthNameFromNumber(int month_number)
    {
        string month = "";
      
            if (month_number == 1) month = "Jan";
            if (month_number == 2) month = "Feb";
            if (month_number == 3) month = "Mar";
            if (month_number == 4) month = "Apr";
            if (month_number == 5) month = "May";
            if (month_number == 6) month = "Jun";
            if (month_number == 7) month = "Jul";
            if (month_number == 8) month = "Aug";
            if (month_number == 9) month = "Sep";
            if (month_number == 10) month = "Oct";
            if (month_number == 11) month = "Nov";
            if (month_number == 12) month = "Dec";
    
        return month;

    }

    /* 
     * Get Day of Week From Year, Month and Day
     * 
     * Example: Monday <===> 2016,1,2
     */
    public string getDayOfWeek(int year, int month, int day)
	{
		System.DateTime dateValue = new System.DateTime(year,month,day);
        string englishName = dateValue.DayOfWeek.ToString();

        if (current_UiLanguage == 0)
            switch (englishName)
            {
                case "Monday":
                    return "Mon";

                case "Tuesday":
                    return "Tue";

                case "Wednesday":
                    return "Wed";

                case "Thursday":
                    return "Thu";

                case "Friday":
                    return "Fri";

                case "Saturday":
                    return "Sat";

                case "Sunday":
                    return "Sun";
            }
        if (current_UiLanguage == 1)
            switch (englishName)
            {
                case "Monday":
                    return "Lunedì";

                case "Tuesday":
                    return "Martedì";

                case "Wednesday":
                    return "Mercoledì";

                case "Thursday":
                    return "Giovedì";

                case "Friday":
                    return "Venerdì";

                case "Saturday":
                    return "Sabato";

                case "Sunday":
                    return "Domenica";
            }
        else if (current_UiLanguage == 2)
            switch (englishName)
            {
                case "Monday":
                    return "Lunes";

                case "Tuesday":
                    return "Martes";

                case "Wednesday":
                    return "Miércoles";

                case "Thursday":
                    return "Jueves";

                case "Friday":
                    return "Viernes";

                case "Saturday":
                    return "Sábado";

                case "Sunday":
                    return "Domingo";
            }
        return englishName;
    }

    /* 
	 * Get index of first slot where start day numeration
	 */
    public int getIndexOfFirstSlotInMonth(int year, int month)
	{
		int indexOfFirstSlot = 0;

		System.DateTime dateValue = new System.DateTime(year,month,1);
		string dayOfWeek          = dateValue.DayOfWeek.ToString();

		if(dayOfWeek == "Monday")    indexOfFirstSlot = 0;
		if(dayOfWeek == "Tuesday")   indexOfFirstSlot = 1;
		if(dayOfWeek == "Wednesday") indexOfFirstSlot = 2;
		if(dayOfWeek == "Thursday")  indexOfFirstSlot = 3;
		if(dayOfWeek == "Friday")    indexOfFirstSlot = 4;
		if(dayOfWeek == "Saturday")  indexOfFirstSlot = 5;
		if(dayOfWeek == "Sunday")    indexOfFirstSlot = 6;

		return indexOfFirstSlot;
	}

	/*
	 * Disable all day slot 
	 */
	void disableAllSlot()
	{
		for(int i = 0; i < max_day_slots; i++)
			disableSlot(i+1);
	}

	/* 
	 * Disable day slot
	 */
	void disableSlot(int numSlot)
	{
        if (!grid.activeInHierarchy)
            return;
		GameObject day_slot = GameObject.Find("Slot_"+ (numSlot));
		day_slot.GetComponent<Button>().enabled = false;
		day_slot.GetComponent<Image>().enabled  = false;
        day_slot.GetComponentsInChildren<Image>()[1].enabled = false;
        day_slot.GetComponent<Button>().GetComponentInChildren<Text>().enabled = false;
	}

	void setNormalSlot(int numSlot)
    {
        if (!grid.activeInHierarchy)
            return;
        GameObject day_slot = GameObject.Find("Slot_"+ (numSlot));
		day_slot.GetComponent<Button>().enabled = true;
		day_slot.GetComponent<Image>().enabled  = false;
        day_slot.GetComponentsInChildren<Image>()[1].enabled = false;
        day_slot.GetComponent<Button>().GetComponentInChildren<Text>().enabled = true;
		day_slot.GetComponent<Button>().GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_dayTextNormal;
	}

	void setEventSlot(int numSlot)
	{
        if (!grid.activeInHierarchy)
            return;
        GameObject day_slot = GameObject.Find("Slot_"+ (numSlot));
		day_slot.GetComponent<Button>().enabled = true;
        day_slot.GetComponent<Image>().enabled = false;
        day_slot.GetComponentsInChildren<Image>()[1].enabled  = true;
		day_slot.GetComponentsInChildren<Image>()[1].color    = FlatCalendarStyle2.color_bubbleEvent;
		day_slot.GetComponent<Button>().GetComponentInChildren<Text>().enabled = true;
	}

    public void populateEventVisualizer()
    {
        List<EventObj> orderedEventsList = getEventList(currentTime.year, currentTime.month, currentTime.day).OrderBy(x => x.time).ToList();
        foreach (Transform child in eventObjcContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        foreach (EventObj e in orderedEventsList)
        {
            var evnt = Instantiate(eventObj, eventObjcContent.transform);
            Button[] btns = evnt.GetComponentsInChildren<Button>();
            /*foreach(Image img in evnt.GetComponentsInChildren<Image>())
            {
                if (!e.alarm)
                    img.color = new Color32(130, 130, 130, 255);
                else
                    img.color = FlatCalendarStyle2.color_bubbleEvent;
            }*/
            btns[2].GetComponent<Image>().color = FlatCalendarStyle2.color_topGrid;
            btns[1].GetComponent<Image>().color = FlatCalendarStyle2.color_topGrid;

            btns[2].onClick.AddListener(() => evtListener_ModifyEvent(e));
            btns[1].onClick.AddListener(() => removeEvent(currentTime.year, currentTime.month, currentTime.day, e));
            evnt.GetComponent<Button>().onClick.RemoveAllListeners();
            evnt.GetComponent<Button>().onClick.AddListener(() => ShowEvent(e));
            Text[] t = evnt.GetComponentsInChildren<Text>();
            t[0].text = e.name;
            if (e.time.Value.Hours < 10)
                t[1].text = "0" + e.time.Value.Hours.ToString();
            else
                t[1].text = e.time.Value.Hours.ToString();

            if (e.time.Value.Minutes < 10)
                t[1].text += ":0" + e.time.Value.Minutes.ToString();
            else
                t[1].text += ":" + e.time.Value.Minutes.ToString();

                t[4].text = e.description;

        }
        FlatCalendarStyle2.changeUIStyle(current_UiStyle);

    }

    void populateAllSlot(int monthNumber, int year)
	{
		// Disable all slots
		disableAllSlot();

		// Update slots
		for (int i = 0; i < currentTime.totalDays; i++)
		{	
			// Put text
			changeTextSlot(i+currentTime.dayOffset+1,""+(i+1));

			// Check if slot event
			if(checkEventExist(currentTime.year,currentTime.month,(i+1)))
				setEventSlot(i+currentTime.dayOffset+1);
			else
				setNormalSlot(i+currentTime.dayOffset+1);
		}
	}

	void changeTextSlot(int numSlot, string text)
	{
        if (!grid.activeInHierarchy)
            return;
        GameObject day_slot = GameObject.Find("Slot_"+numSlot);
		day_slot.GetComponent<Button>().GetComponentInChildren<Text>().text = text;
	}

	int getDayInSlot(int numSlot)
	{
		GameObject day_slot = GameObject.Find("Slot_"+ (numSlot));
		string txt = day_slot.GetComponentInChildren<Text>().text;
		return int.Parse(txt);
	}

	public void markSelectionDay(int day)
	{
        if (!grid.activeInHierarchy)
            return;
        GameObject day_slot = GameObject.Find("Slot_"+ (day+currentTime.dayOffset));

        day_slot.GetComponent<Image>().enabled = true;

        if(day == DateTime.Now.Day && currentTime.month == DateTime.Now.Month && currentTime.year == DateTime.Now.Year)
        {
            day_slot.GetComponentsInChildren<Image>()[2].enabled = true;
            day_slot.GetComponentsInChildren<Image>()[2].color = FlatCalendarStyle2.color_bubbleSelectionMarker;
            currentDaySlot = day + currentTime.dayOffset;
        }

        day_slot.GetComponent<Image>().color = FlatCalendarStyle2.color_bubbleSelectionMarker;
        for (int i = 0; i < max_day_slots; i++)
            GameObject.Find("Slot_" + (i + 1)).GetComponentInChildren<Text>().color = new Color32(255, 255, 255, 255);
        day_slot.GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_topGrid;

        // Update Text
        setLabelDayOfWeek();
        label_dayNumber.GetComponent<Text>().text = "" + currentTime.day;
        label_dayNumber.GetComponent<Text>().color = FlatCalendarStyle2.color_bubbleSelectionMarker;
    }

    void unmarkSelctionDay(int day)
	{
		GameObject day_slot = GameObject.Find("Slot_"+ (day+currentTime.dayOffset));
        // Change Image
        if (!checkEventExist(currentTime.year,currentTime.month,day))
		{
			setNormalSlot(day+currentTime.dayOffset);
		}
        else
        {
            setEventSlot(day + currentTime.dayOffset);
        }
	}

	public static bool checkEventExist(int year, int month, int day)
	{
		if(events_list == null)
			return false;

		if(!events_list.ContainsKey(year))
			return false;

		if(!events_list[year].ContainsKey(month))
			return false;

		if(!events_list[year][month].ContainsKey(day))
			return false;

		if(events_list[year][month][day] == null)
			return false;

		if(events_list[year][month][day].Count == 0)
			return false;

		return true;
	}

	virtual protected void addEventsListener()
	{
        btn_SwitchModality.GetComponent<Button>().onClick.AddListener(() => evtListener_SwitchModality());
        btn_nextMonth.GetComponent<Button>().onClick.AddListener(() => evtListener_NextMonth());
		btn_prevMonth.GetComponent<Button>().onClick.AddListener(() => evtListener_PreviousMonth());
        btn_nextYear.GetComponent<Button>().onClick.AddListener(() => evtListener_NextYear());
        btn_prevYear.GetComponent<Button>().onClick.AddListener(() => evtListener_PreviousYear());
        btn_calendar.GetComponent<Button>().onClick.AddListener(()   => evtListener_GoToNowday());
        //btn_AddEvent[0].GetComponent<Button>().onClick.AddListener(() => evtListener_ShowAddEvent());
        //btn_AddEvent[1].GetComponent<Button>().onClick.AddListener(() => evtListener_ShowAddEvent());
        //btn_AddEvent[2].GetComponent<Button>().onClick.AddListener(() => evtListener_ShowAddEvent());
        btn_AddEvent[3].GetComponent<Button>().onClick.AddListener(() => evtListener_onAlarm_Enable_Disable());        
        //btn_Confirm.GetComponent<Button>().onClick.AddListener(() => customDerivedCalendar.evtListener_AddNewEvent());
        event_Visualizer_backButton.GetComponent<Button>().onClick.AddListener(() => evtListenet_VisualizerBackButton());
        for (int i = 0; i < max_day_slots; i++)
			GameObject.Find("Slot_"+(i+1)).GetComponent<Button>().onClick.AddListener(() => evtListener_DaySelected());
	}

	public void setCurrentTime()
	{
		currentTime.year      = System.DateTime.Now.Year;
		currentTime.month     = System.DateTime.Now.Month;
		currentTime.day       = System.DateTime.Now.Day;
		currentTime.dayOfWeek = System.DateTime.Now.DayOfWeek.ToString();
		currentTime.totalDays = System.DateTime.DaysInMonth(currentTime.year,currentTime.month);
		currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year,currentTime.month);
        //btn_nextYear.GetComponent<Text>().text = (currentTime.year + 1).ToString();
        //btn_prevYear.GetComponent<Text>().text = (currentTime.year - 1).ToString();
        if (startWeekFromSunday)
            currentTime.dayOffset++;
    }

    public void getCurrentTime()
    {
        //currentTime.year = 
    }

	void setCurrentTime(TimeObj obj)
	{
		obj.year      = System.DateTime.Now.Year;
		obj.month     = System.DateTime.Now.Month;
		obj.day       = System.DateTime.Now.Day;
		obj.dayOfWeek = System.DateTime.Now.DayOfWeek.ToString();
		obj.totalDays = System.DateTime.DaysInMonth(obj.year,obj.month);
		obj.dayOffset = getIndexOfFirstSlotInMonth(obj.year,obj.month);
        if (startWeekFromSunday)
            currentTime.dayOffset++;
    }

	public void installDemoData()
	{
		addEvent(2016,3,7,  new EventObj("Event","Description"));
		addEvent(2016,3,7,  new EventObj("Event","Description"));
		addEvent(2016,3,10, new EventObj("Event","Description"));
		addEvent(2016,3,22, new EventObj("Event","Description"));
		addEvent(2016,4,5,  new EventObj("Event","Description"));
		addEvent(2016,4,5,  new EventObj("Event","Description"));
		addEvent(2016,4,5,  new EventObj("Event","Description"));
		addEvent(2016,4,15, new EventObj("Event","Description"));
		addEvent(2016,4,22, new EventObj("Event","Description"));
		addEvent(2016,5,1,  new EventObj("Event","Description"));
		addEvent(2016,5,2,  new EventObj("Event","Description"));
		addEvent(2016,5,3,  new EventObj("Event","Description"));
		addEvent(2016,5,15, new EventObj("Event","Description"));
		addEvent(2016,6,2,  new EventObj("Event","Description"));
		addEvent(2016,6,3,  new EventObj("Event","Description"));
		addEvent(2016,6,4,  new EventObj("Event","Description"));
		addEvent(2016,6,22, new EventObj("Event","Description"));


		updateCalendar(currentTime.month,currentTime.year);
		markSelectionDay(currentTime.day);
	}

	public void setUIStyle(int style)
	{
		current_UiStyle = style;
		FlatCalendarStyle2.changeUIStyle(current_UiStyle);
	}

    public void setLabelDayOfWeek()
    {
        label_dayOfWeek.GetComponent<Text>().color = FlatCalendarStyle2.color_bubbleSelectionMarker;

        // ENGLISH
        if (current_UiLanguage == 0)
        {
            add_event_labels[5].GetComponent<Text>().text = "Medicines"; //  "Today's Events";
            add_event_labels[6].GetComponent<Text>().text = "Medicines"; // "Today's Events";

            if (!startWeekFromSunday)
            {
                label_dayOfWeek.GetComponent<Text>().text = currentTime.dayOfWeek;
                daysOfWeek_events_label[0].GetComponent<Text>().text = "MON";
                daysOfWeek_events_label[1].GetComponent<Text>().text = "TUE";
                daysOfWeek_events_label[2].GetComponent<Text>().text = "WED";
                daysOfWeek_events_label[3].GetComponent<Text>().text = "THU";
                daysOfWeek_events_label[4].GetComponent<Text>().text = "FRI";
                daysOfWeek_events_label[5].GetComponent<Text>().text = "SAT";
                daysOfWeek_events_label[6].GetComponent<Text>().text = "SUN";
            }
            else
            {
                label_dayOfWeek.GetComponent<Text>().text = currentTime.dayOfWeek;
                daysOfWeek_events_label[1].GetComponent<Text>().text = "MON";
                daysOfWeek_events_label[2].GetComponent<Text>().text = "TUE";
                daysOfWeek_events_label[3].GetComponent<Text>().text = "WED";
                daysOfWeek_events_label[4].GetComponent<Text>().text = "THU";
                daysOfWeek_events_label[5].GetComponent<Text>().text = "FRI";
                daysOfWeek_events_label[6].GetComponent<Text>().text = "SAT";
                daysOfWeek_events_label[0].GetComponent<Text>().text = "SUN";
            }
        }
        // ITALIAN
        if (current_UiLanguage == 1)
        {
            add_event_labels[5].GetComponent<Text>().text = "Eventi Giornalieri";
            add_event_labels[6].GetComponent<Text>().text = "Eventi Giornalieri";

            if (!startWeekFromSunday)
            {
                daysOfWeek_events_label[0].GetComponent<Text>().text = "LUN";
                daysOfWeek_events_label[1].GetComponent<Text>().text = "MAR";
                daysOfWeek_events_label[2].GetComponent<Text>().text = "MER";
                daysOfWeek_events_label[3].GetComponent<Text>().text = "GIO";
                daysOfWeek_events_label[4].GetComponent<Text>().text = "VEN";
                daysOfWeek_events_label[5].GetComponent<Text>().text = "SAB";
                daysOfWeek_events_label[6].GetComponent<Text>().text = "DOM";
            }
            else
            {
                daysOfWeek_events_label[1].GetComponent<Text>().text = "LUN";
                daysOfWeek_events_label[2].GetComponent<Text>().text = "MAR";
                daysOfWeek_events_label[3].GetComponent<Text>().text = "MER";
                daysOfWeek_events_label[4].GetComponent<Text>().text = "GIO";
                daysOfWeek_events_label[5].GetComponent<Text>().text = "VEN";
                daysOfWeek_events_label[6].GetComponent<Text>().text = "SAB";
                daysOfWeek_events_label[0].GetComponent<Text>().text = "DOM";
            }
            switch (currentTime.dayOfWeek)
            {
                case ("Monday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Lunedì";
                    break;
                case ("Tuesday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Martedì";
                    break;
                case ("Wednesday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Mercoledì";
                    break;
                case ("Thursday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Giovedì";
                    break;
                case ("Friday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Venerdì";
                    break;
                case ("Saturday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Sabato";
                    break;
                case ("Sunday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Domenica";
                    break;
            }
        }
        // SPANISH
        if (current_UiLanguage == 2)
        {
            add_event_labels[5].GetComponent<Text>().text = "Eventos de hoy";
            add_event_labels[6].GetComponent<Text>().text = "Eventos de hoy";

            if (!startWeekFromSunday)
            {
                daysOfWeek_events_label[0].GetComponent<Text>().text = "LUN";
                daysOfWeek_events_label[1].GetComponent<Text>().text = "MAR";
                daysOfWeek_events_label[2].GetComponent<Text>().text = "MIÉ";
                daysOfWeek_events_label[3].GetComponent<Text>().text = "JUE";
                daysOfWeek_events_label[4].GetComponent<Text>().text = "VIE";
                daysOfWeek_events_label[5].GetComponent<Text>().text = "SÁB";
                daysOfWeek_events_label[6].GetComponent<Text>().text = "DOM";
            }
            else
            {
                daysOfWeek_events_label[1].GetComponent<Text>().text = "LUN";
                daysOfWeek_events_label[2].GetComponent<Text>().text = "MAR";
                daysOfWeek_events_label[3].GetComponent<Text>().text = "MIÉ";
                daysOfWeek_events_label[4].GetComponent<Text>().text = "JUE";
                daysOfWeek_events_label[5].GetComponent<Text>().text = "VIE";
                daysOfWeek_events_label[6].GetComponent<Text>().text = "SÁB";
                daysOfWeek_events_label[0].GetComponent<Text>().text = "DOM";
            }
                       
            switch (currentTime.dayOfWeek)
            {
                case ("Monday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Lunes";
                    break;
                case ("Tuesday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Martes";
                    break;
                case ("Wednesday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Miércoles";
                    break;
                case ("Thursday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Jueves";
                    break;
                case ("Friday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Viernes";
                    break;
                case ("Saturday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Sábado";
                    break;
                case ("Sunday"):
                    label_dayOfWeek.GetComponent<Text>().text = "Domingo";
                    break;
            }
        }
    }

    public void setLanguageLabelAddEvent()
    {
        if (current_UiLanguage == 0)
        {
            add_event_labels[0].GetComponent<Text>().text = "New Event";
            add_event_labels[1].GetComponent<Text>().text = "Event time";
            add_event_labels[2].GetComponent<Text>().text = "Event name";
            add_event_labels[3].GetComponent<Text>().text = "Event description";
            add_event_labels[4].GetComponent<Text>().text = "Save";
        }
        if (current_UiLanguage == 1)
        {
            add_event_labels[0].GetComponent<Text>().text = "Nuovo Evento";
            add_event_labels[1].GetComponent<Text>().text = "Orario evento";
            add_event_labels[2].GetComponent<Text>().text = "Nome evento";
            add_event_labels[3].GetComponent<Text>().text = "Descrizione evento";
            add_event_labels[4].GetComponent<Text>().text = "Salva";
        }
        if (current_UiLanguage == 2)
        {
            add_event_labels[0].GetComponent<Text>().text = "Nuevo Evento";
            add_event_labels[1].GetComponent<Text>().text = "Hora del evento";
            add_event_labels[2].GetComponent<Text>().text = "Nombre del evento";
            add_event_labels[3].GetComponent<Text>().text = "Descripción del evento";
            add_event_labels[4].GetComponent<Text>().text = "Salvar";
        }
    }

    public void addEvent(int year, int month, int day, EventObj ev)
	{
		if(!events_list.ContainsKey(year))
			events_list.Add(year,new Dictionary<int, Dictionary<int,List<EventObj>>>());
		
		if(!events_list[year].ContainsKey(month))
			events_list[year].Add(month,new Dictionary<int, List<EventObj>>());
		
		if(!events_list[year][month].ContainsKey(day))
			events_list[year][month].Add(day,new List<EventObj>());

		events_list[year][month][day].Add(ev);
        //refreshCalendar();
	}

    public void modifyEvent(int year, int month, int day, EventObj oldEvent, EventObj newEvent)
    {
        if(checkEventExist(year, month, day))
        {
            if (events_list[year][month][day].Contains(oldEvent))
            {
                removeEvent(year, month, day, oldEvent);
                addEvent(year, month, day, newEvent);
            }
        }
    }

    // To Remove selected reminder.
	public void removeEvent(int year, int month, int day, EventObj ev)
	{
		if(!events_list.ContainsKey(year))
			events_list.Add(year,new Dictionary<int, Dictionary<int,List<EventObj>>>());
		
		if(!events_list[year].ContainsKey(month))
			events_list[year].Add(month,new Dictionary<int, List<EventObj>>());
		
		if(!events_list[year][month].ContainsKey(day))
			events_list[year][month].Add(day,new List<EventObj>());

		if(events_list[year][month][day].Contains(ev))
			events_list[year][month][day].Remove(ev);

        //if (filePath != "")
        //    exportFlatCalendarEvents(filePath);

        DateTime notiTime = new DateTime(year, month, day, ev.time.Value.Hours, ev.time.Value.Minutes, ev.time.Value.Seconds);
        MyNotificationManager.Instance.CancelScheduledNotification("Medication Reminder", ev.name, notiTime);

        //populateEventVisualizer();
        //onSliderUpdateCaledar();
        //updateCalendar(currentTime.month, currentTime.day);
        
        //event_Visualizer.gameObject.SetActive(false);
    }

     

    public void removeAllEventOfDay(int year, int month, int day)
	{
		if(!events_list.ContainsKey(year))
			events_list.Add(year,new Dictionary<int, Dictionary<int,List<EventObj>>>());
		
		if(!events_list[year].ContainsKey(month))
			events_list[year].Add(month,new Dictionary<int, List<EventObj>>());
		
		if(!events_list[year][month].ContainsKey(day))
			events_list[year][month].Add(day,new List<EventObj>());

		events_list[year][month][day].Clear();
	}

	public void removeAllCalendarEvents()
	{
		events_list.Clear();
	}

	public static List<EventObj> getEventList(int year, int month, int day)
	{
		List<EventObj> list = new List<EventObj>();

		if(!events_list.ContainsKey(year))
			return list;

		if(!events_list[year].ContainsKey(month))
			return list;

		if(!events_list[year][month].ContainsKey(day))
			return list;

		return events_list[year][month][day];
	}

    private EventObj e_rb;
    public void ShowEvent(EventObj evnt)
    {
        //e_rb = evnt;
        
        if (event_Visualizer.activeInHierarchy)
            event_Visualizer.SetActive(false);
        else
        {
            event_Visualizer.SetActive(true);
            Text[] t = event_Visualizer.GetComponentsInChildren<Text>();
            t[0].text = evnt.name;
            if (evnt.time.Value.Hours < 10)
                t[1].text = "0" + evnt.time.Value.Hours.ToString();
            else
                t[1].text = evnt.time.Value.Hours.ToString();
            if (evnt.time.Value.Minutes < 10)
                    t[1].text += ":0" + evnt.time.Value.Minutes.ToString();
            else
                t[1].text += ":" + evnt.time.Value.Minutes.ToString();
            t[2].text = evnt.description;

            //DateTime StartDate = Convert.ToDateTime(evnt.startDate startDateStr, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //DateTime EndDate = Convert.ToDateTime(endDateStr, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

            t[6].text = evnt.startDate.Date.Day + " - " + getShortMonthNameFromNumber(evnt.startDate.Date.Month) + " - " + evnt.startDate.Date.Year;  //  evnt.startDate.Date.ToLongDateString(); 
            t[7].text = evnt.endDate.Date.Day + " - " + getShortMonthNameFromNumber(evnt.endDate.Date.Month) + " - " + evnt.endDate.Date.Year;   // evnt.endDate.Date.ToLongDateString();  
             //event_Visualizer.GetComponentsInChildren<Button>()[1].onClick.AddListener(() => customDerivedCalendar.removeAllAssociatedEvent(currentTime.year, currentTime.month, currentTime.day, evnt));
            //event_Visualizer.GetComponentsInChildren<Button>()[1].onClick.AddListener(() => customDerivedCalendar.removeEvent(currentTime.year, currentTime.month, currentTime.day, evnt));
            //event_Visualizer.GetComponentsInChildren<Button>()[2].onClick.AddListener(() => customDerivedCalendar.evtListener_ModifyEvent(evnt));
            event_Visualizer.GetComponentsInChildren<Button>()[1].onClick.RemoveAllListeners();
            event_Visualizer.GetComponentsInChildren<Button>()[2].onClick.RemoveAllListeners();
            event_Visualizer.GetComponentsInChildren<Button>()[1].onClick.AddListener(() => DeleteEventClicked(currentTime.year, currentTime.month, currentTime.day, evnt));
            event_Visualizer.GetComponentsInChildren<Button>()[2].onClick.AddListener(() => EditEvent_ClickedOK(evnt));

        }
    }

    // ================================================
    // =============== BUTTON LISTENERS ===============
    // ================================================

    private void DeleteEventClicked(int year, int month, int day, EventObj evnt)
    {
        DeleteReminderPanel.SetActive(true);
        event_Visualizer.SetActive(true);
        DeleteReminderPanel.GetComponentsInChildren<Button>()[1].onClick.RemoveAllListeners();
        DeleteReminderPanel.GetComponentsInChildren<Button>()[1].onClick.AddListener(() => DeleteEvent_ClickedOK(currentTime.year, currentTime.month, currentTime.day, evnt));

    }

    public async void DeleteEvent_ClickedOK(int year, int month, int day, EventObj evnt)
    {
        DeleteReminderPanel.SetActive(false);

        if (DeleteReminderPanel.GetComponentsInChildren<Toggle>()[0].isOn) 
        {
             await Task.Run(() => removeEvent(currentTime.year, currentTime.month, currentTime.day, evnt));
        }
        else if (DeleteReminderPanel.GetComponentsInChildren<Toggle>()[1].isOn) 
        {
            //customDerivedCalendar.removeAllAssociatedEvent(currentTime.year, currentTime.month, currentTime.day, evnt);
            await Task.Run(()=> customDerivedCalendar.RunRemoveEventTaskAsync(year, month, day, evnt));
        }
        await Task.Delay(0).ConfigureAwait(true);
        //Debug.Log("Last");
        //Invoke("UpdateCalForcefully", 0.25f);
 
        //populateEventVisualizer();
        customDerivedCalendar.onSliderUpdateCaledar();
        onSliderUpdateCaledar();
        updateCalendar(currentTime.month, currentTime.day);
        event_Visualizer.gameObject.SetActive(false);

        if (filePath != "")
            exportFlatCalendarEvents(filePath);

    }

    private void UpdateCalForcefully()
    {
        onSliderUpdateCaledar();
    }
 
    public void EditEvent_ClickedOK(EventObj e)
    {
        customDerivedCalendar.DateTimeFromMainCalendar(currentTime.day, currentTime.month);
        customDerivedCalendar._CacheOldEventBeforeApplyingEditOnEntireSeries(e);

        customDerivedCalendar.evtListener_ModifyEvent(e);
        //customDerivedCalendar.removeAllAssociatedEvent(currentTime.year, currentTime.month, currentTime.day, e_rb);
    }

    virtual public void evtListener_SwitchModality()
    {
        if (slider.activeSelf)
        {
            slider.SetActive(false);
            grid.SetActive(true);
            updateCalendar(currentTime.month, currentTime.year);

            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);

            if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
            {
                day_slot.GetComponentsInChildren<Image>()[2].enabled = true;
            }
            else
                day_slot.GetComponentsInChildren<Image>()[2].enabled = false;

            //eventsScrollView.transform.position = new Vector3(eventsScrollView.transform.position.x, eventsScrollView.transform.position.y - 80, eventsScrollView.transform.position.z);
            eventsScrollView.GetComponent<RectTransform>().offsetMax = new Vector2(eventsScrollView.GetComponent<RectTransform>().offsetMax.x, 0);
        }
        else
        {
            slider.SetActive(true);
            grid.SetActive(false);
            
            //eventsScrollView.transform.position = new Vector3(eventsScrollView.transform.position.x, eventsScrollView.transform.position.y + 80, eventsScrollView.transform.position.z);
            eventsScrollView.GetComponent<RectTransform>().offsetMax = new Vector2(eventsScrollView.GetComponent<RectTransform>().offsetMax.x, 150);
            onSliderUpdateCaledar();
        }
    }

	void evtListener_NextMonth()
	{
		unmarkSelctionDay(currentTime.day);

        if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
        {
            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);
            day_slot.GetComponentsInChildren<Image>()[2].enabled = false;
        }

        currentTime.month = (currentTime.month+1) % 13;
		if(currentTime.month == 0)
		{
			currentTime.year++;
			currentTime.month = 1;
		}        

        currentTime.day       = 1;
        currentTime.dayOfWeek = getDayOfWeek(currentTime.year,currentTime.month,currentTime.day);
		currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year,currentTime.month);
        currentTime.totalDays = System.DateTime.DaysInMonth(currentTime.year, currentTime.month);
        if (startWeekFromSunday)
            currentTime.dayOffset++;        

        updateCalendar(currentTime.month,currentTime.year);

		markSelectionDay(currentTime.day);

        if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
        {
            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);
            day_slot.GetComponentsInChildren<Image>()[2].enabled = true;
        }

        // Send Callback
        if (delegate_onmonthchanged != null)
			delegate_onmonthchanged(currentTime);
	}

	void evtListener_PreviousMonth()
	{
		unmarkSelctionDay(currentTime.day);

        if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
        {
            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);
            day_slot.GetComponentsInChildren<Image>()[2].enabled = false;
        }

        currentTime.month = (currentTime.month-1) % 13;
		if(currentTime.month == 0)
		{
			currentTime.year--;
			currentTime.month = 12;
		}

		currentTime.day   = 1;
		currentTime.dayOfWeek = getDayOfWeek(currentTime.year,currentTime.month,currentTime.day);
		currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year,currentTime.month);
        currentTime.totalDays = System.DateTime.DaysInMonth(currentTime.year, currentTime.month);
        if (startWeekFromSunday)
            currentTime.dayOffset++;

        updateCalendar(currentTime.month,currentTime.year);

		markSelectionDay(currentTime.day);

        if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
        {
            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);
            day_slot.GetComponentsInChildren<Image>()[2].enabled = true;
        }

        // Send Callback
        if (delegate_onmonthchanged != null)
			delegate_onmonthchanged(currentTime);

	}

    void evtListener_NextYear()
    {
        unmarkSelctionDay(currentTime.day);

        if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
        {
            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);
            day_slot.GetComponentsInChildren<Image>()[2].enabled = false;
        }

        currentTime.year += 1;


        currentTime.day = 1;
        currentTime.dayOfWeek = getDayOfWeek(currentTime.year, currentTime.month, currentTime.day);
        currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);
        currentTime.totalDays = System.DateTime.DaysInMonth(currentTime.year, currentTime.month);

        if (startWeekFromSunday)
            currentTime.dayOffset++;

        updateCalendar(currentTime.month, currentTime.year);

        markSelectionDay(currentTime.day);

        if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
        {
            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);
            day_slot.GetComponentsInChildren<Image>()[2].enabled = true;
        }

        // Send Callback
        if (delegate_onmonthchanged != null)
            delegate_onmonthchanged(currentTime);
    }

    void evtListener_PreviousYear()
    {
        unmarkSelctionDay(currentTime.day);

        if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
        {
            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);
            day_slot.GetComponentsInChildren<Image>()[2].enabled = false;
        }

        currentTime.year -= 1;


        currentTime.day = 1;
        currentTime.dayOfWeek = getDayOfWeek(currentTime.year, currentTime.month, currentTime.day);
        currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);
        currentTime.totalDays = System.DateTime.DaysInMonth(currentTime.year, currentTime.month);

        if (startWeekFromSunday)
            currentTime.dayOffset++;

        updateCalendar(currentTime.month, currentTime.year);

        markSelectionDay(currentTime.day);

        if (DateTime.Now.Month == currentTime.month && DateTime.Now.Year == currentTime.year)
        {
            GameObject day_slot = GameObject.Find("Slot_" + currentDaySlot);
            day_slot.GetComponentsInChildren<Image>()[2].enabled = true;
        }

        // Send Callback
        if (delegate_onmonthchanged != null)
            delegate_onmonthchanged(currentTime);
    }

    public void evtListener_DaySelected()
	{
        // Unmark old slot
		unmarkSelctionDay(currentTime.day);

		// Update current day
		string slot_name             = EventSystem.current.currentSelectedGameObject.name;
		int    slot_position         = int.Parse(slot_name.Substring(5,(slot_name.Length-5)));
		 	   currentTime.day       = getDayInSlot(slot_position);
			   currentTime.dayOfWeek = getDayOfWeek(currentTime.year,currentTime.month,currentTime.day);

		// Mark current slot
		markSelectionDay(currentTime.day);

		// Send Callback
		if(delegate_ondayselected != null)
			delegate_ondayselected(currentTime);

		// Send Callback
		if(getEventList(currentTime.year,currentTime.month,currentTime.day).Count > 0)
			if(delegate_oneventselected != null)
				delegate_oneventselected(currentTime,getEventList(currentTime.year,currentTime.month,currentTime.day));

        populateEventVisualizer();
    }
    virtual public void onSliderDaySelect(GameObject obj)
    {
        foreach (GameObject o in sliderDaysButtons)
        {
            o.transform.GetChild(4).GetComponent<Image>().enabled = false;
        }
        Text[] day_number = obj.GetComponentsInChildren<Text>();
        currentTime.day = Int32.Parse(day_number[0].text);
        ReminderDateErrorMessage.SetActive(false);

        onSliderUpdateCaledar();
    }

    virtual public void onSliderMonthSelect(GameObject obj)
    {
  
        foreach (GameObject o in sliderDaysButtons)
        {
            o.transform.GetChild(4).GetComponent<Image>().enabled = false;  
        }
        for (int i = 0; i < 12; i++)
        {
            if (sliderMonthsButtons[i] == obj)
            {
                currentTime.month = i + 1;
                if(currentTime.day == 31 || currentTime.day == 30)
                {
                    if(currentTime.month == 2)
                    {
                        currentTime.day = 29;
                    }
                    if(currentTime.day == 31)
                    {
                        if(currentTime.month == 4 || currentTime.month == 6 || currentTime.month == 9 ||
                            currentTime.month == 11)
                        {
                            currentTime.day = 30;
                        }
                    }
                }
                //currentTime.day = 1;
                currentTime.totalDays = DateTime.DaysInMonth(currentTime.year, currentTime.month);
                currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);
            }
        }
        onSliderUpdateCaledar();
    }
    virtual public void onSliderYearSelect(GameObject obj)
    {
        foreach (GameObject o in sliderDaysButtons)
        {
            o.GetComponentInChildren<Image>().enabled = false;
        }
        for (int i = 0; i < sliderYearsButtons.Length; i++)
        {
            if (sliderYearsButtons[i] == obj)
            {
                currentTime.year = Int32.Parse(sliderYearsButtons[i].GetComponentInChildren<Text>().text);
                currentTime.month = 1;
                currentTime.day = 1;
                currentTime.totalDays = DateTime.DaysInMonth(currentTime.year, currentTime.month);
                currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);
            }
        }
        onSliderUpdateCaledar();
    }

    void evtListener_GoToNowday()
	{
		// Unmark old slot
		unmarkSelctionDay(currentTime.day);

		// Set Current Time
		setCurrentTime();

		// Update Calendar
		updateCalendar(currentTime.month,currentTime.year);

		// Mark Selection Day
		markSelectionDay(currentTime.day);

		// Send Callback
		if(delegate_onnowday != null)
			delegate_onnowday(currentTime);
	}

    void evtListenet_VisualizerBackButton()
    {
        event_Visualizer.SetActive(false);
    }

    public void BackBtnFromAddEventScreen()
    {
        addEvent_Visualizer.gameObject.SetActive(false);
        customDerivedCalendar.editExistingEvent = false;
        reminder_FooterPanel.SetActive(true);
        setCurrentTime();
        onSliderUpdateCaledar();
    }

    public void evtListener_ShowAddEvent()
    {
        DateTime todaySystemDate;
        DateTime userSelectedDate;
 
        if (addEvent_Visualizer.gameObject.activeInHierarchy)
        {
            //Debug.Log("a1");
            field_timeError.GetComponent<Text>().text = "";
            addEvent_Visualizer.gameObject.SetActive(false);
        }
        else
        {
            //Debug.Log("a2");
            todaySystemDate = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, 0, 0, 0);
            userSelectedDate = new DateTime(currentTime.year, currentTime.month, currentTime.day, 0, 0, 0);
            int result = DateTime.Compare(userSelectedDate, todaySystemDate);
            if (result < 0)
            {
                ReminderDateErrorMessage.SetActive(true);
                return;
            }
            ReminderDateErrorMessage.SetActive(false);
            addEvent_Visualizer.gameObject.SetActive(true);
            label_Date.GetComponent<Text>().text = currentTime.day + "/" + currentTime.month + "/" + currentTime.year;
            label_Date.GetComponentInParent<Image>().color = FlatCalendarStyle2.color_topGrid;
            field_Name.GetComponent<InputField>().text = ""; 
            field_Description.GetComponent<InputField>().text = "";
            setLanguageLabelAddEvent();
            field_Name.GetComponent<InputField>().placeholder.GetComponent<Text>().text = "";
            field_Description.GetComponent<InputField>().placeholder.GetComponent<Text>().text = "";

            customDerivedCalendar.OnEnable();
            customDerivedCalendar.DateTimeFromMainCalendar(currentTime.day, currentTime.month);
            customDerivedCalendar.StartDatePicker.transform.GetChild(0).GetComponent<Text>().text = currentTime.day + "-" + getShortMonthNameFromNumber(currentTime.month) + "-" + currentTime.year;
            customDerivedCalendar.startDateStr = currentTime.day + "-" + getShortMonthNameFromNumber(currentTime.month) + "-" + currentTime.year;
            reminder_FooterPanel.SetActive(false);
            //customDerivedCalendar.EndDatePicker.transform.GetChild(0).GetComponent<Text>().text = currentTime.day + "-" + getShortMonthNameFromNumber(currentTime.month) + "-" + currentTime.year;
            //customDerivedCalendar.endDateStr = currentTime.day + "-" + getShortMonthNameFromNumber(currentTime.month) + "-" + currentTime.year;
        }

        currentAlarmValue = true;
        btn_AddEvent[3].GetComponentsInChildren<Image>()[1].color = FlatCalendarStyle2.color_bubbleEvent;
        btn_AddEvent[3].GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_bubbleEvent;
        btn_AddEvent[3].GetComponentInChildren<Text>().text = "Alarm On";
        selected_hours = -1;
        selected_minutes = -1;
        foreach (Text t in field_TimeSlider[0].GetComponentsInChildren<Text>())
        {
            t.color = new Color32(156, 156, 156, 255);
            t.resizeTextMaxSize = 35;
        }
        foreach (Text t in field_TimeSlider[1].GetComponentsInChildren<Text>())
        {
            t.color = new Color32(156, 156, 156, 255);
            t.resizeTextMaxSize = 35;

        }
    }

    virtual public void evtListener_ModifyEvent(EventObj e)
    {
        foreach (Text t in field_TimeSlider[0].GetComponentsInChildren<Text>())
        {
            t.color = new Color32(156, 156, 156, 255);
        }
        foreach (Text t in field_TimeSlider[1].GetComponentsInChildren<Text>())
        {
            t.color = new Color32(156, 156, 156, 255);
        }
        addEvent_Visualizer.gameObject.SetActive(true);
        label_Date.GetComponent<Text>().text = currentTime.day + "/" + currentTime.month + "/" + currentTime.year;
        label_Date.GetComponentInParent<Image>().color = FlatCalendarStyle2.color_topGrid;
        setLanguageLabelAddEvent();
        event_Visualizer.gameObject.SetActive(false);
        field_Name.GetComponent<InputField>().text = e.name;
        add_event_labels[0].GetComponent<Text>().text = e.name;
        field_Description.GetComponent<InputField>().text = e.description;
        selected_hours = e.time.Value.Hours;
        selected_minutes = e.time.Value.Minutes;
        field_TimeSlider[0].GetComponentsInChildren<Text>()[selected_hours].color = FlatCalendarStyle2.color_bubbleSelectionMarker;
        field_TimeSlider[1].GetComponentsInChildren<Text>()[selected_minutes/5].color = FlatCalendarStyle2.color_bubbleSelectionMarker;
        field_Name.GetComponent<InputField>().placeholder.GetComponent<Text>().text = "";
        field_Description.GetComponent<InputField>().placeholder.GetComponent<Text>().text = "";
        modifingEvent = e;
        if (e.alarm != currentAlarmValue)
        {
            evtListener_onAlarm_Enable_Disable();
            //currentAlarmValue = true;
            //btn_AddEvent[3].GetComponentsInChildren<Image>()[1].color = FlatCalendarStyle2.color_bubbleEvent;
            //btn_AddEvent[3].GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_bubbleEvent;
            //btn_AddEvent[3].GetComponentInChildren<Text>().text = "Alarm On";
        }
    }

    public void evtListener_onAlarm_Enable_Disable()
    {
        if (currentAlarmValue)
        {
            currentAlarmValue = false;
            btn_AddEvent[3].GetComponentsInChildren<Image>()[1].color = new Color32(130, 130, 130, 255);
            btn_AddEvent[3].GetComponentInChildren<Text>().color = new Color32(130, 130, 130, 255);
            btn_AddEvent[3].GetComponentInChildren<Text>().text = "Alarm Off";

        }
        else
        {
            currentAlarmValue = true;
            btn_AddEvent[3].GetComponentsInChildren<Image>()[1].color = FlatCalendarStyle2.color_bubbleEvent;
            btn_AddEvent[3].GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_bubbleEvent;
            btn_AddEvent[3].GetComponentInChildren<Text>().text = "Alarm On";

        }
    }
     

     virtual public void evtListener_AddNewEvent()
     {
        TimeSpan new_time = new TimeSpan(selected_hours, selected_minutes, 00);
        string name = field_Name.GetComponent<InputField>().text;
        string description = field_Description.GetComponent<InputField>().text;
        if (name == "")
        {
            string missing_name = "";
            if (current_UiLanguage == 0)
                missing_name = "Event name is required!";
            if (current_UiLanguage == 1)
                missing_name = "Nome evento necessario!";
            if (current_UiLanguage == 2)
                missing_name = "Nombre del evento es obligatorio!";

            field_Name.GetComponent<InputField>().placeholder.GetComponent<Text>().text = missing_name;
            //Debug.Log("Name is Required");
            return;
        }
        if (description == "")
        {
            string missing_name = "";
            if (current_UiLanguage == 0)
                missing_name = "Event description is required!";
            if (current_UiLanguage == 1)
                missing_name = "Descrizione evento necessaria!";
            if (current_UiLanguage == 2)
                missing_name = "Se requiere descripción del evento!";

            field_Description.GetComponent<InputField>().placeholder.GetComponent<Text>().text = missing_name;
            //Debug.Log("Description is Required");
            return;
        }
        if(selected_hours == -1 || selected_minutes == -1)
        {
            string missing_time = "";
            if (current_UiLanguage == 0)
                missing_time = "*Event time is required!";
            if (current_UiLanguage == 1)
                missing_time = "*Orario evento necessario!";
            if (current_UiLanguage == 2)
                missing_time = "*Se requiere tiempo de evento!";
            //Debug.Log("Time is Required");
            field_timeError.GetComponent<Text>().text = missing_time;
            return;
        }
        EventObj evnt = new EventObj(name, description, new_time);
        evnt.alarm = currentAlarmValue;
        var existingEvent = getEventList(currentTime.year, currentTime.month, currentTime.day).Where(item => item.name == evnt.name).ToList();

        //event already exists
        if(modifingEvent != null)
        {
            modifyEvent(currentTime.year, currentTime.month, currentTime.day, modifingEvent, evnt);
        }
        else
            addEvent(currentTime.year, currentTime.month, currentTime.day, evnt);

        modifingEvent = null;
        addEvent_Visualizer.gameObject.SetActive(false);
        //event_Visualizer.gameObject.SetActive(false);
        markSelectionDay(currentTime.day);
        sliderDaysButtons[currentTime.day - 1].transform.GetChild(4).GetComponent<Image>().enabled = true; //  GetComponentInChildren<Image>().enabled = true;
        if (filePath != "")
            exportFlatCalendarEvents(filePath);
        updateCalendar(currentTime.month, currentTime.day);
        foreach (Text t in field_TimeSlider[0].GetComponentsInChildren<Text>())
        {
            t.color = new Color32(156, 156, 156, 255);
        }
        foreach (Text t in field_TimeSlider[1].GetComponentsInChildren<Text>())
        {
            t.color = new Color32(156, 156, 156, 255);
        }
        field_timeError.GetComponent<Text>().text = "";
        if (!currentAlarmValue)
        {
            currentAlarmValue = true;
            btn_AddEvent[3].GetComponentsInChildren<Image>()[1].color = FlatCalendarStyle2.color_bubbleEvent;
            btn_AddEvent[3].GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_bubbleEvent;
            btn_AddEvent[3].GetComponentInChildren<Text>().text = "Alarm On";
        }
    }
    public void onSliderHoursSelect(GameObject obj)
    {
        foreach (Text t in field_TimeSlider[0].GetComponentsInChildren<Text>())
        {
            t.color = new Color32(156, 156, 156, 255);
            t.resizeTextMaxSize = 35;
        }
        obj.GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_bubbleSelectionMarker;
        obj.GetComponentInChildren<Text>().resizeTextMaxSize = obj.GetComponentInChildren<Text>().resizeTextMaxSize + 10;

        selected_hours = Int32.Parse(obj.GetComponentInChildren<Text>().text);
    }
    public void onSliderMinutesSelect(GameObject obj)
    {
        foreach (Text t in field_TimeSlider[1].GetComponentsInChildren<Text>())
        {
            t.color = new Color32(156, 156, 156, 255);
            t.resizeTextMaxSize = 35;
        }
        obj.GetComponentInChildren<Text>().color = FlatCalendarStyle2.color_bubbleSelectionMarker;
        obj.GetComponentInChildren<Text>().resizeTextMaxSize = obj.GetComponentInChildren<Text>().resizeTextMaxSize+10;

        selected_minutes = Int32.Parse(obj.GetComponentInChildren<Text>().text);
    }

    void evtListener_UpdateEventsFile()
    {
        if (filePath != "")
            exportFlatCalendarEvents(filePath);
    }

    // =========================================================
    // ================= SET DELEGATE CALLBACKS ================
    // =========================================================

    public void setCallback_OnDaySelected(Delegate_OnDaySelected func)
	{
		delegate_ondayselected = func;
	}

	public void setCallback_OnEventSelected(Delegate_OnEventSelected func)
	{
		delegate_oneventselected = func;
	}

	public void setCallback_OnMonthChanged(Delegate_OnMonthChanged func)
	{
		delegate_onmonthchanged = func;
	}

	public void setCallback_OnNowday(Delegate_OnNowDay func)
	{
		delegate_onnowday = func;
	}

    public void setCallback_OnTriggerEvent(Delegate_OnTriggerEvent func)
    {
        delegate_ontriggerevent = func;
    }

}
