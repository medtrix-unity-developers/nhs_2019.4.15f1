﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using static FlatCalendar2.EventExportList;
using UnityEngine.UI.Extensions;
using UnityEngine.Networking;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.CompilerServices;

//TODO Calendar related functionality.
public class CustomDerivedCalendar : FlatCalendar2
{
    //public GameObject panel_Waiting;
    [SerializeField] private SendReminderData sendReminderData;

    [Header("My Custom Variables")]
    [SerializeField] private GameObject PopupErrorTimeMultiple;
    [SerializeField] private Dropdown MedicineTypeDropDown;
    [SerializeField] private Dropdown MedicineNameDropDown;
    [SerializeField] private Dropdown MedicineDaysTypeDropDown;
    [SerializeField] private Dropdown MedicineTimingsDropDown;
    [SerializeField] private GameObject MedicineDetailsContainer;
    [SerializeField] private GameObject MultivitaminContainer;
    [SerializeField] private GameObject LoadingCircle;


    //Error GameObjects:
    [SerializeField] private GameObject MessageTypeError;
    [SerializeField] private GameObject CustomMedicineNameError;

    [SerializeField] private GameObject CustomMedicineContainer;
    [SerializeField] private GameObject CustomMedicineDays;

    [SerializeField] private InputField CustomMedicineTxt;
    [SerializeField] private GameObject[] TimePickers;
    [SerializeField] private GameObject DateSlide;
    [SerializeField] private GameObject TimeSlide;
    [SerializeField] public GameObject StartDatePicker;
    [SerializeField] public GameObject EndDatePicker;
    [SerializeField] private List<string> customMedicineDaysArray = new List<string>();
    [SerializeField] private List<Tuple<int, int, string>> medicineTimingsPerDayArray = new List<Tuple<int, int, string>>();
    [SerializeField] private GameObject[] CustomDaysGameObjects;

    [Header("CustomDerivedClass Variables.")]
    // Custom variables ...
    [SerializeField] private GameObject[] customSliderDaysButtons;
    [SerializeField] private GameObject[] customSliderMonthsButtons;
    [SerializeField] private GameObject[] customSliderYearsButtons;
    [SerializeField] private ScrollRect[] customScrollRects;

    [SerializeField] private ScrollRect addReminderScrollRect;
    [SerializeField] private Text dateError;

    private bool startDatePicked;
    private bool endDatePicked;
    private string pickedTimePickerName;
    [HideInInspector]
    public bool editExistingEvent;

    [Header("Server data.")]
    // Data to be stored im server.
    private string daySelectedStr;
    public string startDateStr;
    public string endDateStr;
    private string medicationTypeStr;
    private string medicineNameStr;

    [HideInInspector]
    public bool editThisEvent, editAllEvents;

    //Counter...
    private int minutesCounter = 0;
    private int minutesAndroidCounter = 0;


    public void OnEnable()
    {
        //setCurrentTime();
        //Debug.Log("HelloWorld " + currentTime.day);
        OnResetAddReminderPage();
        addReminderScrollRect.verticalNormalizedPosition = 2f;
        customMedicineDaysArray.Clear();
        medicineTimingsPerDayArray.Clear();
        daySelectedStr = "";

        string monthShortName = getShortMonthNameFromNumber(currentTime.month);
        startDateStr = currentTime.day + "-" + monthShortName + "-" + currentTime.year;
        endDateStr = ""; // currentTime.day + "-" + monthShortName + "-" + currentTime.year;

        medicationTypeStr = "";
        medicineNameStr = "";
        medicineNameStr = MedicineNameDropDown.options[MedicineNameDropDown.value].text;

        medicineTimingsPerDayArray.Add(new Tuple<int, int, string>(System.DateTime.Now.Hour, System.DateTime.Now.Minute, DatePickerControl.fecha.ToLongTimeString()));    // ToShortTimeString()
        TimePickers[0].transform.GetChild(0).GetComponent<Text>().text = String.Format("{0:00}:{1:00}", System.DateTime.Now.Hour, System.DateTime.Now.Minute);

        for (int i = 0; i < CustomDaysGameObjects.Length; i++)
        {

            CustomDaysGameObjects[i].transform.GetChild(0).GetComponent<Image>().enabled = false;
            CustomDaysGameObjects[i].transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(50, 50, 50, 255);
            customMedicineDaysArray.Remove(CustomDaysGameObjects[i].name);

        }

        LoadingCircle.SetActive(false);
        if (filePath == "")
            filePath = Application.persistentDataPath + "/events.json";
    }

    //Medicine Type changed from deop down from the user.
    public void MedicineTypeDropDownChanged(Dropdown medicationDropDown)
    {
        //Debug.Log("MEDICATION TYPE MedicineTypeDropDownChanged :" + medicationDropDown.options[medicationDropDown.value].text);
        string choosedMedicineType = medicationDropDown.options[medicationDropDown.value].text;
        MessageTypeError.SetActive(false);
        if (choosedMedicineType == "Custom")
        {
            MedicineDetailsContainer.gameObject.SetActive(false);
            MultivitaminContainer.gameObject.SetActive(false);
            CustomMedicineContainer.gameObject.SetActive(true);
            medicationTypeStr = choosedMedicineType;

            //Adding default medicine when user changes the medicine drop down. Need to test this.
            medicineNameStr = CustomMedicineTxt.GetComponent<InputField>().text;
        }
        else if (choosedMedicineType == "Phosphate Binder")
        {
            MedicineDetailsContainer.gameObject.SetActive(true);
            CustomMedicineContainer.gameObject.SetActive(false);
            MultivitaminContainer.gameObject.SetActive(false);
            CustomMedicineNameError.SetActive(false);
            medicationTypeStr = choosedMedicineType;

            //Adding default medicine when user changes the medicine drop down. Need to test this.
            medicineNameStr = "Renacet (Calcium Acetate)";
        }
        else if (choosedMedicineType == "Multivitamin")
        {
            MultivitaminContainer.gameObject.SetActive(true);
            CustomMedicineContainer.gameObject.SetActive(false);
            MedicineDetailsContainer.gameObject.SetActive(false);
            CustomMedicineNameError.SetActive(false);
            medicationTypeStr = choosedMedicineType;

            if (MultivitaminContainer.transform.GetChild(1).GetComponent<Dropdown>().value == 0)
            {
                medicineNameStr = "Renavit";
            }
            else if (MultivitaminContainer.transform.GetChild(1).GetComponent<Dropdown>().value == 1)
            {
                medicineNameStr = "Ketovite";
            }
        }
        else
        {
            MedicineDetailsContainer.gameObject.SetActive(false);
            MultivitaminContainer.gameObject.SetActive(false);
            CustomMedicineContainer.gameObject.SetActive(false);
            medicationTypeStr = "";
        }
    }

    //Medicine Name changed from deop down from the user after type selected.
    public void MedicineNameDropDownChanged(Dropdown medicineDropDown)
    {
        string choosedMedicineName = medicineDropDown.options[medicineDropDown.value].text;
        medicineNameStr = choosedMedicineName;

        switch (choosedMedicineName)
        {
            case "Renacet (Calcium Acetate)":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Swallow with liquid, during or immediately after a meal and do not chew.";
                break;
            case "Phosex (Calcium Acetate)":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Take with meals. Swallow whole with liquid. Can be broken, but don't chew. ";
                break;
            case "Osvaren (Calcium Acetate + Mg)":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Take with meals. Do not crush or chew.";
                break;
            case "Calcichew (Calcium Carbonate)":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "For phosphate binding, chew or suck before, during or just after a meal.";
                break;
            case "Renvela (Sevelamer Carbonate)":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Swallow whole with food and not on an empty stomach. Do not crush, chew or break.";
                break;

            case "Sevelamer Carbonate":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Swallow whole with food and not on an empty stomach. Do not crush, chew or break.";
                break;

            case "Renagel (Sevelamer Chloride)":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Swallow whole with meals. Do not crush, chew or break.";
                break;

            case "Fosrenol (Lanthanum Carbonate)":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Chew during or immediately after meals. Do not swallow whole. Can be crushed first to aid chewing.";
                break;
            case "Velphoro (Sucroferric Oxyhydroxide)":
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Chew during meals. Do not swallow whole. Can be crushed first to aid chewing.";
                break;
            case "Renavit":
                MultivitaminContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Take with a little water after dialysis. Swallow whole. Do not chew.";
                break;
            case "Ketovite":
                MultivitaminContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Take 1 tablet three times a day after dialysis. Keep the tablets in the fridge.";
                break;

        }

    }

    //Medicine days type changed from drop down from the user after mediine selected.
    public void MedicineDaysTypeDropDownChanged(Dropdown daysDropDown)
    {
        //Debug.Log("MedicineDaysTypeDropDownChanged :" + daysDropDown.options[daysDropDown.value].text);
        string daysType = daysDropDown.options[daysDropDown.value].text;

        if (daysType == "Custom" || daysType == "Dialysis days")
        {
            CustomMedicineDays.SetActive(true);
        }
        else
        {
            CustomMedicineDays.SetActive(false);
        }
    }

    //Selecting days from the list for custom days selection.
    public void OnCustomDaysSelection(GameObject customDaySelected)
    {
        //bool isDaysStored = customMedicineDaysArray.Contains(customDaySelected);
        string daySelectedName = customDaySelected.name;
        bool isDaysStoredAlready = customMedicineDaysArray.Contains(daySelectedName);
        if (customMedicineDaysArray.Contains(daySelectedName))
        {
            customDaySelected.transform.GetChild(0).GetComponent<Image>().enabled = false;
            customDaySelected.transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(50, 50, 50, 255);
            customMedicineDaysArray.Remove(daySelectedName);
        }
        else
        {
            customDaySelected.transform.GetChild(0).GetComponent<Image>().enabled = true;
            customDaySelected.transform.GetChild(1).gameObject.GetComponent<Text>().color = Color.white;
            customMedicineDaysArray.Add(daySelectedName);
        }
    }


    //Setting medicine timing per day by the user.
    public void MedicineTimingsDropDownChanged(Dropdown medicineTimeDropDown)
    {
        //Debug.Log("MEDICATION TYPE :" + medicineTimeDropDown.options[medicineTimeDropDown.value].text);
        string medicineTimings = medicineTimeDropDown.options[medicineTimeDropDown.value].text;
        if (medicineTimings == "Once a day")
        {
            TimePickers[0].SetActive(true);
            TimePickers[1].SetActive(false);
            TimePickers[2].SetActive(false);
            TimePickers[3].SetActive(false);
            TimePickers[4].SetActive(false);
        }
        else if (medicineTimings == "Twice a day")
        {
            TimePickers[0].SetActive(true);
            TimePickers[1].SetActive(true);
            TimePickers[2].SetActive(false);
            TimePickers[3].SetActive(false);
            TimePickers[4].SetActive(false);
        }
        else if (medicineTimings == "Three times a day")
        {
            TimePickers[0].SetActive(true);
            TimePickers[1].SetActive(true);
            TimePickers[2].SetActive(true);
            TimePickers[3].SetActive(false);
            TimePickers[4].SetActive(false);
        }
        else if (medicineTimings == "Four times a day")
        {
            TimePickers[0].SetActive(true);
            TimePickers[1].SetActive(true);
            TimePickers[2].SetActive(true);
            TimePickers[3].SetActive(true);
            TimePickers[4].SetActive(false);
        }
        else if (medicineTimings == "Five times a day")
        {
            TimePickers[0].SetActive(true);
            TimePickers[1].SetActive(true);
            TimePickers[2].SetActive(true);
            TimePickers[3].SetActive(true);
            TimePickers[4].SetActive(true);
        }
    }

    //Picking up the start date and end date from calendar.
    public void OnShowCalendar(string dateOptionsType)
    {
        //Debug.Log(":::CustomOnShowCalendar1::: " + currentTime.day + " " + currentTime.month);

        if (dateOptionsType == "StartDate")
        {
            //Debug.Log("startDateStr " + startDateStr);
            DateSlide.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = "Start Date";
            DateSlide.transform.GetChild(1).GetChild(0).GetComponent<Text>().fontStyle = FontStyle.Bold;
            startDatePicked = true;
            endDatePicked = false;
            if (editExistingEvent)
                UpdateDateAndMonth_MiniCalendar("StartDate");
        }
        else if (dateOptionsType == "EndDate")
        {
            //Debug.Log("endDateStr " + endDateStr);
            DateSlide.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = "End Date";
            DateSlide.transform.GetChild(1).GetChild(0).GetComponent<Text>().fontStyle = FontStyle.Bold;
            startDatePicked = false;
            endDatePicked = true;
            if (editExistingEvent)
                UpdateDateAndMonth_MiniCalendar("EndDate");
        }
        //startDateStr = currentTime.day + "-" + currentTime.month + "-" + currentTime.year;

        //Debug.Log(":::CustomOnShowCalendar2::: " + base.currentTime.day + " " + base.currentTime.month);
        onSliderUpdateCaledar();
        SnapToTargetMonthAndDate_MiniCalendar();
    }

    private string[] arr;
    private void UpdateDateAndMonth_MiniCalendar(string dateOptionsType)
    {
        int _date = 1, _month = 1;
        if (dateOptionsType == "StartDate")
        {
            arr = startDateStr.Split('-');
            _date = int.Parse(arr[0]);

            if (arr[1].Contains("1") || arr[1].Contains("2") ||
                arr[1].Contains("3") || arr[1].Contains("4") ||
                arr[1].Contains("5") || arr[1].Contains("6") ||
                arr[1].Contains("7") || arr[1].Contains("8") ||
                arr[1].Contains("9") || arr[1].Contains("0"))
            {
                _month = int.Parse(arr[1]);
            }

            if (arr[1].Contains("Jan")) _month = 1;
            if (arr[1].Contains("Feb")) _month = 2;
            if (arr[1].Contains("Mar")) _month = 3;
            if (arr[1].Contains("Apr")) _month = 4;
            if (arr[1].Contains("May")) _month = 5;
            if (arr[1].Contains("Jun")) _month = 6;
            if (arr[1].Contains("Jul")) _month = 7;
            if (arr[1].Contains("Aug")) _month = 8;
            if (arr[1].Contains("Sep")) _month = 9;
            if (arr[1].Contains("Oct")) _month = 10;
            if (arr[1].Contains("Nov")) _month = 11;
            if (arr[1].Contains("Dec")) _month = 12;
        }
        else if (dateOptionsType == "EndDate")
        {
            arr = endDateStr.Split('-');
            _date = int.Parse(arr[0]);

            if (arr[1].Contains("1") || arr[1].Contains("2") ||
                arr[1].Contains("3") || arr[1].Contains("4") ||
                arr[1].Contains("5") || arr[1].Contains("6") ||
                arr[1].Contains("7") || arr[1].Contains("8") ||
                arr[1].Contains("9") || arr[1].Contains("0"))
            {
                _month = int.Parse(arr[1]);
            }

            if (arr[1].Contains("Jan")) _month = 1;
            if (arr[1].Contains("Feb")) _month = 2;
            if (arr[1].Contains("Mar")) _month = 3;
            if (arr[1].Contains("Apr")) _month = 4;
            if (arr[1].Contains("May")) _month = 5;
            if (arr[1].Contains("Jun")) _month = 6;
            if (arr[1].Contains("Jul")) _month = 7;
            if (arr[1].Contains("Aug")) _month = 8;
            if (arr[1].Contains("Sep")) _month = 9;
            if (arr[1].Contains("Oct")) _month = 10;
            if (arr[1].Contains("Nov")) _month = 11;
            if (arr[1].Contains("Dec")) _month = 12;
        }
        
        //Debug.Log("_month" + _month);
        DateTimeFromMainCalendar(_date, _month);
    }

    public void DateTimeFromMainCalendar(int _day, int _month)
    {
        setCurrentTime();

        currentTime.month = _month;
        currentTime.day = _day;
        currentTime.totalDays = DateTime.DaysInMonth(currentTime.year, currentTime.month);
        currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);

        updateCalendar(currentTime.month, currentTime.day);

        onSliderUpdateCaledar();
    }

    public void SnapToTargetMonthAndDate_MiniCalendar()
    {
        GameObject.FindObjectOfType<RB_GameManager>().SnapToTargetMonth_MiniCalendar(currentTime.month);
        GameObject.FindObjectOfType<RB_GameManager>().SnapToTargetDate_MiniCalendar(currentTime.day);
    }

    

    // ON setting the date from the calendar popup on the screen.
    public void OnSetDate()
    {
        if (startDatePicked == true)
        {
            OnSetStartDate();
        }
        else if (endDatePicked == true)
        {
            OnSetEndDate();
        }
    }

    private void OnSetStartDate()
    {
        DateTime todaySystemDate;
        DateTime userSelectedDate;
        if (endDateStr != "")
            endDateSelected = Convert.ToDateTime(endDateStr, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

        todaySystemDate = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, 0, 0, 0);
        userSelectedDate = new DateTime(currentTime.year, currentTime.month, currentTime.day, 0, 0, 0);
        int result = DateTime.Compare(userSelectedDate, todaySystemDate);
        int result1 = DateTime.Compare(userSelectedDate, endDateSelected);
        if (result < 0)
        {
            dateError.text = "Reminder can't be set to previous date";
            dateError.gameObject.SetActive(true);
            return;
        }
        else if (endDateStr != "" && result1 > 0)
        {
            dateError.text = "Start date can't be greater than end date";
            dateError.gameObject.SetActive(true);
            return;
        }
        else
        {
            dateError.gameObject.SetActive(false);
        }

        StartDatePicker.transform.GetChild(0).GetComponent<Text>().text = "";
        StartDatePicker.transform.GetChild(0).GetComponent<Text>().text = currentTime.day + " - " + getShortMonthNameFromNumber(currentTime.month) + " - " + currentTime.year;
        startDateStr = currentTime.day + "-" + currentTime.month + "-" + currentTime.year;
    }

    DateTime endDateSelected;
    private void OnSetEndDate()
    {
        DateTime todaySystemDate;
        DateTime userSelectedDate;

        if (endDatePicked == true)
        {
            todaySystemDate = Convert.ToDateTime(startDateStr, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            userSelectedDate = new DateTime(currentTime.year, currentTime.month, currentTime.day, 0, 0, 0);
            endDateSelected = userSelectedDate;
            int result = DateTime.Compare(userSelectedDate, todaySystemDate);
            if (result < 0)
            {
                dateError.text = "End date can't be less than Start date or Today's date";
                dateError.gameObject.SetActive(true);
                return;
            }
            else
            {
                dateError.gameObject.SetActive(false);
            }

            EndDatePicker.transform.GetChild(0).GetComponent<Text>().text = "";
            EndDatePicker.transform.GetChild(0).GetComponent<Text>().text = currentTime.day + " - " + getShortMonthNameFromNumber(currentTime.month) + " - " + currentTime.year;
            endDateStr = currentTime.day + "-" + currentTime.month + "-" + currentTime.year;
        }
    }



    //On Show time clicked from the time bar on the screen.
    public void OnShowTimer(string pickedTimeName)
    {
        pickedTimePickerName = pickedTimeName;
    }

    // On setting the time from the time popup on the screen.
    public void OnSetTime()
    {
        if (pickedTimePickerName == "PickTimes_1")
        {
            medicineTimingsPerDayArray[0] = (new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));
            TimePickers[0].transform.GetChild(0).GetComponent<Text>().text = DatePickerControl.dateStringFormato; //  DatePickerControl.dateStringFormato.ToString();
        }
        else if (pickedTimePickerName == "PickTimes_2")
        {
            if (medicineTimingsPerDayArray.Count >= 2)
                medicineTimingsPerDayArray[1] = (new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));
            else
                medicineTimingsPerDayArray.Add(new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));

            TimePickers[1].transform.GetChild(0).GetComponent<Text>().text = DatePickerControl.dateStringFormato;
        }
        else if (pickedTimePickerName == "PickTimes_3")
        {
            if (medicineTimingsPerDayArray.Count >= 3)
                medicineTimingsPerDayArray[2] = (new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));
            else
                medicineTimingsPerDayArray.Add(new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));
            TimePickers[2].transform.GetChild(0).GetComponent<Text>().text = DatePickerControl.dateStringFormato;
        }
        else if (pickedTimePickerName == "PickTimes_4")
        {
            if (medicineTimingsPerDayArray.Count >= 4)
                medicineTimingsPerDayArray[3] = (new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));
            else
                medicineTimingsPerDayArray.Add(new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));
            TimePickers[3].transform.GetChild(0).GetComponent<Text>().text = DatePickerControl.dateStringFormato;
        }
        else if (pickedTimePickerName == "PickTimes_5")
        {
            if (medicineTimingsPerDayArray.Count >= 5)
                medicineTimingsPerDayArray[4] = (new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));
            else
                medicineTimingsPerDayArray.Add(new Tuple<int, int, string>(DatePickerControl.fecha.Hour, DatePickerControl.fecha.Minute, DatePickerControl.fecha.ToShortTimeString()));
            TimePickers[4].transform.GetChild(0).GetComponent<Text>().text = DatePickerControl.dateStringFormato;
        }
        // DateTime StartDate = Convert.ToDateTime(DatePickerControl.dateStringFormato.ToString());
    }





    // >>>>>>>>>>>>>>Calendar Functionality Derived. >>>>>>>>>>>>>>>>>>

    override public void onSliderUpdateCaledar()
    {
        //set Days
        foreach (GameObject button in customSliderDaysButtons)
        {
            button.SetActive(false);
            //button.GetComponentInChildren<Image>().enabled = false;
        }

        for (int i = 0; i < currentTime.totalDays; i++)
        {
            customSliderDaysButtons[i].SetActive(true);
            Text[] day = customSliderDaysButtons[i].GetComponentsInChildren<Text>();
            day[1].text = getDayOfWeek(currentTime.year, currentTime.month, i + 1);

            if (checkEventExist(currentTime.year, currentTime.month, i + 1))
            {
                customSliderDaysButtons[i].transform.GetChild(4).GetComponent<Image>().enabled = true;
                customSliderDaysButtons[i].transform.GetChild(4).GetComponent<Image>().color = FlatCalendarStyle2.color_bubbleEvent;
            }
            else
            {
                customSliderDaysButtons[i].transform.GetChild(4).GetComponent<Image>().enabled = false;
            }

            customSliderDaysButtons[i].transform.GetChild(0).gameObject.SetActive(true);
            customSliderDaysButtons[i].transform.GetChild(1).gameObject.SetActive(false);

            if (currentTime.day == i + 1)
            {
                customSliderDaysButtons[i].gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                customSliderDaysButtons[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(255, 255, 255, 255);
                customSliderDaysButtons[i].gameObject.transform.GetChild(3).GetComponent<Text>().color = new Color32(255, 255, 255, 255);
            }
            else
            {
                customSliderDaysButtons[i].gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color32(194, 194, 194, 255);
                customSliderDaysButtons[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(66, 85, 99, 255);
                customSliderDaysButtons[i].gameObject.transform.GetChild(3).GetComponent<Text>().color = new Color32(66, 85, 99, 255);
            }
        }

        //set Month
        for (int i = 0; i < 12; i++)
        {
            customSliderMonthsButtons[i].GetComponentInChildren<Text>().text = getMonthStringFromNumber(i + 1);
            customSliderMonthsButtons[i].transform.GetChild(1).gameObject.SetActive(true);
            if (i + 1 == currentTime.month)
            {
                customSliderMonthsButtons[i].GetComponentInChildren<Text>().color = Color.white; // FlatCalendarStyle2.color_dayTextNormal;
                customSliderMonthsButtons[i].gameObject.transform.GetChild(1).GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                customSliderMonthsButtons[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(255, 255, 255, 255);
            }
            else
            {
                customSliderMonthsButtons[i].gameObject.transform.GetChild(1).GetComponent<Image>().color = new Color32(194, 194, 194, 255);
                customSliderMonthsButtons[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(66, 85, 99, 255);
            }
        }

        //set Year
        for (int i = 0; i < 16; i++)
        {
            customSliderYearsButtons[i].GetComponentInChildren<Text>().text = (System.DateTime.Now.Year + i).ToString();
            if (customSliderYearsButtons[i].GetComponentInChildren<Text>().text == (currentTime.year).ToString())
            {
                customSliderYearsButtons[i].GetComponentInChildren<Text>().color = Color.white; // FlatCalendarStyle2.color_dayTextNormal;
                customSliderYearsButtons[i].gameObject.transform.GetChild(1).GetComponent<Image>().color = new Color32(0, 114, 206, 255);
                customSliderYearsButtons[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(255, 255, 255, 255);
            }
            else
            {
                customSliderYearsButtons[i].gameObject.transform.GetChild(1).GetComponent<Image>().color = new Color32(194, 194, 194, 255);
                customSliderYearsButtons[i].gameObject.transform.GetChild(2).GetComponent<Text>().color = new Color32(66, 85, 99, 255);
            }
         }

        //set Events       
        populateEventVisualizer();
    }

    //ON Day selected from Calendar.
    override public void onSliderDaySelect(GameObject obj)
    {
        foreach (GameObject o in customSliderDaysButtons)
        {
            o.transform.GetChild(4).GetComponent<Image>().enabled = false;
        }
        Text[] day_number = obj.GetComponentsInChildren<Text>();
        currentTime.day = Int32.Parse(day_number[0].text);
        onSliderUpdateCaledar();
        // Debug.Log("Coming to custom onslider day select..." + startDatePicked + "  " + endDatePicked + "   " + currentTime.day + "   " + currentTime.month + "  " + currentTime.year);
    }

    //On Month selected from the calendar.
    override public void onSliderMonthSelect(GameObject obj)
    {
        foreach (GameObject o in customSliderDaysButtons)
        {
            o.transform.GetChild(4).GetComponent<Image>().enabled = false;
        }
        for (int i = 0; i < 12; i++)
        {
            if (customSliderMonthsButtons[i] == obj)
            {
                currentTime.month = i + 1;
                if (currentTime.day == 31 || currentTime.day == 30)
                {
                    if (currentTime.month == 2)
                    {
                        currentTime.day = 29;
                    }
                    if (currentTime.day == 31)
                    {
                        if (currentTime.month == 4 || currentTime.month == 6 || currentTime.month == 9 ||
                            currentTime.month == 11)
                        {
                            currentTime.day = 30;
                        }
                    }
                }
                currentTime.totalDays = DateTime.DaysInMonth(currentTime.year, currentTime.month);
                currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);
            }
        }
        onSliderUpdateCaledar();
    }

    //On Year selected from the calendar.
    override public void onSliderYearSelect(GameObject obj)
    {
        foreach (GameObject o in customSliderYearsButtons)
        {
            o.GetComponentInChildren<Image>().enabled = false;
        }
        for (int i = 0; i < customSliderYearsButtons.Length; i++)
        {
            if (customSliderYearsButtons[i] == obj)
            {
                currentTime.year = Int32.Parse(sliderYearsButtons[i].GetComponentInChildren<Text>().text);
                currentTime.month = 1;
                currentTime.day = 1;
                currentTime.totalDays = DateTime.DaysInMonth(currentTime.year, currentTime.month);
                currentTime.dayOffset = getIndexOfFirstSlotInMonth(currentTime.year, currentTime.month);
            }
        }
        onSliderUpdateCaledar();
    }


    public void OnCustomMedNameChanged()
    {
        CustomMedicineNameError.SetActive(false);
    }

    private EventObj cachedEvent;

    //private EventObj cachedEvent;
    public void _CacheOldEventBeforeApplyingEditOnEntireSeries(EventObj e)
    {
        cachedEvent = e;
    }

     override public void evtListener_AddNewEvent()
     {
        StartCoroutine(evtListener_ModifyEvent_Coroutine());
     }
     

    private IEnumerator evtListener_ModifyEvent_Coroutine()
    {
        //panel_Waiting.SetActive(true);

        yield return new WaitForSeconds(0f);

        if (modifingEvent != null)
        {
            //removeAllAssociatedEvent(currentTime.year, currentTime.month, currentTime.day, cachedEvent);
            yield return RunRemoveEventTaskAsync(currentTime.year, currentTime.month, currentTime.day, cachedEvent);
        }
        string customMedName;

        //Check all the fields are entered to submit the reminder data.
        if (medicationTypeStr == "")
        {
            MessageTypeError.SetActive(true);
            //panel_Waiting.SetActive(false);
            yield break;
        }
        else if (medicationTypeStr == "Custom")
        {
            customMedName = CustomMedicineTxt.GetComponent<InputField>().text;
            if (customMedName == "")
            {
                CustomMedicineNameError.SetActive(true);
                //panel_Waiting.SetActive(false);
                yield break;
            }
            else
            {
                CustomMedicineNameError.SetActive(false);
            }
        }
        else
        {
            MessageTypeError.gameObject.SetActive(false);
        }

        //validate whether end date is choosed.
        if (endDateStr == "")
        {
            dateError.text = "End date can't be empty";
            dateError.gameObject.SetActive(true);
            //panel_Waiting.SetActive(false);
            yield break;
        }
        else
        {
            dateError.gameObject.SetActive(false);
        }

        //validate whether same time is set for multiple times as reminder
        int time_CountValue = 0;
        for (int i = 0; i < TimePickers.Length; i++)
        {
            if (TimePickers[i].activeSelf)
            {
                time_CountValue++;
            }
        }
        if (time_CountValue > 1)
        {
            string[] timeStr = new string[time_CountValue];
            for (int i = 0; i < time_CountValue; i++)
            {
                timeStr[i] = TimePickers[i].transform.GetChild(0).GetComponent<Text>().text.ToString();
            }
            for (int i = 0; i < time_CountValue; i++)
            {
                for (int j = 0; j < time_CountValue; j++)
                {
                    if (i != j)
                    {
                        if (timeStr[i].Equals(timeStr[j]))
                        {
                            //Debug.Log(timeStr[i]);
                            //print(ENABLE) error message here
                            PopupErrorTimeMultiple.SetActive(true);
                            //panel_Waiting.SetActive(false);
                            yield break;
                        }
                    }
                }
            }
        }
        //print(DISABLE) error message here
        PopupErrorTimeMultiple.SetActive(false);
        btn_AddEvent[0].GetComponent<Button>().interactable = false;
        Color temp = btn_AddEvent[0].transform.GetChild(0).GetComponent<Image>().color;
        temp.a = 0.5f;
        btn_AddEvent[0].transform.GetChild(0).GetComponent<Image>().color = temp;

        LoadingCircle.SetActive(true);

        // Debug.Log("Coming here to confirm on custom derived reminder class....."+ startDateStr + "  "+ endDateStr);
        yield return RunTaskAsync(); // .AsIEnumerator();
                                     //yield return  TaskAsyncCountDown(20,"") .AsIEnumerator();
 

        addEvent_Visualizer.gameObject.SetActive(false);
        modifingEvent = null;
        
      
        base.onSliderUpdateCaledar();
        base.SnapToTargetMonthAndDate();
        OnResetAddReminderPage();
        editExistingEvent = false;
        reminder_FooterPanel.SetActive(true);
        //panel_Waiting.SetActive(false);
        if (!currentAlarmValue)
        {
            currentAlarmValue = true;
        }

#if UNITY_ANDROID
        yield return SetMobileNotification();
#endif
    }
     

    private async Task SetMobileNotification()
    {
        DateTime StartDate = Convert.ToDateTime(startDateStr, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        DateTime EndDate =  Convert.ToDateTime(endDateStr, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat); //  StartDate.AddDays(4); // 
        string storedReminderTimes = MedicineTimingsDropDown.options[MedicineTimingsDropDown.value].text;

        DateTime notiTime;
        string description = "";
        TimeSpan new_time;
     
        foreach (DateTime day in EachCalendarDay(StartDate, EndDate, MedicineDaysTypeDropDown, customMedicineDaysArray))
        {
            double hours = day.Subtract(EndDate).TotalHours;

            //Debug.Log("Date is : " + day.ToString("dd-MM-yyyy"));
            for (int i = 0; i < medicineTimingsPerDayArray.Count; i++)
            {
                if (day <= StartDate.AddDays(2) || (hours < 48 && hours > 0))
                {
                    new_time = new TimeSpan(medicineTimingsPerDayArray[i].Item1, medicineTimingsPerDayArray[i].Item2, 00);
                    EventObj evnt = new EventObj(name, description, new_time, sendReminderData.storedUserId, StartDate.Date, EndDate.Date, storedCustomDays.ToString(), storedMedicineTimings.ToString(), storedReminderTimes);

                    notiTime = new DateTime(day.Date.Year, day.Date.Month, day.Date.Day, evnt.time.Value.Hours, evnt.time.Value.Minutes, evnt.time.Value.Seconds);
                    MyNotificationManager.Instance.ShowAndroidNotification("Medication Reminder", evnt.name, notiTime);

                    if (day.AddDays(2) >= EndDate && i == 0)
                    {
                        DateTime midDayTime = new DateTime(day.Date.Year, day.Date.Month, day.Date.Day, 12, minutesAndroidCounter, minutesAndroidCounter);
                        MyNotificationManager.Instance.ShowNotificationAfterDelay("Medication Reminder", "Your Reminder for " + evnt.name + " is about to expire. Please update it if you wish to continue.", midDayTime);
                        minutesAndroidCounter++;
                    }

                    /*if ((hours < 48 && hours > 0))
                    {
                        MyNotificationManager.Instance.ShowAndroidNotification("Medication Reminder", "Your Reminder for " + evnt.name + " is about to expire. Please update it if you wish to continue.", notiTime);
                    }*/
                }
                await Task.Delay(0).ConfigureAwait(false);
            }
        }

    } 



    // Demo function fro Asynchronous call.
    public async Task TaskAsyncCountDown(int count, string flag = "")
    {
        for (int i = count; i >= 0; i--)
        {
            //Debug.Log(i +" "+ flag);
            await Task.Delay(1000).ConfigureAwait(false);
        }
    }

    public async Task RunTaskAsync()
    {
        DateTime StartDate = Convert.ToDateTime(startDateStr, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        DateTime EndDate = Convert.ToDateTime(endDateStr, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string storedReminderTimes = MedicineTimingsDropDown.options[MedicineTimingsDropDown.value].text;
        AddAllCustomDaysToString();
        AddAllTimingsPerDayToString();

        string name = "";
        string description = "";
        DateTime notiTime;
        //DateTime modifyNotiTime;
        TimeSpan new_time;
        string customMediName = "";

        foreach (DateTime day in EachCalendarDay(StartDate, EndDate, MedicineDaysTypeDropDown, customMedicineDaysArray))
        {
            //Debug.Log("Date is : " + day.ToString("dd-MM-yyyy"));
            for (int i = 0; i < medicineTimingsPerDayArray.Count; i++)
            {
                new_time = new TimeSpan(medicineTimingsPerDayArray[i].Item1, medicineTimingsPerDayArray[i].Item2, 00);


                if (medicationTypeStr == "Custom")
                {
                    customMediName = CustomMedicineTxt.GetComponent<InputField>().text;
                    name = customMediName;
                }
                else
                {
                    name = medicineNameStr;
                }

                description = MedicineDaysTypeDropDown.options[MedicineDaysTypeDropDown.value].text;

                EventObj evnt = new EventObj(name, description, new_time, sendReminderData.storedUserId, StartDate.Date, EndDate.Date, storedCustomDays.ToString(), storedMedicineTimings.ToString(), storedReminderTimes);
                evnt.alarm = currentAlarmValue;

                //event already exists
                if (modifingEvent != null)
                {
                    addEvent(day.Date.Year, day.Date.Month, day.Date.Day, evnt);
                }
                else
                {
                    addEvent(day.Date.Year, day.Date.Month, day.Date.Day, evnt);
                }
                notiTime = new DateTime(day.Date.Year, day.Date.Month, day.Date.Day, evnt.time.Value.Hours, evnt.time.Value.Minutes, evnt.time.Value.Seconds);

                if (day.AddDays(2) >= EndDate && i == 0)
                {
                    DateTime midDayTime = new DateTime(day.Date.Year, day.Date.Month, day.Date.Day, 12, minutesCounter, minutesCounter);
                    MyNotificationManager.Instance.ShowNotificationAfterDelay("Medication Reminder", "Your Reminder for " + evnt.name + " is about to expire. Please update it if you wish to continue.", midDayTime);
                    minutesCounter++;
                }

                //else
                //{
                MyNotificationManager.Instance.ShowNotificationAfterDelay("Medication Reminder", evnt.name, notiTime);
                //}
  

                //markSelectionDay(currentTime.day);
                //sliderDaysButtons[day.Date.Day - 1].GetComponentInChildren<Image>().enabled = true;
                //updateCalendar(day.Date.Month, day.Date.Day);

                await Task.Delay(0).ConfigureAwait(true);  // As i am not blocking the asychronous operation on the thread pool. True will Resuming it on UI thread.
            }
        }

        if (filePath == "")
            filePath = Application.persistentDataPath + "/events.json";
        exportFlatCalendarEvents(filePath);
        setCurrentTime();
        sendReminderData.SendJSONDataToServer();
        onSliderUpdateCaledar();
        LoadingCircle.SetActive(false);
        btn_AddEvent[0].GetComponent<Button>().interactable = true;
        Color temp = btn_AddEvent[0].transform.GetChild(0).GetComponent<Image>().color;
        temp.a = 1f;
        btn_AddEvent[0].transform.GetChild(0).GetComponent<Image>().color = temp;
         
    }

    public static IEnumerable<DateTime> EachCalendarDay(DateTime startDate, DateTime endDate, Dropdown _customDaysDropDownSelected, List<string> _customDaysArray)
    {
        // Everyday = 1, EveryOtherDay = 2, Weekly = 7, Monthly = 30, Fortnightly = 14, Dialysis Day = customMedicineDaysArray, Custom Days = customMedicineDaysArray
        string frequency = _customDaysDropDownSelected.options[_customDaysDropDownSelected.value].text;

        switch (frequency)
        {
            case "Everyday":
                for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(1)) yield return date;
                break;
            case "Every other day":
                for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(2)) yield return date;
                break;
            case "Weekly":
                for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(7)) yield return date;
                break;
            case "Fortnightly":
                for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(14)) yield return date;
                break;
            case "Monthly":
                for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(30)) yield return date;
                break;

            case "Custom":

                for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(1))
                {
                    for (int i = 0; i < _customDaysArray.Count; i++)
                    {
                        if (_customDaysArray[i] == date.DayOfWeek.ToString().Substring(0, 3))
                        {
                            yield return date;
                        }
                    }
                }
                break;
            case "Dialysis days":
                for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(1))
                {
                    for (int i = 0; i < _customDaysArray.Count; i++)
                    {
                        if (_customDaysArray[i] == date.DayOfWeek.ToString().Substring(0, 3))
                        {
                            yield return date;
                        }
                    }
                }
                break;
        }
    }

    // ON Resetting the reminder pages once done.
    private void OnResetAddReminderPage()
    {
        MedicineTypeDropDown.value = 0;
        MedicineTypeDropDownChanged(MedicineTypeDropDown);
        MedicineDaysTypeDropDown.value = 0;
        MedicineDaysTypeDropDownChanged(MedicineDaysTypeDropDown);
        MedicineTimingsDropDown.value = 0;
        MedicineTimingsDropDownChanged(MedicineTimingsDropDown);

        StartDatePicker.transform.GetChild(0).GetComponent<Text>().text = currentTime.day + " - " + getShortMonthNameFromNumber(currentTime.month) + " - " + currentTime.year;
        EndDatePicker.transform.GetChild(0).GetComponent<Text>().text = "";

        CustomMedicineTxt.text = "";
        TimePickers[0].transform.GetChild(0).GetComponent<Text>().text = "";  // System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute;
        TimePickers[1].transform.GetChild(0).GetComponent<Text>().text = "";
        TimePickers[2].transform.GetChild(0).GetComponent<Text>().text = "";
        TimePickers[3].transform.GetChild(0).GetComponent<Text>().text = "";
        TimePickers[4].transform.GetChild(0).GetComponent<Text>().text = "";
    }

    //Mofifying the data.
    override public void evtListener_ModifyEvent(EventObj e)
    {
        //Debug.Log(e.name);
        //cachedEvent = e;
        editExistingEvent = true;
        reminder_FooterPanel.SetActive(false);
        addEvent_Visualizer.gameObject.SetActive(true);
        setLanguageLabelAddEvent();
        event_Visualizer.gameObject.SetActive(false);
        SetOptionModifyValues(e.name);
        SetDefaultFrequency(e.description, e.storedCustomDays);

        // Adding start date from modify data
        StartDatePicker.transform.GetChild(0).GetComponent<Text>().text = "";
        StartDatePicker.transform.GetChild(0).GetComponent<Text>().text = e.startDate.Day + " - " + getShortMonthNameFromNumber(e.startDate.Month) + " - " + e.startDate.Year;
        startDateStr = e.startDate.Day + " - " + getShortMonthNameFromNumber(e.startDate.Month) + " - " + e.startDate.Year;

        //Adding end date from modify data.
        EndDatePicker.transform.GetChild(0).GetComponent<Text>().text = "";
        EndDatePicker.transform.GetChild(0).GetComponent<Text>().text = e.endDate.Day + " - " + getShortMonthNameFromNumber(e.endDate.Month) + " - " + e.endDate.Year;
        endDateStr = e.endDate.Day + " - " + getShortMonthNameFromNumber(e.endDate.Month) + " - " + e.endDate.Year;

        MedicineTimingsDropDown.value = MedicineTimingsDropDown.options.FindIndex((i) => { return i.text.Equals(e.storedReminderTimes); });
        MedicineTimingsDropDownChanged(MedicineTimingsDropDown);


        string[] _storedCustomTimings = e.storedMedicineTimings.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(mobile => mobile.Trim()).Where(s => s != string.Empty).ToArray<string>();
        medicineTimingsPerDayArray.Clear();
        for (int i = 0; i < _storedCustomTimings.Length; i++)
        {
            //Debug.Log("_storedCustomTimings" + i + " : " + _storedCustomTimings[i]);
            string[] _extractTimeFromString = _storedCustomTimings[i].Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries).Select(mobile => mobile.Trim()).Where(s => s != string.Empty).ToArray<string>();
            TimePickers[i].transform.GetChild(0).GetComponent<Text>().text = _extractTimeFromString[0] + ":" + _extractTimeFromString[1];
            medicineTimingsPerDayArray.Add(new Tuple<int, int, string>(Int32.Parse(_extractTimeFromString[0]), Int32.Parse(_extractTimeFromString[1]), DatePickerControl.fecha.ToShortTimeString()));
            TimePickers[i].SetActive(true);
        }
 
        modifingEvent = e;
        if (e.alarm != currentAlarmValue)
        {
            evtListener_onAlarm_Enable_Disable();
        }
    }

    //Modifying the event, default values.
    private void SetOptionModifyValues(string choosedMedicineName)
    {
        switch (choosedMedicineName)
        {
            //Phosphate.
            case "Renacet (Calcium Acetate)":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 0;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Swallow with liquid, during or immediately after a meal and do not chew.";
                break;
            case "Phosex (Calcium Acetate)":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 1;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Take with meals. Swallow whole with liquid. Can be broken, but don't chew. ";
                break;
            case "Osvaren (Calcium Acetate + Mg)":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 2;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Take with meals. Do not crush or chew.";
                break;
            case "Calcichew (Calcium Carbonate)":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 3;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "For phosphate binding, chew or suck before, during or just after a meal.";
                break;
            case "Renvela (Sevelamer Carbonate)":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 4;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Swallow whole with food and not on an empty stomach. Do not crush, chew or break.";
                break;

            case "Sevelamer Carbonate":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 5;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Swallow whole with food and not on an empty stomach. Do not crush, chew or break.";
                break;

            case "Renagel (Sevelamer Chloride)":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 6;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Swallow whole with meals. Do not crush, chew or break.";
                break;

            case "Fosrenol (Lanthanum Carbonate)":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 7;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Chew during or immediately after meals. Do not swallow whole. Can be crushed first to aid chewing.";
                break;
            case "Velphoro (Sucroferric Oxyhydroxide)":
                MedicineTypeDropDown.value = 1;
                MedicineNameDropDown.value = 8;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(true);
                MedicineDetailsContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Chew during meals. Do not swallow whole. Can be crushed first to aid chewing.";
                break;

            //Multivitamin  
            case "Renavit":
                MultivitaminContainer.SetActive(true);
                MedicineDetailsContainer.SetActive(false);
                MedicineTypeDropDown.value = 2;
                MultivitaminContainer.transform.GetChild(1).GetComponent<Dropdown>().value = 0;
                MultivitaminContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Take with a little water after dialysis. Swallow whole. Do not chew.";
                break;
            case "Ketovite":
                MultivitaminContainer.SetActive(true);
                MedicineDetailsContainer.SetActive(false);
                MedicineTypeDropDown.value = 2;
                MultivitaminContainer.transform.GetChild(1).GetComponent<Dropdown>().value = 1;
                MultivitaminContainer.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "Take 1 tablet three times a day after dialysis. Keep the tablets in the fridge.";
                break;

            default:
                CustomMedicineContainer.SetActive(true);
                MedicineTypeDropDown.value = 3;
                MultivitaminContainer.SetActive(false);
                MedicineDetailsContainer.SetActive(false);
                CustomMedicineTxt.text = choosedMedicineName;
                break;

        }
    }

    //Modifying the event, default values.
    private void SetDefaultFrequency(string choosedMedicineDaysType, string storedCustomDaysStr)
    {
        string[] _storedCustomDays;
        switch (choosedMedicineDaysType)
        {
            case "Everyday":
                MedicineDaysTypeDropDown.value = 0;
                break;
            case "Every other day":
                MedicineDaysTypeDropDown.value = 1;
                break;

            case "Dialysis days":
                CustomMedicineDays.SetActive(true);
                MedicineDaysTypeDropDown.value = 2;

                _storedCustomDays = storedCustomDaysStr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(mobile => mobile.Trim()).Where(s => s != string.Empty).ToArray<string>();
                for (int i = 0; i < CustomDaysGameObjects.Length; i++)
                {
                    if (_storedCustomDays.Contains(CustomDaysGameObjects[i].name))
                    {
                        CustomDaysGameObjects[i].transform.GetChild(0).GetComponent<Image>().enabled = true;
                        CustomDaysGameObjects[i].transform.GetChild(1).gameObject.GetComponent<Text>().color = Color.white;
                        customMedicineDaysArray.Add(CustomDaysGameObjects[i].name);
                    }
                    else
                    {
                        CustomDaysGameObjects[i].transform.GetChild(0).GetComponent<Image>().enabled = false;
                        CustomDaysGameObjects[i].transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(50, 50, 50, 255);
                        customMedicineDaysArray.Remove(CustomDaysGameObjects[i].name);
                    }
                }
                break;

            case "Weekly":
                MedicineDaysTypeDropDown.value = 3;
                break;
            case "Fortnightly":
                MedicineDaysTypeDropDown.value = 4;
                break;
            case "Monthly":
                MedicineDaysTypeDropDown.value = 5;
                break;

            case "Custom":
                CustomMedicineDays.SetActive(true);
                MedicineDaysTypeDropDown.value = 6;

                _storedCustomDays = storedCustomDaysStr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(mobile => mobile.Trim()).Where(s => s != string.Empty).ToArray<string>();

                for (int i = 0; i < CustomDaysGameObjects.Length; i++)
                {
                    if (_storedCustomDays.Contains(CustomDaysGameObjects[i].name))
                    {
                        CustomDaysGameObjects[i].transform.GetChild(0).GetComponent<Image>().enabled = true;
                        CustomDaysGameObjects[i].transform.GetChild(1).gameObject.GetComponent<Text>().color = Color.white;
                        customMedicineDaysArray.Add(CustomDaysGameObjects[i].name);
                    }
                    else
                    {
                        CustomDaysGameObjects[i].transform.GetChild(0).GetComponent<Image>().enabled = false;
                        CustomDaysGameObjects[i].transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(50, 50, 50, 255);
                        customMedicineDaysArray.Remove(CustomDaysGameObjects[i].name);
                    }
                }

                break;

        }
    }


    //Adding fresh custom date details to json and server which is not stored in earlier development.
    public StringBuilder storedCustomDays = new StringBuilder();
    public StringBuilder storedMedicineTimings = new StringBuilder();
    private void AddAllCustomDaysToString()
    {
        storedCustomDays.Clear();

        for (int i = customMedicineDaysArray.Count - 1; i > 0; i--)
        {
            storedCustomDays.Append(customMedicineDaysArray[i]);
            storedCustomDays.Append(",");

        }
    }

    //Adding fresh timing per day details to json and server which is not stored in earlier development.
    private void AddAllTimingsPerDayToString()
    {
        storedMedicineTimings.Clear();
        for (int i = 0; i < medicineTimingsPerDayArray.Count; i++)
        {
            TimeSpan new_time = new TimeSpan(medicineTimingsPerDayArray[i].Item1, medicineTimingsPerDayArray[i].Item2, 00);
            storedMedicineTimings.Append(new_time);
            storedMedicineTimings.Append(",");
        }
    }

    //To Remove All reminders with the same time and same medicine (Remove associated Reminders).
    //public void removeAllAssociatedEvent(int year, int month, int day, EventObj selectedEV)
    public async Task RunRemoveEventTaskAsync(int year, int month, int day, EventObj selectedEV)
    {
        string[] _storedCustomTimings = selectedEV.storedMedicineTimings.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(mobile => mobile.Trim()).Where(s => s != string.Empty).ToArray<string>();
        //_storedCustomTimings.Reverse();
        List<EventObj> existingEvent;

        Debug.Log("_storedCustomTimings : " + _storedCustomTimings.Length);

        foreach (DateTime eachDay in EachCalendarDay(selectedEV.startDate, selectedEV.endDate, MedicineDaysTypeDropDown, customMedicineDaysArray))
        {
            existingEvent = getEventList(eachDay.Date.Year, eachDay.Date.Month, eachDay.Date.Day).OrderBy(x => x.time).ToList();

            foreach (EventObj ev in existingEvent)
            {
                 //Debug.Log(ev.time);
                for (int i = 0; i < _storedCustomTimings.Length; i++)
                {
                    Debug.Log("_storedCustomTimings inside loop :" + _storedCustomTimings[i]);
                    if (_storedCustomTimings[i] == ev.time.ToString() && selectedEV.name == ev.name)
                    {
                        removeEvent(eachDay.Date.Year, eachDay.Date.Month, eachDay.Date.Day, ev);
                    }
                }
                 await Task.Delay(0).ConfigureAwait(true);
            }
        }

       /* populateEventVisualizer();
        onSliderUpdateCaledar();
        updateCalendar(currentTime.month, currentTime.day);
        event_Visualizer.gameObject.SetActive(false);

        if (filePath != "")
            exportFlatCalendarEvents(filePath); */
    }

}


 
