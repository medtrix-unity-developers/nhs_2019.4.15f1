﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Text;
using System;
using UnityEngine.UI;
using Newtonsoft.Json;

//TODO REeminder webservices for stalin.
public class SendReminderData : MonoSingleton<SendReminderData>
{
    private string FilePath;
    public int storedUserId;
    string setURL = "https://www.medtrixhealthcare.com/NHS_ChatBot/NHSChatbot/SetRemainderAPI";

    [SerializeField] private ReminderData _RemiderDatas = new ReminderData();

    void Awake()
    {
        storedUserId = PlayerPrefs.GetInt("UserUniqueID");
        AppManager.Instance.PatientUserID = PlayerPrefs.GetInt("UserUniqueID");
        GetPosts(storedUserId.ToString());
    }

    void Start()
    {
        FilePath = Path.Combine(Application.persistentDataPath, "events.json");
    }

    public void SendJSONDataToServer()
    {
        FilePath = Path.Combine(Application.persistentDataPath, "events.json");
        StartCoroutine(PostMethod());
    }

    IEnumerator PostMethod()
    {
        string reminderJSONData = File.ReadAllText(Application.persistentDataPath + "/events.json");
        Debug.Log("patientJSONData : " + reminderJSONData);

        var request = new UnityWebRequest(setURL, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(reminderJSONData);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();
        if (!request.isNetworkError)
        {
            Debug.Log("Data successfully sent to server." + request.downloadHandler.text);
        }
        else
        {
            Debug.Log("Error sending data to server.");
        }
    }

    //Testing whether it will work or not.
    public void GetPosts(string userID)
    {
        StartCoroutine(GetRequest("https://www.medtrixhealthcare.com/NHS_ChatBot/NHSChatbot/GetRemainderAPI?userId="+ storedUserId.ToString(), (UnityWebRequest req) =>
          {
              if (req.isNetworkError || req.isHttpError)
              {
                  Debug.Log($"{req.error}: {req.downloadHandler.text}");
              }
              else
              { 
                    string response = System.Text.Encoding.UTF8.GetString(req.downloadHandler.data);
                  //_RemiderDatas = JsonConvert.DeserializeObject<ReminderData>(response);
                  _RemiderDatas.status = JsonUtility.FromJson<ReminderData>(response).status;
 
                  SaveReminderDataToLocal();
                  //Debug.Log("GET REMINDER JSON  : " + _RemiderDatas.status);
 
              }
          }));
    }

    IEnumerator GetRequest(string endpoint, Action<UnityWebRequest> callback)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(endpoint))
        {
            //Debug.Log("GET REMINDER REQUEST  : " + request);
            // Send the request and wait for a response
            yield return request.SendWebRequest();

            callback(request);
        }
    }

    public void SaveReminderDataToLocal()
    {
        //string jsonString = JsonUtility.ToJson(_RemiderDatas.status); // old working..
        string jsonString = JsonConvert.SerializeObject(_RemiderDatas.status);

        try
        {
            File.WriteAllText(FilePath, jsonString);
            Debug.Log("File Saved");
        }
        catch (Exception)
        {
            Debug.Log("File Couldn't save.");
        }
    }
     
}

[System.Serializable]
public class ReminderData
{
    public List<Status> status = new List<Status>();
 
}
[System.Serializable]
public class Status
{
    public string Id;
    public bool Alarm; // { get; set; }
    public string Day; // { get; set; }
    public string Name; // { get; set; }
    public string Description; // { get; set; }

    public int UserId; // { get; set; }
    public string Created_Date;
    public string Updated_Date;
    public string storedCustomDays; // { get; set; }
    public string storedMedicineTimings; // { get; set; }
    public string storedReminderTimes; // { get; set; }
    public string startDate; // { get; set; }
    public string endDate; // { get; set; }
}