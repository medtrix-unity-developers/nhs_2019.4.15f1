﻿using UnityEngine;
using UnityEngine.UI.Extensions;
using static FlatCalendar2;
using System;
 
public class CalendarInit : MonoBehaviour
{
    //Ref to the Calendar prefab
    public FlatCalendar2 calendar;
    private bool isFocused;
    private string evntName;
    private int delayHours;
    private int delayMinutes;
    private int delaySeconds;


    public void Start()
    {
        ////Set the event callback 
        //calendar.setCallback_OnTriggerEvent(Notify);

        ////init the calendar (important)
        //calendar.initFlatCalendar();
        //calendar.evtListener_SwitchModality();

     }

    public void OnCalendarInitEnabled()
    {
        //Set the event callback 
        calendar.setCallback_OnTriggerEvent(Notify);
        //init the calendar (important)
        calendar.initFlatCalendar();
        calendar.evtListener_SwitchModality();
    }

    public void OnCalendarInitDisabled()
    {
        //calendar.scrollRects[0].GetComponent<HorizontalScrollSnap>().enabled = false;
        //calendar.scrollRects[1].GetComponent<HorizontalScrollSnap>().enabled = false;
        //calendar.scrollRects[1].GetComponent<HorizontalScrollSnap>().RestartOnEnable = false;
    }


    //Method called when an event occurs
    public void Notify(EventObj evnt)
    {
        //DateTime dt = DateTime.Now;
        //delayHours = evnt.time.Value.Hours - dt.Hour;
        //delayMinutes = evnt.time.Value.Minutes -  dt.Minute;
        //evntName = evnt.name;
        evnt.print();
    }

 /*
#if UNITY_EDITOR ||  UNITY_ANDROID
    private void OnApplicationFocus(bool focus)
    {
        isFocused = focus;
        //Debug.Log("OnApplicationFocus Editor.. :::" + focus);

        if (focus == false)
        {
            //if user left your app schedule all your notifications
            GleyNotifications.SendNotification("Medication Reminder", evntName, new System.TimeSpan(delayHours, delayMinutes, delaySeconds), null, null, "Opened from Gley Minimized Notification");
        }
        else
        {
            //call initialize when user returns to your app to cancel all pending notifications
            //GleyNotifications.Initialize();
        }
    }
#endif

#if UNITY_EDITOR || UNITY_IOS
    private void OnApplicationPause(bool focus)
    {
        isFocused = focus;
        Debug.Log("OnApplicationPause Editor.. :::"+ focus);

        if (focus == true)
        {

            //if user left your app schedule all your notifications
            //GleyNotifications.SendNotification("Medication Reminder", evntName, new System.TimeSpan(delayHours, delayMinutes, delaySeconds), null, null, "Opened from Gley Minimized Notification");
        }
        else
        {
            //call initialize when user returns to your app to cancel all pending notifications
            //GleyNotifications.Initialize();
        }
    }
#endif
   */
}
