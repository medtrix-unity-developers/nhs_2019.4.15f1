﻿using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [RequireComponent(typeof(MyScrollRect))]
    public class HorizontalScrollSnap : ScrollSnapBase, IEndDragHandler
    {
        [Tooltip("The container the screens or pages belong to. REQUIRED")]
        public Transform ScreensContainer;


        //private int _screens = 1;
        private int _startingScreen = 1;

        private bool _fastSwipeTimer = false;
        private int _fastSwipeCounter = 0;
        private int _fastSwipeTarget = 30;


        private System.Collections.Generic.List<Vector3> _positions;
        private MyScrollRect myScrollRect;
        //private Vector3 _lerp_target;
        //private bool _lerp;

        private int _containerSize;

        //[Tooltip("The gameobject that contains toggles which suggest pagination. THIS CAN BE MISSING")]
        //public GameObject Pagination;

        //[Tooltip("Button to go to the next page. THIS CAN BE MISSING")]
        //public GameObject NextButton;
        //[Tooltip("Button to go to the previous page. THIS CAN BE MISSING")]
        //public GameObject PrevButton;

        //public Boolean UseFastSwipe = true;
        //public int FastSwipeThreshold = 100;

        // Use this for initialization
        void Start()
        {

            DistributePages();

            _screens = ScreensContainer.childCount;
            _startingScreen = 1;

            myScrollRect = gameObject.GetComponent<MyScrollRect>();
            _lerp = false;

            _positions = new System.Collections.Generic.List<Vector3>();

            if (_screens > 0)
            {
                for (int i = 0; i < _screens; ++i)
                {
                    _scroll_rect.horizontalNormalizedPosition = (float)i / (float)(_screens - 1);
                    _positions.Add(ScreensContainer.localPosition);
                }
            }

            _scroll_rect.horizontalNormalizedPosition = (float)(_startingScreen - 1) / (float)(_screens - 1);

            _containerSize = (int)ScreensContainer.gameObject.GetComponent<RectTransform>().offsetMax.x;

            ChangeBulletsInfo(CurrentScreen());

            if (NextButton)
                NextButton.GetComponent<Button>().onClick.AddListener(() => { NextScreen(); });

            if (PrevButton)
                PrevButton.GetComponent<Button>().onClick.AddListener(() => { PreviousScreen(); });
        }

        void Update()
        {
            if (_lerp)
            {
                ScreensContainer.localPosition = Vector3.Lerp(ScreensContainer.localPosition, _lerp_target, 7.5f * Time.deltaTime);
                if (Vector3.Distance(ScreensContainer.localPosition, _lerp_target) < 0.005f)
                {
                    _lerp = false;
                }

                //change the info bullets at the bottom of the screen. Just for visual effect
                if (Vector3.Distance(ScreensContainer.localPosition, _lerp_target) < 10f)
                {
                    ChangeBulletsInfo(CurrentScreen());
                }
            }

            if (_fastSwipeTimer)
            {
                _fastSwipeCounter++;
            }

        }

        private bool fastSwipe = false; //to determine if a fast swipe was performed
        public void DragEnd()
        {
            _startDrag = true;
            if (_scroll_rect.horizontal)
            {
                if (UseFastSwipe)
                {

                    fastSwipe = false;
                    _fastSwipeTimer = false;
                    if (_fastSwipeCounter <= _fastSwipeTarget)
                    {
                        if (Math.Abs(_startPosition.x - ScreensContainer.localPosition.x) > FastSwipeThreshold)
                        {
                            fastSwipe = true;
                        }
                    }
                    if (fastSwipe)
                    {
                        if (_startPosition.x - ScreensContainer.localPosition.x > 0)
                        {
                            NextScreenCommand();
                        }
                        else
                        {
                            PrevScreenCommand();
                        }
                    }
                    else
                    {
                        _lerp = true;
                        _lerp_target = FindClosestFrom(ScreensContainer.localPosition, _positions);
                    }


                }
                else
                {
                    _lerp = true;
                    _lerp_target = FindClosestFrom(ScreensContainer.localPosition, _positions);
                }

            }
        }

        private bool _startDrag = true;

        public void OnDrag()
        {
            _lerp = false;
            if (_startDrag)
            {
                OnDragStart();
                _startDrag = false;
            }
        }

        //private Vector3 _startPosition = new Vector3();
        private int _currentScreen;

        public void OnDragStart()
        {
            _startPosition = ScreensContainer.localPosition;
            _fastSwipeCounter = 0;
            _fastSwipeTimer = true;
            _currentScreen = CurrentScreen();
        }

        //Function for switching screens with buttons
        public void NextScreen()
        {
            if (CurrentScreen() < _screens - 1)
            {
                _lerp = true;
                _lerp_target = _positions[CurrentScreen() + 1];

                ChangeBulletsInfo(CurrentScreen() + 1);
            }
        }

        //Function for switching screens with buttons
        public void PreviousScreen()
        {
            if (CurrentScreen() > 0)
            {
                _lerp = true;
                _lerp_target = _positions[CurrentScreen() - 1];

                ChangeBulletsInfo(CurrentScreen() - 1);
            }
        }

        //Because the CurrentScreen function is not so reliable, these are the functions used for swipes
        private void NextScreenCommand()
        {
            if (_currentScreen < _screens - 1)
            {
                _lerp = true;
                _lerp_target = _positions[_currentScreen + 1];

                ChangeBulletsInfo(_currentScreen + 1);
            }
        }

        //Because the CurrentScreen function is not so reliable, these are the functions used for swipes
        private void PrevScreenCommand()
        {
            if (_currentScreen > 0)
            {
                _lerp = true;
                _lerp_target = _positions[_currentScreen - 1];

                ChangeBulletsInfo(_currentScreen - 1);
            }
        }


        //find the closest registered point to the releasing point
        private Vector3 FindClosestFrom(Vector3 start, System.Collections.Generic.List<Vector3> positions)
        {
            Vector3 closest = Vector3.zero;
            float distance = Mathf.Infinity;

            foreach (Vector3 position in _positions)
            {
                if (Vector3.Distance(start, position) < distance)
                {
                    distance = Vector3.Distance(start, position);
                    closest = position;
                }
            }

            return closest;
        }


        //returns the current screen that the is seeing
        public int CurrentScreen()
        {
            float absPoz = Math.Abs(ScreensContainer.gameObject.GetComponent<RectTransform>().offsetMin.x);

            absPoz = Mathf.Clamp(absPoz, 1, _containerSize - 1);

            float calc = (absPoz / _containerSize) * _screens;

            return (int)calc;
        }

        //changes the bullets on the bottom of the page - pagination
        private void ChangeBulletsInfo(int currentScreen)
        {
            if (Pagination)
                for (int i = 0; i < Pagination.transform.childCount; i++)
                {
                    Pagination.transform.GetChild(i).GetComponent<Toggle>().isOn = (currentScreen == i)
                        ? true
                        : false;
                }
        }

        //used for changing between screen resolutions
        private void DistributePages()
        {
            int _offset = 20;
            //int _step = 150;    // Screen.width;
            float _dimension = 0;

            float currentXPosition = 0;

            for (int i = 0; i < ScreensContainer.transform.childCount; i++)
            {
                RectTransform child = ScreensContainer.transform.GetChild(i).gameObject.GetComponent<RectTransform>();
                currentXPosition = _offset + i * child.rect.width;
                child.anchoredPosition = new Vector2(currentXPosition, 0f);
                //child.sizeDelta = new Vector2(gameObject.GetComponent<RectTransform>().sizeDelta.x, gameObject.GetComponent<RectTransform>().sizeDelta.y);
                child.sizeDelta = new Vector2(child.GetComponent<RectTransform>().sizeDelta.x, child.GetComponent<RectTransform>().sizeDelta.y);

            }

            _dimension = currentXPosition + _offset * -1;
            //ScreensContainer.GetComponent<RectTransform>().offsetMax = new Vector2(_dimension + 600f, 0f);
             ScreensContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(_dimension - 500f, 0f);

        }

        public void OnEndDrag(PointerEventData eventData)
        {
            //throw new NotImplementedException();
        }




        // Siva Added this, has to be removed later...
        /// <summary>
        /// Add a new child to this Scroll Snap and recalculate it's children
        /// </summary>
        /// <param name="GO">GameObject to add to the ScrollSnap</param>
        public void AddChild(GameObject GO)
        {
            AddChild(GO, false);
        }

        /// <summary>
        /// Add a new child to this Scroll Snap and recalculate it's children
        /// </summary>
        /// <param name="GO">GameObject to add to the ScrollSnap</param>
        /// <param name="WorldPositionStays">Should the world position be updated to it's parent transform?</param>
        public void AddChild(GameObject GO, bool WorldPositionStays)
        {
            _scroll_rect.horizontalNormalizedPosition = 0;
            GO.transform.SetParent(_screensContainer, WorldPositionStays);
            InitialiseChildObjectsFromScene();
            DistributePages();
            if (MaskArea) UpdateVisible();

            SetScrollContainerPosition();
        }

        /// <summary>
        /// Remove a new child to this Scroll Snap and recalculate it's children 
        /// *Note, this is an index address (0-x)
        /// </summary>
        /// <param name="index">Index element of child to remove</param>
        /// <param name="ChildRemoved">Resulting removed GO</param>
        public void RemoveChild(int index, out GameObject ChildRemoved)
        {
            RemoveChild(index, false, out ChildRemoved);
        }

        /// <summary>
        /// Remove a new child to this Scroll Snap and recalculate it's children 
        /// *Note, this is an index address (0-x)
        /// </summary>
        /// <param name="index">Index element of child to remove</param>
        /// <param name="WorldPositionStays">If true, the parent-relative position, scale and rotation are modified such that the object keeps the same world space position, rotation and scale as before</param>
        /// <param name="ChildRemoved">Resulting removed GO</param>
        public void RemoveChild(int index, bool WorldPositionStays, out GameObject ChildRemoved)
        {
            ChildRemoved = null;
            if (index < 0 || index > _screensContainer.childCount)
            {
                return;
            }
            _scroll_rect.horizontalNormalizedPosition = 0;

            Transform child = _screensContainer.transform.GetChild(index);
            child.SetParent(null, WorldPositionStays);
            ChildRemoved = child.gameObject;
            InitialiseChildObjectsFromScene();
            DistributePages();
            if (MaskArea) UpdateVisible();

            if (_currentPage > _screens - 1)
            {
                CurrentPage = _screens - 1;
            }

            SetScrollContainerPosition();
        }

        /// <summary>
        /// Remove all children from this ScrollSnap
        /// </summary>
        /// <param name="ChildrenRemoved">Array of child GO's removed</param>
        public void RemoveAllChildren(out GameObject[] ChildrenRemoved)
        {
            RemoveAllChildren(false, out ChildrenRemoved);
        }

        /// <summary>
        /// Remove all children from this ScrollSnap
        /// </summary>
        /// <param name="WorldPositionStays">If true, the parent-relative position, scale and rotation are modified such that the object keeps the same world space position, rotation and scale as before</param>
        /// <param name="ChildrenRemoved">Array of child GO's removed</param>
        public void RemoveAllChildren(bool WorldPositionStays, out GameObject[] ChildrenRemoved)
        {
            var _screenCount = _screensContainer.childCount;
            ChildrenRemoved = new GameObject[_screenCount];

            for (int i = _screenCount - 1; i >= 0; i--)
            {
                ChildrenRemoved[i] = _screensContainer.GetChild(i).gameObject;
                ChildrenRemoved[i].transform.SetParent(null, WorldPositionStays);
            }

            _scroll_rect.horizontalNormalizedPosition = 0;
            CurrentPage = 0;
            InitialiseChildObjectsFromScene();
            DistributePages();
            if (MaskArea) UpdateVisible();
        }

        private void SetScrollContainerPosition()
        {
            _scrollStartPosition = _screensContainer.localPosition.x;
            _scroll_rect.horizontalNormalizedPosition = (float)(_currentPage) / (_screens - 1);
            OnCurrentScreenChange(_currentPage);
             
        }
    }
}