﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using System.IO;

public class StringManager : MonoBehaviour
{
    //public Text inputText;
    //private string inputString;
    //private string[] arr;
    public RawImage t, t1;
    // Start is called before the first frame update
    void Start()
    {
        //inputString = inputText.text.ToString();
        //Splitter(inputString);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Texture2D tempTexture2D = (Texture2D)t.texture;
            byte[] texAsByte = tempTexture2D.EncodeToPNG();
            PlayerPrefs.SetInt("UserProfileImageExists", 1);
            File.WriteAllBytes(Application.persistentDataPath + "/UserProfileImage.png", texAsByte);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if(PlayerPrefs.HasKey("UserProfileImageExists"))
            {
                byte[] bArray = File.ReadAllBytes(Application.persistentDataPath + "/UserProfileImage.png");
                Texture2D tempTexture2D = new Texture2D(2, 2);
                tempTexture2D.LoadImage(bArray);
                t1.texture = tempTexture2D;
            } 
        }
    }

    private void Splitter(string str)
    {
       //arr = str.Split('-');
       // for(int i=0; i<arr.Length; i++)
       // {
       //     Debug.Log(arr[i]);
       // }  
    }

    public void OnClickS()
    {
        Texture2D tempTexture2D = (Texture2D)t.texture;
        byte[] texAsByte = tempTexture2D.EncodeToPNG();
        PlayerPrefs.SetInt("UserProfileImageExists", 1);
        File.WriteAllBytes(Application.persistentDataPath + "/UserProfileImage.png", texAsByte);
    }

    public void OnClickP()
    {
        if (PlayerPrefs.HasKey("UserProfileImageExists"))
        {
            byte[] bArray = File.ReadAllBytes(Application.persistentDataPath + "/UserProfileImage.png");
            Texture2D tempTexture2D = new Texture2D(2, 2);
            tempTexture2D.LoadImage(bArray);
            t1.texture = tempTexture2D;
        }
    }
}
